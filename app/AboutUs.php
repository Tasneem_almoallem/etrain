<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class AboutUs extends Model
{
    
   protected $table="about_us";
   public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brief',
        'vision',
        'goal',
    ];

}
