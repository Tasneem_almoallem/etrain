<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
     public function user_course(){

    	return $this->belongsTo('App\User_Course');
    }

     protected $fillable = ['name_trainee_ar',
                             'name_trainee_en',
                             'national_id',
                             'code_trainee',
                             'code_course',
                             'date_receipt',
                             'money_payed',
                             'name_admin'
       ];
   protected $hidden = [
         'remember_token',];
}
