<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class exam_user extends Model
{
    //
    protected $table="exams_users";
    protected $guarded=[];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }
}
