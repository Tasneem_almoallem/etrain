<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    protected $table="home_page";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_brief',
        'coach1_id',
        'coach2_id',
        'coach3_id',
        'coach4_id',
   
    ];

    
    public function coach1(){
        return $this->belongsTo('App\User');
    }

    public function coach2(){

        return $this->belongsTo('App\User');
    }

     public function coach3(){

        return $this->belongsTo('App\User');
    }
    public function coach4(){

        return $this->belongsTo('App\User');
    }

}
