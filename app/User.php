<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name_ar',
        'middle_name_ar',
        'last_name_ar',
        'first_name_en',
        'middle_name_en',
        'last_name_en',
        'national_id',
        'email',
        'code',
        'birth_country',
        'date_of_birth',
        'state',
        'phone',
        'address',
        'gender',
        'image',
        'cv',
        'notes',
        'password',
        'role',
        'rating'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole(string $role) : bool
    {
        if( $this->role ==$role) {
                return true;
            }
        return false;
    }

    public function courses(){
        return $this->belongsToMany('App\Course','App\Course_User','user_id','course_id');
    }

    public function certificates(){

        return $this->hasMany('App\Certificate');
    }

     public function messages(){

        return $this->hasMany('App\Message');
    }

    public function coachCourses()
    {
        return $this->hasMany(Course::class,'coach_id');
    }

    public function examUsers(){
        return $this->hasMany(exam_user::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }




}
