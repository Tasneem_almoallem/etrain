<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{

	protected $fillable = [
	 	    'user_id',
            'course_id' ,
            'date_of_give' ,

	 ];

     public function user(){

    	return $this->belongsTo('App\User');
    }

    public function course(){

    	return $this->belongsTo('App\Course');
     }
}
