<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Course extends Model
{
    protected $table ="user_courses";

     protected $fillable = ['name_trainee_ar',
                             'name_trainee_en',
                             'national_id',
                             'code_trainee',
                             'code_course',
                             'discount_value',
                             'cause_discount',
                             'type',
                             'notes',
                             'course_name'];

   protected $hidden = [
         'remember_token',];
   public function receipts(){

        return $this->hasMany('App\Receipt');
    }
    // public function course(){

    //     return $this->belongsToMany('App\course','user_courses','code','code_course');
    // }
}
