<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{   
     protected $fillable = [
        'title', 'body', 'sender_id','seen','receiver_id','name','email','is_guest'
    ];

    public function sender(){

    	return $this->belongsTo('App\User');
    }
    public function receiver(){

    	return $this->belongsTo('App\User');
    }
}
