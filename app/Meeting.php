<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    //
    protected $guarded=[];
    public $timestamps=false;
    protected $dates=['start_time'];
    public function course(){
        return $this->belongsTo(Course::class);
    }
}
