<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Footer extends Model
{
    
   protected $table="footer";
   public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'email',
        'address',
        'brief',
        'facebook',
        'twitter',
        'instagram',
        'youtube',
        'linkedin'
    ];

}
