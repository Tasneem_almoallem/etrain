<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Course;
use Carbon\Carbon;

class DashboardController extends Controller
{

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::now();
        $trainees= User::where('role','trainee')
                         ->whereMonth('created_at',$today)
                          ->whereYear('created_at',$today)
                          ->get();
        $trainees_all = User::where('role','trainee')->get();
        $coaches= User::where('role','coach')
                         ->whereMonth('created_at',$today)
                          ->whereYear('created_at',$today)
                          ->get();
        $coaches_all = User::where('role','coach')->get();

        $courses= Course::whereMonth('created_at',$today)
                          ->whereYear('created_at',$today)
                          ->get();
        $courses_all = Course::all();

        $categories= Course::select('category', \DB::raw('count(*) as total'))->groupBy('category')->get();

       $last_courses=Course::orderBy('created_at', 'desc')->take(4)->get();
       $last_trainees = User::where('role','trainee')->take(5)->get();

        return view('dashboard.layout.landing',['trainees'=>count($trainees),
                                              'trainees_all'=>count($trainees_all),
                                               'coaches'=>count($coaches),
                                              'coaches_all'=>count($coaches_all),
                                               'courses'=>count($courses),
                                              'courses_all'=>count($courses_all),
                                            'categories'=>count($categories),
                                            'last_courses'=>($last_courses),
                                          'last_trainees'=>($last_trainees)]);
    }


}
