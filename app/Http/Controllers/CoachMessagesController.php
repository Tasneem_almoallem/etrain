<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Message;
use Carbon\Carbon;

class CoachMessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function store(Request $request)
    {   
        $count = Message::count();
         $messages = new Message();
        $messages->sender_id=Auth::user()->id;
          $messages->receiver_id=$request['receiver_id'];
        $messages->body=$request['body'];
        if(!isset($request['ref'])){
               $count=Message::count();
          $t = Carbon::now();
           $messages->ref= $t->month.$t->year.$t->day.$count;
        }else{
            $messages->ref = $request['ref'];
        }
       // $messages->seen=true;
         $messages->save();
         return redirect()->back();

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $msgs=Message::where(function ($query) use($id) {
                $query->where('receiver_id', '=', $id)
                      ->orWhere('sender_id', '=', $id);
            })->where(function ($query) {
                $query->where('receiver_id', '=', Auth::user()->id)
                      ->orWhere('sender_id', '=', Auth::user()->id);
            })->get();
        $sender = User::where('id',$id)->first();
        $ref='';
        if(!isset($msgs[0])){
            $count=Message::count();
          $t = Carbon::now();
           $ref= $t->month.$t->year.$t->day.$count;
        }
        else{
          $ref = $msgs[0]->ref;  
        }
        
        return view('dashboard.messages.message',compact('msgs','sender','ref'));
    }

 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
