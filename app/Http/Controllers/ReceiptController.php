<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\User;
use App\User_Course;
use App\Receipt;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receipts= Receipt::orderBy('created_at','desc')->paginate(10);

          return view('dashboard.receipts.list',compact('receipts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $users = User::where('role','Admin')->get();
      $courses=Course::all('code','name');
      return view('dashboard.receipts.create',compact('users','courses'));

     }


     public function getform($id)
    {

        $users = User::where('role','Admin')->get();
       $usercourse = User_Course::find($id);
      return view('dashboard.receipts.form',compact('users','usercourse'));
      }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->except('_token', 'id'));
        $receipt = Receipt::create(
            $request->except('_token', 'id')
        );

        return redirect('receipts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('role','Admin')->get();
         $receipt = Receipt::find($id);
        $courses=Course::all('code','name');
        $course=Course::where('code',$receipt->code_course)->first();
        $user_course = User_Course::where('code_course', $course->code)->get();
        $codes=[];
        foreach($user_course as $code){
            array_push($codes,$code->code_trainee);
        }
        $students = User:: whereIn('code',$codes)->get();
        return view('dashboard.receipts.edit',compact('receipt','users','courses','students') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Receipt::where('id', $id)->update(
            $request->except('_token', 'id','_method')
        );


        return redirect('receipts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Receipt::find($id)->delete();
        return redirect('receipts');
    }


    public function print($id)
    {
        $receipt = Receipt::where('id', $id)->first();
        return view('dashboard.receipts.print',compact('receipt') );
    }
}
