<?php

namespace App\Http\Controllers;

use App\answer;
use App\Exam;
use App\exam_user;
use App\media_user;
use App\Meeting;
use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\HomePage;
use App\User;
use App\Certificate;
use App\Footer;
use App\Photo;
use App\AboutUs;
use App\Faq;
use App\User_Course;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\Models\Media;

class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {

        }

        /**
         * Show the application home page.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
          $homeContent=HomePage::with('coach1','coach2','coach3','coach4')->first();
          $courses_count=count(Course::all());
          $coachs_count=count(User::where('role','coach')->get());
          $certificats_count=count(Certificate::all());
          $trainee_count = count(User::where('role','trainee')->get());
          $last_courses = Course::orderBy('created_at','desc')->take(3)->get();
          $footer = Footer::first();
          $photos = Photo::all();
          $categories = Course::groupBy('category')->get();
            $about_us = AboutUs::first();

            $compacts = ['homeContent'=>$homeContent,
          'courses_count'=>$courses_count,
          'coachs_count'=>$coachs_count,
          'certificats_count'=>$certificats_count,
          'trainee_count'=>$trainee_count,
          'last_courses'=>$last_courses,
          'footer'=>$footer,
          'photos'=>$photos,
          'categories'=>$categories,
                'about_us'=>$about_us];
          if(Auth::user())
              $compacts['user']=Auth::user();
            $compacts['recommendations']=$this->recommendations();

          return view('home', $compacts);
      }


      public function search_cat(Request $request)
      {
        $footer = Footer::first();
        $categories = Course::groupBy('category')->get();
        if($request['degree']!='' || $request['course']!=''){
          if($request['degree']!='' && $request['course']!=''){
            $courses = Course::where(function ($query) use($request){
                                $query->where('category',$request['degree'])
                                ->orWhere('name','LIKE','%'.$request['course'].'%');
                            })->get();
          }
            else if($request['degree']!=''){
               $courses = Course::where(function ($query) use($request){
                                $query->where('category',$request['degree']);
                            })->get();
            }
            else if($request['course']!=''){
               $courses = Course::where(function ($query) use($request){
                                $query->where('name','LIKE','%'.$request['course'].'%');
                            })->get();
            }
               if(count($courses)>0)
                  return view('website.courses',compact('courses','footer','categories'));
                else{
                   $courses = Course::all();
                   $messageSearch='لا يوجد دورات بهذه المعلومات ';
                  return view ('website.courses',compact('courses','footer','categories','messageSearch'));
                }
        }  else{
          $courses = Course::all();
          return view ('website.courses',compact('courses','footer','categories','messageSearch'));
        }

    }

        /**
         * Show the application courses.
         *
         * @return \Illuminate\Http\Response
         */
        public function courses()
        {
          $footer = Footer::first();
          $courses = Course::all();
          $categories = Course::groupBy('category')->get();
          $compacts = [ 'footer'=>$footer, 'courses'=>$courses,'categories'=>$categories];
          if(Auth::user())
              $compacts['user']=Auth::user();

            return view('website.courses',$compacts);
        }

        /**
         * Show the application course details.
         *
         * @return \Illuminate\Http\Response
         */
        public function courseDetails($id)
        {
           $footer = Footer::first();
            $last_courses = Course::take(5)->get();
            $course = Course::where('id',$id)->first();
            $categories = Course::select('category', \DB::raw('count(*) as total'))
                                  ->groupBy('category')->get();
            $compacts = [ 'footer'=>$footer, 'last_courses'=>$last_courses,
                          'categories'=>$categories, 'course'=>$course];
          if(Auth::user()){
             $compacts['user']=Auth::user();
             $compacts['courseUser']= User_Course::where('code_trainee',Auth::user()->code)
                                                  ->where('code_course',$course->code)->count();
          }
             $count =User_Course::where('code_course',$course->code)->count();//max_num
             $date= Carbon::createFromFormat('Y-m-d', $course['date_of_end']);
             $mutable = Carbon::now();
             $not_availble = $mutable >= $date || $count >= $course->max_num;
             $compacts['not_availble'] = $not_availble;
            $compacts['recommendations']=$this->recommendations();
            if(Auth::user())
            {
                $isRated=media_user::where('user_id',Auth::user()->id)->where('course_id',$course->id)->select('rating','media_id')->get();
                $rated=collect();
                foreach ($isRated as $rec){
                    $rated[$rec->media_id]=$rec->rating;
                }
                $compacts['rated']=$rated;
                $compacts['comments']=$course->comments()->whereNull('parent_id')->get();
                $rec=Receipt::where('code_course',$course->code)->where('code_trainee',Auth::user()->code)->first();
                $compacts['is_paid']=isset($rec);
                //dd($isRated);
            }
            return view('website.course-details',$compacts);
        }


      public function store(){
        $code_trainee=Auth::user()->code;
      }
        /**
         * Show the application faq.
         *
         * @return \Illuminate\Http\Response
         */
        public function faq()
        {
           $footer = Footer::first();

            $faq = Faq::all();
            $compacts = ['footer'=>$footer,'faqs'=>$faq];
            if(Auth::user())
              $compacts['user']=Auth::user();
            return view('website.faq',$compacts);
        }

        /**
         * Show the application contact us.
         *
         * @return \Illuminate\Http\Response
         */
        public function contactUs()
        {
            $footer = Footer::first();

            $compacts = ['footer'=>$footer];
            if(Auth::user())
              $compacts['user']=Auth::user();
            return view('website.contact-us',$compacts);
        }
        /**
         * Show the application about us.
         *
         * @return \Illuminate\Http\Response
         */
        public function aboutUs()
        {
          $homeContent=HomePage::with('coach1','coach2','coach3','coach4')->first();
          $courses_count=count(Course::all());
          $coaches = User::where('role','coach')->get();
          $coachs_count=count($coaches);
          $certificats_count=count(Certificate::all());
          $trainee_count = count(User::where('role','trainee')->get());
          $last_courses = Course::orderBy('created_at','desc')->take(3)->get();
          $footer = Footer::first();
          $about_us = AboutUs::first();

          $compacts = ['homeContent'=>$homeContent,
          'courses_count'=>$courses_count,
          'coachs_count'=>$coachs_count,
          'certificats_count'=>$certificats_count,
          'trainee_count'=>$trainee_count,
          'last_courses'=>$last_courses,
          'footer'=>$footer,
           'coaches'=>$coaches,
           'about_us'=>$about_us];
          if(Auth::user())
              $compacts['user']=Auth::user();
            return view('website.about-us',$compacts);
        }
        /**
         * Show the application profile.
         *
         * @return \Illuminate\Http\Response
         */
        public function profile()
        {
            return view('website.profile');
        }

        public function download($id)
        {
            $media=Media::findorfail($id);
            return $media;
        }

        public function getExam($id)
        {
            if(Auth::user())
                $compacts['user']=Auth::user();
            $exam=Exam::findorfail($id);
            $footer = Footer::first();
            return view('website.exam',['exam'=>$exam,'user'=>Auth::user(),'footer'=>$footer]);
        }
        public function getSignature(Request $request){
            $meeting=$request->meetingData;
            $time = time() * 1000 - 30000;//time in milliseconds (or close enough)

            $data = base64_encode("l3Yp3mZfBMnldcdUwomJ7dJ9dlXMsDllW6ju" . $meeting['meetingNumber'] . $time . $meeting['role']);

            $hash = hash_hmac('sha256', $data, "ZAgKcnXzF0JIAh2IrQPZ9eZHgkAAwmrYoPqX", true);

            $_sig = "l3Yp3mZfBMnldcdUwomJ7dJ9dlXMsDllW6ju" . "." . $meeting['meetingNumber'] . "." . $time . "." . $meeting['role'] . "." . base64_encode($hash);

            //return signature, url safe base64 encoded
            return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
        }

        public function join($id)
        {
            $meeting=Meeting::find($id);
            $role=0;
            return view('dashboard.zoom',compact('meeting','role'));
        }

        public function getExamResult($id,Request $request){

            $exam=Exam::findorfail($id);
            $fullmark=$exam->questions->sum('marks');
            $studentMark=0;
            $questions=$exam->questions;
            foreach($questions as $qu){
                if(isset($request->question[$qu->id]))
                {
                    $correct=answer::where('question_id',$qu->id)->where('isCorrect',1)->get();
                    if($correct[0]->id==$request->question[$qu->id])
                        $studentMark+=$qu->marks;
                }
            }
            exam_user::create([
               'result'=>$studentMark,
               'fullMark'=>$fullmark,
                'user_id'=>Auth::user()->id,
                'exam_id'=>$exam->id
            ]);
            $answers=$request->question;
            $footer = Footer::first();
            $user=Auth::user();
            return view('website.exam_result',compact('footer','user','fullmark','studentMark','exam','answers'));

        }

        public function recommendations(){
            $recomendation=collect();
            if(Auth::user()&&Auth::user()->role=='trainee') {
                $id = Auth::user()->code;
                $courses = User_Course::where('code_trainee', $id)->get();
                $ids = [];
                foreach ($courses as $course) {
                    array_push($ids, $course->code_course);
                }
                $course_last = User_Course::where('code_trainee', $id)->orderBy('id', 'desc')->first();

                $course = Course::where('code', $course_last->code_course)->first();

                $recomendation = Course::whereNotIn('code', $ids)->get();


                if ($course_last) {
                    $recomendation = $recomendation->where('category', $course->category)->take(2);
                }

                $recomendation = $recomendation->sortByDesc("created_at");
                $recomendation2 = collect();
                $favCate = Course::whereIn('code', $ids)->select('category_id', DB::raw('count(*) as total'))->groupby('category_id')->orderby('total', 'DESC')->first();
                if (isset($favCate))
                    $recomendation2 = Course::whereNotIn('code', $ids)->where('category_id', $favCate->category_id)->limit(4)->get();
                $recomendation = $recomendation->merge($recomendation2);
            }
            return $recomendation;
        }

        public function saveRate($id,$course,Request $request)
        {
            $request->validate([
               'star'=>'required'
            ]);

            $video=Media::find($id);
            $course=Course::find($course);

            media_user::create([
             'user_id'=>Auth::user()->id,
             'course_id'=>$course->id,
             'media_id'=>$id,
             'rating'=>$request->star
            ]);
            $rating=DB::table('media_users')->where('course_id',$course->id)->avg('rating');
            $course->update([
               'rating'=>$rating
            ]);

            return back();
        }
    }
