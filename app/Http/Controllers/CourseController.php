<?php

namespace App\Http\Controllers;

use App\category;
use App\User_Course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Course;
use App\User;
use Illuminate\Support\Facades\Date;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.courses.list', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = new Course;
        $course->code = $this->generateRandomString(6);
        $coachs = User:: where('role','coach')->get();
        $categories=category::all();
        return view('dashboard.courses.form', compact('course','coachs','categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   // $course =new Course();

        $request->validate([
            'name' => ['required'],
            'description' => ['required'],
            'period' => ['required'],
            'cost_student' => ['required'],
            'cost_individual' => ['required'],
            'cost_company' => ['required'],
            'max_num' => ['required'],
            'date_of_start' => ['required'],
            'date_of_end' => ['required'],
            'session_duration' => ['required'],
            'time_of_start_session' => ['required'],
            'time_of_end_session' => ['required'],
            'days_of_session' => ['required'],
            'num_room' => ['required'],
            'coach_id' => ['required'],
            'cost_hour_coach' => ['required'],
            'cost_total_coach' => ['required'],
            'cost_rate_coach' => ['required'],
            'category_id'=>'required'
        ]);

        $request['category']=category::findorfail($request->category_id)->name;
        //dd($request->except('_token', 'id'));
        $course = Course::create(
            $request->except('_token', 'id')
        );


        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $course->image = $imageName;
            $course->save();
        }
        return redirect('courses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
         $coachs = User:: where('role','coach')->get();
        $categories=category::all();
        return view('dashboard.courses.form',compact('course','coachs','categories') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request['category']=category::findorfail($request->category_id)->name;
       // dd( $request->except('_token', 'id','_method'));
        Course::where('id', $id)->update(
            $request->except('_token', 'id','_method')
        );
        $course =   Course::find($id);
        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $course->image = $imageName;
            $course->save();
        }

        return redirect('courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Course::find($id)->delete();
        return redirect('courses');
    }

    public function search(Request $request)
    {
        $q = $request['q'];
        $courses = Course::where(function ($query) use($q){
            $query->where('name','LIKE','%'.$q.'%')
                  ->orWhere('category','LIKE','%'.$q.'%');
        })->paginate(10);


        if(count($courses) > 0)
        {
            return view('dashboard/courses.list',compact('courses','q'));
            }  else
            return view ('dashboard/courses.list',compact('q'))->withMessage('لا يوجد كورس بهذه المعلومات ');
    }



  public function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return time() . '.' . $randomString;
    }

    public function getAvailableTime(Request $request){
        $courses=Course::where('num_room',$request->room)->whereDate('date_of_start','<=', $request->startDate)
            ->whereDate('date_of_end','>=', $request->startDate)
            ->get();
        $courses2=Course::where('num_room',$request->room)->whereDate('date_of_start','<=', $request->endDate)
            ->whereDate('date_of_end','>=', $request->endDate)
            ->get();
        $courses=$courses->merge($courses2);
        $available_time=collect();
        $hoursRange=$this->hoursRange(28800, 64800, $request->duration*60);
      //  dd($courses);
        for($i=0;$i<count($hoursRange)-1;$i++)
        {
            $booked=false;
            $hour=$hoursRange[$i];
            foreach ($courses as $course)
            {
                $startTime=new Carbon($course->time_of_start_session);
                $endTime=new Carbon($course->time_of_end_session);
                $current=new Carbon($hour);
                $next=new Carbon($hoursRange[$i+1]);
                if(($current->isBetween($startTime,$endTime)&&$current!=$endTime)||($next->isBetween($startTime,$endTime)&&$next!=$startTime))
                {
                    $booked=true;
                    break;
                }

            }
            if(!$booked)
            {
                $available_time->push($hour." - ".$hoursRange[(int)$i+1]);
            }
        }
        return response()->json(['times'=>$available_time]);
    }
    function hoursRange( $lower = 28800, $upper = 86400, $step = 3600, $format = '' ) {
        $times = array();

        if ( empty( $format ) ) {
            $format = 'H:i';
        }

        foreach ( range( $lower, $upper, $step ) as $increment ) {
            $increment = gmdate( 'H:i', $increment );

            list( $hour, $minutes ) = explode( ':', $increment );

            $date = new \DateTime( $hour . ':' . $minutes );

            $times[] = $date->format( $format );
        }

        return $times;
    }

    public function getPrice(Course $course)
    {
        if($course)
        {
            $user_course = User_Course::where('code_course', $course->code)->get();
            $codes=[];
            foreach($user_course as $code){
                array_push($codes,$code->code_trainee);
            }
            $users = User:: whereIn('code',$codes)->get();
            return response()->json(['price'=>$course->cost_student,'students'=>$users]);
        }
        else
            return response()->json(['price'=>0,'students'=>[]]);
    }

}
