<?php

namespace App\Http\Controllers;


use App\Meeting;
use App\Traits\ZoomMeetingTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeetingController extends Controller
{
    use ZoomMeetingTrait;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    public function index()
    {
        $ids=[];
        if(Auth::user()->role=='coach')
        {
            foreach (Auth::user()->coachCourses as $course)
                array_push($ids,$course->id);
        }

        $meetings=Meeting::whereIn('course_id',$ids)->orderBy('id','desc')->paginate(10);
        return view('dashboard.meetings.list',compact('meetings'));
    }
    public function show($id)
    {
        $meeting = $this->get($id);

        return view('meetings.index', compact('meeting'));
    }

    public function createMeeting()
    {
        return view('dashboard.meetings.form');
    }
public function edit(Meeting $meeting)
{
    return view('dashboard.meetings.form',compact('meeting'));
}
    public function store(Request $request)
    {
       // dd();
        $res=$this->create($request->all());
       // dd(now())
        Meeting::create([
           'meeting_id'=>$res['data']['id'],
            'password'=>$res['data']['encrypted_password'],
            'start_time'=>new \DateTime($request->start_time),
            'course_id'=>$request->course_id,
            'topic'=>$request->topic,
            'duration'=>$request->duration
        ]);

        return redirect('meeting');
    }

    public function updateMeeting($id, Request $request)
    {
        $meeting=Meeting::find($id);
        $res=$this->update($meeting->meeting_id, $request->all());
        //dd($meeting);

        $meeting->update([
            'start_time'=>new \DateTime($request->start_time),
            'course_id'=>$request->course_id,
            'topic'=>$request->topic,
            'duration'=>$request->duration
        ]);
        return redirect('meeting');
    }

    public function destroy(Meeting $meeting)
    {
        $this->delete($meeting->meeting_id);
        $meeting->delete();

        return redirect('meeting');
    }

    public function startMeeting($id){
        $meeting=Meeting::find($id);
        $role=1;
        return view('dashboard.zoom',compact('meeting','role'));
    }
}
