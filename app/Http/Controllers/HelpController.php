<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\HomePage;
use App\User;
use App\Certificate;
use App\Footer;
use App\Photo;
use App\AboutUs;
use App\Message;
use Carbon\Carbon;
class HelpController extends Controller
{
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {

        }

        /**
         * Show the application home page.
         *
         * @return \Illuminate\Http\Response
         */
        public function sendMsg(Request $request)
        { 
          if(!Auth::user()){

            $request['is_guest'] = true;
          }
          else{
             $request['sender_id'] = Auth::user()->id;
             $request['receiver_id'] = 1;
          }
          $msg = Message::create(
            $request->except('_token')
          );
          $count=Message::count();
          $t = Carbon::now();
           $msg->ref= $t->month.$t->year.$t->day.$count;
           $msg->save();
          return redirect('contact-us')->with('MessageSuccess' , 'سيتم التواصل معك قريبا من قبل المسؤول.');
      }

      public function showphotos()
        { 
            
        }

       public function editphotos(Request $request)
        { 
            
        }

    }
