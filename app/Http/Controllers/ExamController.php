<?php

namespace App\Http\Controllers;

use App\answer;
use App\Exam;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ids=[];
        if(Auth::user()->role=='coach')
        {
            foreach (Auth::user()->coachCourses as $course)
                array_push($ids,$course->id);
        }

        $exams=Exam::whereIn('course_id',$ids)->orderBy('created_at','desc')->paginate(10);
        return view('dashboard.exams.list',compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dashboard.exams.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       // dd($request->all());
        $request->validate([
           'title'=>'required',
           'course_id'=>'required',
           'questions'=>'required',
            'difficult'=>'required'
        ]);
        $exam=Exam::create([
           'title'=>$request->title,
           'course_id'=>$request->course_id,
            'difficult'=>$request->difficult
        ]);
        foreach($request->questions as $que)
        {
            $q=Question::create([
               'text'=>$que['text'],
               'marks'=>$que['mark'],
                'exam_id'=>$exam->id
            ]);
            foreach ($que['choice'] as $answer){
                $an=answer::create([
                    'text'=>$answer['text'],
                    'isCorrect'=>isset($answer['correct'])?$answer['correct']:false,
                    'question_id'=>$q->id
                ]);
            }
        }
        return redirect('exam');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //
        return view('dashboard.exams.form',compact('exam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        //
        $request->validate([
            'title'=>'required',
            'course_id'=>'required',
            'questions'=>'required',
            'difficult'=>'required'
        ]);
        foreach ($exam->questions as $q)
                $q->answers()->delete();
        $exam->questions()->delete();
        $exam->update([
            'title'=>$request->title,
            'course_id'=>$request->course_id,
            'difficult'=>$request->difficult
        ]);
        foreach($request->questions as $que)
        {
            $q=Question::create([
                'text'=>$que['text'],
                'marks'=>$que['mark'],
                'exam_id'=>$exam->id
            ]);
            foreach ($que['choice'] as $answer){
                $an=answer::create([
                    'text'=>$answer['text'],
                    'isCorrect'=>isset($answer['correct'])?$answer['correct']:false,
                    'question_id'=>$q->id
                ]);
            }
        }
        return redirect('exam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        //
        foreach ($exam->questions as $q)
            $q->answers()->delete();
        $exam->questions()->delete();
        $exam->delete();
        return redirect('exam');
    }

    public function getExams(){
        $exams=Auth::user()->examUsers;
        return view('dashboard.exams.trainee',compact('exams'));
    }
}
