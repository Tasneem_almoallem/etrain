<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\User_Course;
use App\Course;
use Illuminate\Support\Facades\DB;

class CourseTraineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::user()->code;

        $courses=User_Course::where('code_trainee',$id)->get();

        $ids= [];
        foreach ($courses as $course){
            array_push($ids,$course->code_course);
            }
         $course_last=User_Course::where('code_trainee',$id)->orderBy('id', 'desc')->first();

         $course= Course::where('code',$course_last->code_course)->first();

         $recomendation = Course::whereNotIn('code', $ids)->get();


         if($course_last){
             $recomendation = $recomendation->where('category',$course->category)->take(2);
         }

          $recomendation=$recomendation->sortByDesc("created_at");
         $recomendation2=collect();
         $favCate=Course::whereIn('code', $ids)->select('category_id', DB::raw('count(*) as total'))->groupby('category_id')->orderby('total','DESC')->first();
         if(isset($favCate))
             $recomendation2=Course::whereNotIn('code', $ids)->where('category_id',$favCate->category_id)->limit(4)->get();
         $recomendation=$recomendation->merge($recomendation2);
     return view('dashboard.users.course',compact('courses','recomendation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
