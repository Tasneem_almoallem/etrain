<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\File as modelFile;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course=Course::findorfail($request->course_id);

        if (count($course->getMedia('documents')) > 0) {
            foreach ($course->getMedia('documents') as $media) {
                if (!in_array($media->file_name, $request->input('files', []))) {
                    $media->delete();
                }
            }
        }

        $media = $course->getMedia('documents')->pluck('file_name')->toArray();

        foreach ($request->input('files', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $course->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('documents','media');
            }
        }

        return redirect()->back();
        /*$image = $request->file('image');
        if (isset($image)) {
            $file = new File;
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $file->path = $imageName;
            $file->course_id= $request['course_id'];
            $file->type="image";
            $file->save();
        }
        $cv = $request->file('course_file');
        if (isset($cv)) {
            $file = new File;
            $cvName = time() . '.' . $cv->getClientOriginalExtension();
            $cv->move(public_path('cvs'), $cvName);
            $file->path = $cvName;
             $file->course_id= $request['course_id'];
            $file->type="file";
            $file->save();
        }
        */
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       /* $user = User::find($id);
        return view('dashboard.users.details', $user);*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $files = modelFile::where('course_id',$id)->get();
        $course=Course::findorfail($id);
        return view('dashboard.files.form', ['course'=>$course,'files'=>$files,'course_id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('users');
    }

    public function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return time() . '.' . $randomString;
    }
    public function storeMedia(Request $request)
    {
        $path = storage_path('tmp/uploads/');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    public function deleteMedia(Request $request)
    {
        $path = storage_path('tmp/uploads/');
        // dd($path.$request->name);
       File::delete($path.$request->name);
        return response()->json([
            'success'          => "success"
        ]);
    }


}
