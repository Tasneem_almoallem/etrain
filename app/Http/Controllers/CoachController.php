<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CoachController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role','coach')->orderBy('created_at','desc')->paginate(10);
        return view('dashboard.users.list', ['users'=>$users,'role'=>'coach']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user->code = $this->generateRandomString(6);
        return view('dashboard.users.form', ['user' => $user,'role'=>'coach']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users',
        ]);
        $request['role'] = 'coach';
        $user = User::create(
            $request->except('_token', 'id')
        );
        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $user->image = $imageName;
            $user->save();
        }
        $cv = $request->file('cv');
        if (isset($cv)) {
            $cvName = time() . '.' . $cv->getClientOriginalExtension();
            $cv->move(public_path('cvs'), $cvName);
            $user->cv = $cvName;
            $user->save();
        }
          if (Auth::user()->role == 'Admin')
           return redirect('coach');
        else
          return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.users.form', ['user'=>$user,'role'=>'coach']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id', $id)->update(
            $request->except('_token', 'id','_method','password')
        );

        $user =   User::find($id);
        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $user->image = $imageName;
        }

        $cv = $request->file('cv');
        if (isset($cv)) {
            $cvName =time() . '.'.$cv->getClientOriginalName();
            $cv->move(public_path('cvs'), $cvName);
            $user->cv = $cvName;
        }
        $user->save();
          if (Auth::user()->role == 'Admin')
           return redirect('coach');
        else
          return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('coach');
    }

    public function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return time() . '.' . $randomString;
    }
    public function search(Request $request)
    {
        $q = $request['q'];
        $users = User::where(function ($query) use($q){
            $query->where('first_name_ar','LIKE','%'.$q.'%')
                ->orWhere('last_name_ar','LIKE','%'.$q.'%')
                ->orWhere('last_name_en','LIKE','%'.$q.'%')
                ->orWhere('first_name_en','LIKE','%'.$q.'%')
                ->orWhere('email','LIKE','%'.$q.'%')
                ->orWhere('national_id','LIKE','%'.$q.'%');
        })->where('role','coach')->paginate(10);
        $role = 'coach';
        if(count($users) > 0)
        {
            return view('dashboard/users.list',compact('users','role','q'));
        }
        else
            return view ('dashboard/users.list',compact('role','q'))->withMessage('لا يوجد مدرب بهذه المعلومات');
    }
}
