<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use App\User_Course;
use Illuminate\Http\Request;
use App\Certificate;
use Illuminate\Support\Facades\Auth;


class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $certificates = Certificate::orderBy('created_at','desc')->paginate(10);
        return view('dashboard.certificates.list', compact('certificates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $certificate = new Certificate;
        $users=User::where('role','trainee')->get();
        return view('dashboard.certificates.form', compact('certificate','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'course_id' => ['required'],
            'user_id' => ['required'],
            'date_of_give' => ['required'], ]);
       // dd($request->all());
        $certificate = Certificate::create(
            $request->except('_token', 'id')
        );
        return redirect('certificates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $certificate = Certificate::find($id);
        $user=$certificate->user;
        $id=$user->code;

        $courses=User_Course::where('code_trainee',$id)->get();

        $ids= [];
        foreach ($courses as $course){
            array_push($ids,$course->code_course);
        }
        $courses=Course::whereIn('code', $ids)->get();
        $users=User::where('role','trainee')->get();
        return view('dashboard.certificates.form',compact('certificate','courses','users') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Certificate::where('id', $id)->update(
            $request->except('_token', 'id','_method')
        );
          return redirect('certificates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Certificate::find($id)->delete();
        return redirect('certificates');
    }

    public function search(Request $request)
    {
        $q = $request['q'];
        $certificates = Certificate::where(function ($query) use($q){
            $query->where('name_trainee','LIKE','%'.$q.'%')
                 ->orWhere('name_course','LIKE','%'.$q.'%')
                 ->orWhere('date_of_give','LIKE','%'.$q.'%')
                 ->orWhere('id','LIKE','%'.$q.'%');
        })->paginate(10);


        if(count($certificates) > 0)
        {
            return view('dashboard/certificates.list',compact('certificates','q'));
            }  else
            return view ('dashboard/certificates.list',compact('q'))->withMessage('لا يوجد شهادة بهذه المعلومات ');
    }

    public function print($id)
    {
        $certificate = Certificate::where('id', $id)->first();
        return view('dashboard.certificates.print',compact('certificate') );
    }

    public function getUserCourses(Request $request)
    {
        $user=User::findorfail($request->user_id);
        $options="";
        $id=$user->code;

        $courses=User_Course::where('code_trainee',$id)->get();

        $ids= [];
        foreach ($courses as $course){
            array_push($ids,$course->code_course);
        }
        $courses=Course::whereIn('code', $ids)->get();
        foreach ($courses as $course)
        {
            $options.="<option value='".$course->id."'>".$course->name."</option>";
        }
        return $options;
    }

    public function getCertificates(){
        $certificates=Auth::user()->certificates;
        return view('dashboard.certificates.trainee',compact('certificates'));
    }
}
