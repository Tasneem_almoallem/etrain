<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\User_Course;
class User_CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     $usercourses= User_Course::orderBy('created_at','desc')->paginate(10);
         
          return view('dashboard.register.list',compact('usercourses'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $courses= Course::get();
        $users = User::where('role','trainee')->get(); 
      // $countcourses=User_Course::distinct()->get(['course_name']);
    //    foreach ($countcourses as $countcourse) {
    //        $temp= User_Course::where('course_name','=',$countcourse)->count();
    
    // dd($temp);
    //    }
       //  $temp=User_Course::groupBy('course_name');
       // //  $temp->count();
       //  dd($temp);



        return view('dashboard.register.form',compact('users','courses'));
    }  


    public function getDetails($id=0){
                // $data = User::find($id);
                $data = User::where('national_id',$id)->get();
                // dd($data);
                echo json_encode($data);
                exit;
            }
     public function getValue($id=0){
                
                $data = Course::where('code',$id)->get();
            
                echo json_encode($data);
                exit;
            }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request);
        $course = User_Course::create(
            $request->except('_token', 'id')
        );
  
        return redirect('user_courses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    $courses= Course::get();
        $users = User::where('role','trainee')->get();
        $usercourse = User_Course::find($id);

        return view('dashboard.register.edit',compact('usercourse','courses','users') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        User_Course::where('id', $id)->update(
            $request->except('_token', 'id','_method')
        );


        return redirect('user_courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User_Course::find($id)->delete();
        return redirect('user_courses');
    }
    public function search(Request $request)
    {
        $q = $request['q'];
        $usercourses = User_Course::where(function ($query) use($q){
            $query->where('name_trainee_ar','LIKE','%'.$q.'%')
                 ->orWhere('name_trainee_en','LIKE','%'.$q.'%');
        })->paginate(10);
       
        
        if(count($usercourses) > 0)
        {
            return view('dashboard/register.list',compact('usercourses','q'));
           //dd($usercourse);
            }  else{
            return view ('dashboard/register.list',compact('q'))->withMessage('لا يوجد متدرب مسجل بهذه المعلومات '); }
    }
}
