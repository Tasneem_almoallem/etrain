<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use Carbon\Carbon;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::whereNull('is_guest')->with('sender')
                ->groupBy('ref')
                ->get();
        $guests = Message::whereNotNull('is_guest')
                ->get();        
        return view('dashboard.messages.list', compact('messages','guests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        // $msgs=Message::where('ref',$ref)->with('sender')->get();
        // $sender = Auth::user();
        //   $count=Message::count();
        //   $t = Carbon::now();
        //   $messages->ref= $t->month.$t->year.$t->day.$count;
        // return view('dashboard.messages.message',compact('msgs','sender','ref'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $count = Message::count();
         $messages = new Message();
        $messages->sender_id=Auth::user()->id;
          $messages->receiver_id=$request['receiver_id'];
        $messages->body=$request['body'];
        if(!isset($request['ref'])){
               $count=Message::count();
          $t = Carbon::now();
           $messages->ref= $t->month.$t->year.$t->day.$count;
        }else{
            $messages->ref = $request['ref'];
        }
       // $messages->seen=true;
         $messages->save();
         return redirect()->back();

    }
    public function CreatNote(Request $request){
        $request->validate([
            
            'title' => ['required'],
            'body' => ['required'],
             ]);
        $messages = new Message();
        $messages->sender=Auth::user()->id;
        $messages->title=$request['title'];
        $messages->body=$request['body'];
         $messages->save();


         return redirect()->route('home');
         //return view('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ref= Message::find($id)->ref;
        $msgs=Message::where('ref',$ref)->with('sender')->get();
        $sender = $msgs[0]->sender;
        $ref = $msgs[0]->ref;
        return view('dashboard.messages.message',compact('msgs','sender','ref'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function search(Request $request)
    {
        $q = $request['q'];
        $messages = Message::whereNull('is_guest')->with('sender')->where(function ($query) use($q){
            $query->where('title','LIKE','%'.$q.'%')
                ->orWhere('body','LIKE','%'.$q.'%')
                ->orWhere('sender_id','LIKE','%'.$q.'%');
            })->orWhereHas('sender', function ($query)use($q) {
                    $query->where('first_name_ar','LIKE','%'.$q.'%')
                ->orWhere('last_name_ar','LIKE','%'.$q.'%');
            })->groupBy('ref')
                ->get();

       
        if(count($messages) > 0)
        {
            return view('dashboard/messages.list',compact('messages','q'));
        }
        else
            return view ('dashboard/messages.list',compact('q'))->withMessage('لا يوجد  رسائل بهذه المعلومات');
    }
}
