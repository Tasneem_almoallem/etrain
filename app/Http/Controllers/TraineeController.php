<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Message;
use App\User_Course;
use App\Course;
class TraineeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role','trainee')->orderBy('created_at','desc')->paginate(10);
        return view('dashboard.users.list', ['users'=>$users,'role'=>'trainee']);
    }
    
    public function view(){
     

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user->code = $this->generateRandomString(6);
        return view('dashboard.users.form', ['user' => $user,'role'=>'trainee']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users',
        ]);
        $request['role'] = 'trainee';
        $user = User::create(
            $request->except('_token', 'id')
        );
        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $user->image = $imageName;
            $user->save();
        }
        $cv = $request->file('cv');
        if (isset($cv)) {
            $cvName = time() . '.' . $cv->getClientOriginalExtension();
            $cv->move(public_path('cvs'), $cvName);
            $user->cv = $cvName;
            $user->save();
        }
        if (Auth::user()->role == 'Admin')
           return redirect('trainee');
        else
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* $user = User::find($id);
         return view('dashboard.users.details', $user);*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.users.form', ['user'=>$user,'role'=>'trainee']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        User::where('id', $id)->update(
            $request->except('_token', 'id','_method','password')
        );

        $user =   User::find($id);
        $image = $request->file('image');
        if (isset($image)) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $user->image = $imageName;
        }

        $cv = $request->file('cv');
        if (isset($cv)) {
            $cvName =time() . '.'.$cv->getClientOriginalName();
            $cv->move(public_path('cvs'), $cvName);
            $user->cv = $cvName;
        }
        $user->save();
          if (Auth::user()->role == 'Admin')
           return redirect('trainee');
        else
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('trainee');
    }

    public function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return time() . '.' . $randomString;
    }

    public function search(Request $request)
    {
        $q = $request['q'];
        $users = User::where(function ($query) use($q){
            $query->where('first_name_ar','LIKE','%'.$q.'%')
                ->orWhere('last_name_ar','LIKE','%'.$q.'%')
                ->orWhere('last_name_en','LIKE','%'.$q.'%')
                ->orWhere('first_name_en','LIKE','%'.$q.'%')
                ->orWhere('email','LIKE','%'.$q.'%')
                ->orWhere('national_id','LIKE','%'.$q.'%');
        })->where('role','trainee')->paginate(10);

        $role = 'trainee';
        if(count($users) > 0)
           {
               return view('dashboard/users.list',compact('users','role','q'));
           }
        else
            return view ('dashboard/users.list',compact('role','q'))->withMessage('لا يوجد متدرب بهذه المعلومات');
    }


    public function register($courseId)
    {
        $course = Course::find($courseId);
        $usersInCourse=User_Course::where('code_course',$course->code)->count();
        if($usersInCourse==$course->max_num){
          return redirect()->back()->with('success', 'العدد الأعظمي تجاوز الحد المسموح به لا يمكنك التسجيل');  
        }
        $registerUser= new User_Course();
        
        $user = Auth::user();
        $registerUser->name_trainee_ar = $user->first_name_ar.' '.$user->last_name_ar;
        $registerUser->national_id = $user->national_id;
        $registerUser->code_trainee = $user->code;
        $registerUser->code_course = $course->code;
        $registerUser->course_name = $course->name;
         $registerUser->save();
         return redirect()->back()->with('success', 'تم التسجيل بنجاح');
    }
    
    
     public function closeregister($courseId)
    {
        $course = Course::find($courseId);
        $user = Auth::user();
        User_Course::where('code_trainee',$user->code)->where('code_course',$course->code)->delete();
         return redirect()->back()->with('success', 'تم  إلغاء الاشتراك بنجاح');
    }
}
