<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomePage;
use App\User;
class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homes=HomePage::where('id', 1)->with('coach1','coach2','coach3','coach4')->get();
                $coachs = User::where('role','coach')->get();
        return view('dashboard.mangment.home', compact('homes','coachs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $home = HomePage::with('coach1','coach2','coach3','coach4')->find($id);
        $coachs = User::where('role','coach')->get();
        return view('dashboard.mangment.form',compact('home','coachs') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->coach1){
            $user= User::where('code',$request->coach1)->first()->id;
            if(isset($user)){
                 $request['coach1_id']=$user;
            }
           
        }
        if($request->coach2){
           $user=  User::where('code',$request->coach2)->first()->id;
             if(isset($user)){
                 $request['coach2_id']=$user;
             }
        }
        if($request->coach3){
            $user=User::where('code',$request->coach3)->first()->id;
            if(isset($user)){
                 $ $request['coach3_id']=$user;
             }
        }
        if($request->coach4){
            $user=User::where('code',$request->coach4)->first()->id;
             if(isset($user)){
                  $request['coach4_id']=$user;
             }
        }
        HomePage::where('id', $id)->update(
            $request->except('_token', 'id','_method','coach1','coach2','coach3','coach4')
        );
          return redirect()->back()->with('msg','تم التحديث  بنجاح.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
