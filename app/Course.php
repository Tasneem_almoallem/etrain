<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Course extends Model implements HasMedia
{
    use HasMediaTrait;
	 protected $fillable = [
	 	    'name',
            'description' ,
            'period' ,
            'cost_student' ,
            'cost_individual',
            'cost_company',
            'max_num',
            'date_of_start',
            'date_of_end',
            'session_duration',
            'time_of_start_session',
            'time_of_end_session',
            'days_of_session',
            'num_room',
            'coach_name',
            'cost_hour_coach',
            'cost_total_coach',
            'cost_rate_coach',
            'image',
            'code',
            'category',
            'rating',
         'category_id'
	 ];

	  protected $hidden = [
         'remember_token',
    ];
    public function users(){
       return $this->belongsToMany('App\User','user_courses','course_id','user_id');
    }

    public function certificates(){

        return $this->hasMany('App\Certificate');
    }
      public function files(){

    	return $this->hasMany('App\File');

    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }

    public function meetings()
    {
        return $this->hasMany(Meeting::class);
    }

    public function category()
    {
        return $this->belongsTo(category::class);
    }

    public function coach()
    {
        return $this->belongsTo(User::class,'coach_id');
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
