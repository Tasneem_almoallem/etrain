<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Faq extends Model
{
    
   protected $table="faq";

   public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
        'answer',
    ];

}
