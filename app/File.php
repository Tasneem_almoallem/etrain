<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

	protected $fillable = [
	 	    'path',
            'course_id' ,
            'type' ,
            
	 ];



    public function course(){

    	return $this->belongsTo('App\Course');
        
    }
}