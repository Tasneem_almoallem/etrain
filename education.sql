﻿--
-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2021 at 10:16 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `education`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `brief` text DEFAULT NULL,
  `vision` text DEFAULT NULL,
  `goal` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `brief`, `vision`, `goal`) VALUES
(1, 'يعتبر افتتاح المركز منذ 2019 حدث مهم في عالم التعلم الالكتروني.\r\nاستطعنا خلال سنوات الماضية تقديم الدروس الالكترونية وتقديم المعلومات الوفيرة لطلابنا والصعود بهم إلى درجات العلم العالية.\"', 'نسعى لتحقيق النجاح والتألق لجميع الفئات العمرية من خلال المساعدة في توفير سبل التعلم الالكتروني وتقديم حلول تقنية تساعد في إيصال المعلومات للطلاب.\"', 'أهدافنا الوصول بطلابنا إلى القمم العليا في النجاح والتفوق وتهيئتهم للمباشرة في سوق العمل وتدريبهم لملازمة الحياة العملية فور تخرجهم.\"');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `isCorrect` tinyint(1) NOT NULL DEFAULT 0,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `isCorrect`, `text`, `created_at`, `updated_at`) VALUES
(18, 9, 1, 'database schema builder', '2021-02-06 19:33:18', '2021-02-06 19:33:18'),
(19, 9, 0, 'database tables', '2021-02-06 19:33:18', '2021-02-06 19:33:18'),
(20, 9, 0, 'test type', '2021-02-06 19:33:18', '2021-02-06 19:33:18'),
(21, 9, 0, 'updaaaaaaaaaaate', '2021-02-06 19:33:19', '2021-02-06 19:33:19'),
(22, 10, 1, 'create read update delete', '2021-02-06 19:33:19', '2021-02-06 19:33:19'),
(23, 10, 0, 'blablablabl', '2021-02-06 19:33:19', '2021-02-06 19:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(2, 'web', 'web dev', '2021-02-09 17:16:27', '2021-02-09 17:16:27'),
(3, 'design', 'design', '2021-02-10 21:45:05', '2021-02-10 21:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_of_give` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `date_of_give`, `created_at`, `updated_at`, `user_id`, `course_id`) VALUES
(8, '2021-02-17', '2021-02-10 19:25:14', '2021-02-10 19:25:14', 59, 12);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `course_id`, `comment`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 59, 12, 'blablabalbalalbalbalbalbalbalabla', NULL, '2021-02-10 05:20:36', '2021-02-10 05:20:36'),
(2, 59, 12, 'blablabalbalalbalbalbalbalbalabla', NULL, '2021-02-10 05:20:37', '2021-02-10 05:20:37'),
(3, 59, 12, 'blablabalbalalbalbalbalbalbalabla', NULL, '2021-02-10 05:20:38', '2021-02-10 05:20:38'),
(4, 59, 12, 'blablabalbalalbalbalbalbalbalabla', NULL, '2021-02-10 05:20:39', '2021-02-10 05:20:39'),
(5, 59, 12, 'replyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 2, '2021-02-10 05:30:28', '2021-02-10 05:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `map_image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `period` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_student` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_individual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_start` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_end` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_of_start_session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_of_end_session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days_of_session` varchar(255) CHARACTER SET utf8 NOT NULL,
  `num_room` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coach_id` bigint(20) NOT NULL,
  `cost_hour_coach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_total_coach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_rate_coach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT 5,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `image`, `description`, `period`, `cost_student`, `cost_individual`, `cost_company`, `code`, `max_num`, `date_of_start`, `date_of_end`, `session_duration`, `time_of_start_session`, `time_of_end_session`, `days_of_session`, `num_room`, `coach_id`, `cost_hour_coach`, `cost_total_coach`, `cost_rate_coach`, `notes`, `category`, `rating`, `created_at`, `updated_at`, `category_id`) VALUES
(3, 'تصميم مواقع ويب باستخدام css', '1588787379.jpg', 'تعلم تصميم المواقع من الصفر وحتى الاحتراف وأحصل على شهادة معترف عليها.', 'شهرين', '12000', '15000', '10000', '1587069635.t7p11s', '15', '2020-04-16', '2020-06-16', 'ساعتين', '12:00', '2:00', 'أحد - إثنين- ثلاثاء', '3', 58, '1000', '60000', '30%', NULL, 'design', 5, '2020-04-16 17:42:16', '2020-06-08 23:16:26', NULL),
(4, 'مهارات هندسة البرمجيات', '1612993880.png', 'أساسيات هندسة البرمجيات وأدواتها.', 'شهرين', '8000', '10000', '12000', '15870696357.t7p11s', '15', '2020-04-16', '2020-06-16', 'ساعتين', '12:00', '2:00', 'أحد - إثنين- ثلاثاء', '3', 58, '1000', '75000', '50%', NULL, 'web', 5, '2020-04-16 17:42:16', '2021-02-10 21:51:20', 2),
(8, 'تصميم مواقع الويب', '1612993859.png', 'تم تحديد متطلبات سوق العمل من خلال مواقع التوظيف وتجميعها في هذه الدورة، دورة تصميم مواقع التي تأهلك لتكون مصمم ويب ممتازًا.\r\nمن خلال ورش العمل في هذه الدورة ، لدينا هدف واحد لإعدادك للعمل في السوق وكيفية التفاعل والعمل في فريقك ، وتتضمن الدورة الموضوعات التالية:        \r\nHtml5 language\r\nLanguage Css 3\r\nJavaScript language\r\nJquery Library\r\nWork environment Bootstrap\r\nAjax Library', 'شهرين', '32000', '40000', '30000', '1591654180.PF0Z1g', '20', '2020-08-08', '2020-10-08', 'ساعتين', '5:00', '7:00', 'سبت - اثنين - اربعاء', '5', 58, '1000', '150000', '40%', NULL, 'web', 5, '2020-06-08 22:16:21', '2021-02-10 21:50:59', 2),
(9, 'MySql', '1612993834.png', 'يوضح تاريخ MySQL التركيز على أهم ميزات أنظمة قواعد البيانات وهي السرعة والموثوقية ، مما يؤدي إلى نظام يبرز بين منافسيها دون التضحية بالموثوقية أو سهولة الاستخدام. تعتبر هذه الدورة التدريبية ضرورية لتصبح خبيرًا في أنظمة قواعد البيانات (MySQL).', 'شهر', '25000', '30000', '22000', '1591654639.dQXOeu', '15', '2020-09-01', '2020-10-11', 'ساعة ونصف', '1:00', '2:30', 'أحد - ثلاثاء - خميس', '3', 58, '800', '100000', '48%', NULL, 'web', 5, '2020-06-08 22:21:33', '2021-02-10 21:50:34', 2),
(10, 'CCNA', '1612993797.png', 'تمكين أجهزة الكمبيوتر من التواصل مع بعضها البعض لتسهيل وتنظيم عملية مشاركة البيانات والملفات والمعدات وتوفير الجهد والتكلفة والوصول إلى الخدمات بجميع أنواعها والمتابعة عن بعد ومشاركة الموارد وحماية البيانات وتكرارها وسهولة الإدارة وتوفير الاتصالات والوصول إلى المعلومات والدعم والإدارة المركزية كل هذه الأمور ستتعلمها بعد الانتهاء من هذه الدورة التدريبية ، وستتمكن من تثبيت شبكات الكمبيوتر وتوصيلها ، وتوصيلها ببعضها البعض ، ومنح الأذونات للأجهزة ، وفتح وإغلاق المنفذ وتصبح الدعم الفني وفهم معنى IP ، والتبديل والموجه.', 'شهرين ونصف', '30000', '35000', '28000', '1591656048.keko2K', '16', '2020-07-15', '2020-09-02', 'ساعة ونصف', '4:30', '6:00', 'أحد - ثلاثاء - خميس', '6', 49, '750', '120000', '45%', NULL, 'web', 5, '2020-06-08 22:44:34', '2021-02-10 21:49:57', 2),
(11, 'PhotoShop', '1612993669.png', 'دورة فوتوشوب للمبتدئين', 'شهر ونصف', '21000', '25000', '18000', '1591656287.Gue9Ww', '18', '2020-06-01', '2020-07-20', 'ساعة ونصف', '12:00', '1:30', 'سبت - اثنين - اربعاء', '8', 58, '500', '100000', '25%', NULL, 'design', 5, '2020-06-08 22:49:49', '2021-02-10 21:49:28', 3),
(12, 'laravel', '1612211770.png', 'dfgh jvhjvhj j hj hj jh hj hj hj jh hj jhgfydtreswaesdrtfgyuhjiokijuhg', '3 months', '80000', '30000', '5000', '1611608596.nUheQZ', '20', '2021-01-25', '2021-04-04', '1', '6:00', '7:00', 'thu ,su', '5', 58, '5000', '6000', '10', NULL, 'web', 3, '2021-01-25 19:05:20', '2021-02-09 22:01:39', 2),
(13, 'test test', '1612993621.png', 'test                                                                                                                    \r\n                                               test                                                                                                                    \r\n                                               test                                                                                                                    \r\n                                               test                                                                                                                    \r\n                                               test', '3 months', '80000', '30000', '5000', '1612893487.H4pppX', '20', '2021-02-11', '2021-02-28', '2', '6:00', '7:00', 'thu ,su', '5', 56, '5000', '6000', '10', NULL, 'design', 5, '2021-02-09 17:59:28', '2021-02-10 21:47:02', 3);

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `difficult` enum('hard','medium','easy') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `title`, `course_id`, `created_at`, `updated_at`, `difficult`) VALUES
(9, 'laravel test', 12, '2021-02-06 17:47:46', '2021-02-06 19:33:18', 'hard');

-- --------------------------------------------------------

--
-- Table structure for table `exams_users`
--

CREATE TABLE `exams_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `result` int(11) NOT NULL DEFAULT 0,
  `fullMark` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams_users`
--

INSERT INTO `exams_users` (`id`, `user_id`, `exam_id`, `result`, `fullMark`, `created_at`, `updated_at`) VALUES
(1, 59, 9, 3, 7, '2021-02-08 22:31:38', '2021-02-08 22:31:38'),
(2, 59, 9, 3, 7, '2021-02-08 22:32:16', '2021-02-08 22:32:16'),
(3, 59, 9, 3, 7, '2021-02-08 22:33:15', '2021-02-08 22:33:15'),
(4, 59, 9, 3, 7, '2021-02-08 22:33:21', '2021-02-08 22:33:21'),
(5, 59, 9, 3, 7, '2021-02-08 22:34:53', '2021-02-08 22:34:53'),
(6, 59, 9, 3, 7, '2021-02-08 22:36:50', '2021-02-08 22:36:50');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` text DEFAULT NULL,
  `answer` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(1, 'متى سيبدأ التسجيل في لغات البرمجة؟', 'يبدأ التسجيل في كورس لغات البرمجة في 15 الشهر القادم الموافق ليوم الاثنين ويستمر إلى نهاية الشهر القادم. يرجى الالتزام بالمواعيد وستكمال الأوراق المطلوبة للتسجيل'),
(2, 'ماهي الإجراءات اللازمة للتسجيل؟', 'الأوراق المطلوبة للتسجيل: \r\nصورة هوية الشخصية أو جواز سفر\r\nصورة شخصية عدد أربعة مع خلفية بيضاء\r\nشهادة البكالوريا'),
(3, 'كيف يتم دفع الأقساط المترتبة على الطلاب؟؟\"', 'يمكنكم دفع الأقساط من خلال زياة المركز القائم في المزة استراد ودفع النبلغ المترتب على الطالب ويحصل على إيصال بالدفع يبرزه عند الضرورة.\"'),
(4, 'ماهي الشهادات المطلوبة للتسجيل في كورس لغات البرمجة؟', 'شهادة بكالوريوس بمعدل فوق 70%وشهادة لغة اجنبية.'),
(5, 'كيف يتم الدفع؟', 'يتم عن طريق زيارة المركز والدفع المباشر واستلام إيصال الدفع');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `path` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `course_id`, `path`, `type`, `updated_at`, `created_at`) VALUES
(1, 4, '1591642193.docx', 'file', '2020-06-08 18:49:53', '2020-06-08 18:49:53'),
(2, 4, '1591642425.docx', 'file', '2020-06-08 18:53:45', '2020-06-08 18:53:45'),
(3, 4, '1591642848.jpg', 'image', '2020-06-08 19:00:48', '2020-06-08 19:00:48'),
(4, 12, '1611609550.jpg', 'image', '2021-01-25 19:19:10', '2021-01-25 19:19:10'),
(5, 12, '1611609551.jpg', 'image', '2021-01-25 19:19:11', '2021-01-25 19:19:11'),
(6, 12, '1611609578.jpg', 'image', '2021-01-25 19:19:39', '2021-01-25 19:19:39'),
(7, 12, '1611609598.pdf', 'file', '2021-01-25 19:19:58', '2021-01-25 19:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text CHARACTER SET utf8 DEFAULT NULL,
  `brief` text CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `phone`, `email`, `address`, `brief`, `facebook`, `twitter`, `instagram`, `youtube`, `linkedin`) VALUES
(1, '+963 969 607 789', 'smart-train@svu.edu', 'سوريا ، دمشق ، مزة استراد', 'مركز تدريب وتأهيل  متخصص في مجال الدورات التدريبية.\r\nيمكنكم التواصل معنا عبر البريد الإلكتروني أو عن طريق مواقع التواصل الاجتماعي.\r\nويمكنك أن ترسل لنا ملاحظاتك من خلال خدمة الرسائل المتوفرة في الموقع وسنقوم بالرد عليها في أقرب وقت.\r\nأو يمكنكم الاتصال على الأرقام المتوفرة أو زيارة مركزنا في المزة للمزيد من استفساراتكم.', 'https://facebook.com', 'https://twitter.com', 'https://instagram.com', 'https://youtube.com', 'https://linkedin.com');

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE `home_page` (
  `id` int(11) NOT NULL,
  `course_brief` text CHARACTER SET utf8 COLLATE utf8_german2_ci DEFAULT NULL,
  `coach1_id` int(255) DEFAULT NULL,
  `coach2_id` int(255) DEFAULT NULL,
  `coach3_id` int(255) DEFAULT NULL,
  `coach4_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`id`, `course_brief`, `coach1_id`, `coach2_id`, `coach3_id`, `coach4_id`) VALUES
(1, 'موقعنا عبارة عن منصة تعليمية عبر الإنترنت في جميع أنحاء العالم تأسست في عام 2020 من قبل دكاترة ومهندسين وأساتذة سوريين حيث نقدم دورات تدريبية وتخصصات ودرجات ضخمة مفتوحة على الإنترنت.', 26, 25, 26, 28);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--


CREATE TABLE `media` (
 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
 `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `model_id` bigint(20) unsigned NOT NULL,
 `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `size` bigint(20) unsigned NOT NULL,
 `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL ,
 `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL ,
 `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL ,
 `order_column` int(10) unsigned DEFAULT NULL,
 `created_at` timestamp NULL DEFAULT NULL,
 `updated_at` timestamp NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Course', 12, 'documents', '601f0f637ced3_vedio', '601f0f637ced3_vedio.mp4', 'video/mp4', 'public', 6930382, '[]', '[]', '[]', 1, '2021-02-06 19:51:35', '2021-02-06 19:51:35'),
(2, 'App\\Course', 12, 'documents', '602455fb17ffa_vedio', '602455fb17ffa_vedio.mp4', 'video/mp4', 'public', 6930382, '[]', '[]', '[]', 2, '2021-02-10 21:54:05', '2021-02-10 21:54:05');

-- --------------------------------------------------------

--
-- Table structure for table `media_users`
--

CREATE TABLE `media_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `media_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `rating` double(8,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_users`
--

INSERT INTO `media_users` (`id`, `media_id`, `course_id`, `user_id`, `rating`, `created_at`, `updated_at`) VALUES
(1, 1, 12, 59, 4.00, '2021-02-09 21:57:24', '2021-02-09 21:57:24'),
(2, 1, 12, 59, 4.00, '2021-02-09 21:58:11', '2021-02-09 21:58:11'),
(3, 1, 12, 59, 3.00, '2021-02-09 21:59:49', '2021-02-09 21:59:49'),
(4, 1, 12, 59, 1.00, '2021-02-09 22:01:39', '2021-02-09 22:01:39');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meeting_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `meeting_id`, `password`, `start_time`, `course_id`, `topic`, `duration`) VALUES
(3, '76880318156', 'MTlDTjNobUYrQW10T1NqaVBxUXByZz09', '2021-02-08 21:40:00', 12, 'migrations', 120);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `ref` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_guest` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `ref`, `title`, `body`, `sender_id`, `receiver_id`, `seen`, `created_at`, `updated_at`, `name`, `email`, `is_guest`) VALUES
(8, 8, 'home', 'هل يوجد هناك مجموعة من الخطوات للبدء؟', '', NULL, 0, '2020-05-09 08:55:26', '2020-05-09 08:55:26', 'محمد هادي', 'test@gmail.com', 1),
(9, 9, 'Browser extension', 'متى سيبدأ التسجيل؟', '', NULL, 0, '2020-05-09 08:57:46', '2020-05-09 08:57:46', 'عادل حامد', 'test3@gmail.com', 1),
(10, 10, 'Browser extension', 'متى سيبدأ التسجيل؟', '', NULL, 0, '2020-05-09 08:58:37', '2020-05-09 08:58:37', 'عادل حامد', 'test3@gmail.com', 1),
(11, 1, NULL, 'hello', '21', '22', 0, '2020-05-09 15:36:51', '2020-05-09 15:36:51', NULL, NULL, NULL),
(13, NULL, 'تسجيل', 'كيف يمكنني التسجيل', '22', '1', 0, '2020-05-09 15:47:54', '2020-05-09 15:47:54', 'etest testtt', 'test3@gmail.com', NULL),
(21, 1, NULL, 'test', '22', '22', 0, '2020-05-14 14:52:15', '2020-05-14 14:52:15', NULL, NULL, NULL),
(23, 520202222, 'jj', 'كيف يمكنني التسجيل بالموقع', NULL, NULL, 0, '2020-05-22 14:37:05', '2020-05-22 14:37:05', 'أحمد', 'r@gmail.com', 1),
(32, 62020830, NULL, 'tessst', '24', '28', 0, '2020-06-08 17:20:35', '2020-06-08 17:20:35', NULL, NULL, NULL),
(33, 62020831, NULL, 'testttt', '24', '40', 0, '2020-06-08 17:27:42', '2020-06-08 17:27:42', NULL, NULL, NULL),
(34, 62020933, 'دمشق', 'هل سيتم افتتاح دورة جديدة لنظم المعلومات الذكية:', '40', '1', 0, '2020-06-09 00:32:56', '2020-06-09 00:32:56', 'نور دندل', 'Student@host.com', NULL),
(35, 62020934, 'اللاذقية', 'هل الشهادات الممنوحة معترف بها ؟', '45', '1', 0, '2020-06-09 00:42:35', '2020-06-09 00:42:35', 'آية دندل', 'aya@gmail.com', NULL),
(36, 62020934, NULL, '!', '51', '31', 0, '2020-06-09 17:17:25', '2020-06-09 17:17:25', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 2),
(3, '2020_04_01_214520_create_courses_table', 3),
(4, '2020_04_08_211844_create_certificates_table', 3),
(5, '2020_04_10_201927_create_user_courses_table', 4),
(6, '2020_04_12_154257_create_receipts_table', 5),
(7, '2020_04_17_164351_create_messages_table', 6),
(8, '2020_04_26_093710_create_telegrams_table', 7),
(10, '2021_02_02_213756_create_media_table', 8),
(11, '2021_02_03_224527_create_exams_table', 9),
(12, '2021_02_03_224818_create_questions_table', 9),
(13, '2021_02_03_224841_create_answers_table', 9),
(17, '2021_02_05_103111_add_diff_degree_to_exams', 10),
(18, '2021_02_07_053602_add_meeting_table', 10),
(19, '2021_02_08_195401_add_topic_duration_to_meeting', 11),
(21, '2021_02_09_001023_exam_result_table', 12),
(22, '2021_02_09_080540_create_categories_table', 13),
(24, '2021_02_09_183933_add__category_id_to_courses', 13),
(26, '2021_02_09_232702_create_media_users_table', 14),
(27, '2021_02_10_003346_create_comments_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `image`, `title`, `created_at`, `updated_at`) VALUES
(1, '1612128951.png', 'درس متقدم في الهندسة المعلوماتية', '2020-05-08 16:21:44', '2021-01-31 19:35:51'),
(2, '1612128965.png', 'درس متقدم في الهندسة', '2020-05-08 16:21:44', '2021-01-31 19:36:05'),
(3, '1612128981.png', 'درس متقدم في الهندسة', '2020-05-08 16:21:44', '2021-01-31 19:36:21');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `marks` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `text`, `exam_id`, `marks`, `created_at`, `updated_at`) VALUES
(9, 'what is the meaning of migration3?', 9, 4, '2021-02-06 19:33:18', '2021-02-06 19:33:18'),
(10, 'what is the meaning of CRUD?', 9, 3, '2021-02-06 19:33:19', '2021-02-06 19:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name_trainee_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_trainee_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_trainee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_receipt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `money_payed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `receipts`
--

INSERT INTO `receipts` (`id`, `created_at`, `updated_at`, `name_trainee_ar`, `name_trainee_en`, `national_id`, `code_trainee`, `code_course`, `date_receipt`, `money_payed`, `name_admin`) VALUES
(4, '2020-05-23 22:30:25', '2020-05-23 22:30:25', 'آية دندل', 'aya dandal', '0-98765467989', '90876769809', '98789', '2020-04-05', '5000', 'آية دندل'),
(5, '2021-01-25 19:24:41', '2021-01-25 19:24:41', 'tasneem almoallem', 'rfghjk', '3456789', '1611609647.xcRtW4', '1611608596.nUheQZ', '2021-02-22', '80000', 'آية دندل');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` int(11) DEFAULT 5
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name_ar`, `middle_name_ar`, `last_name_ar`, `first_name_en`, `middle_name_en`, `last_name_en`, `national_id`, `email`, `code`, `birth_country`, `date_of_birth`, `state`, `phone`, `address`, `gender`, `image`, `cv`, `notes`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `rating`) VALUES
(25, 'فرح', 'محمد', 'شموط', 'Farah', 'Mohamed', 'Shammout', '0108967893', 'farah@gmail.com', '1588951565.tvhrDw', 'دمشق', '1997-05-02', 'دمشق', '969686632', 'دمشق', 'أنثى', '1612994349.png', NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'coach', NULL, '2020-05-08 12:27:31', '2021-02-10 21:59:09', 5),
(26, 'جيهان', 'مالك', 'نعمان', 'gehan', 'malek', 'noaman', '0108967893', 'gehan12@gmail.com', '1588951564.tvhrDw', 'دمشق', '1997-05-20', 'دمشق', '969686632', 'دمشق', 'أنثى', '1612994367.png', NULL, NULL, '$2y$10$2UYA.9JXQgudq/jT7kuIj./UO9dFC3oIEOIN9Y13rM19EqsFfdP0m', 'coach', NULL, '2020-05-08 12:27:31', '2021-02-10 21:59:27', 5),
(28, 'محمد', 'حسن', 'الأحمد', 'mohammad', 'Hasan', 'Al Ahmed', '0103636989', 'ahmed12@gmail.com', '1588951377.vzbuS0', 'دمشق', '1889-05-14', 'دمشق', '9639635857', 'دمشق', 'ذكر', '1612994318.png', NULL, NULL, '$2y$10$2UYA.9JXQgudq/jT7kuIj./UO9dFC3oIEOIN9Y13rM19EqsFfdP0m', 'coach', NULL, '2020-05-08 12:25:45', '2021-02-10 21:58:38', 5),
(31, 'آية', 'إبراهيم', 'دندل', 'aya', 'ibrahim', 'dandal', '0111011998989', 'ayadandal100@gmail.com', '1590201907.VIgUnr', 'الولايات المتحدة', '1997-08-23', 'Arizona', '0949823106', 'New York', 'أنثى', NULL, NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'trainee', 'svy8EaVllsaI5ct5o3eNi2Zjn2Q2JHbAiZyqIkGOCB22lIKPpAIIDUUz7QDx', '2020-05-23 02:45:07', '2020-05-23 02:58:15', 5),
(35, 'آية', NULL, 'دندل', 'aya', 'ibrahim', 'dandal', '0111011998989', 'ayadandal504@gmail.com', '1590267936.ATwBtL', 'Syria', '1997-07-14', 'lattakia', '0949823106', 'lattakia', 'أنثى', '1591658695.jpg', '1591658596.Aya Dandal.pdf', NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'coach', 'QbXjy3iY8eiWpzh1WApCbkmUi2weybKzSEYLSaKn6fiXRWG7b63uFC7FOJ3x', '2020-05-23 21:05:36', '2020-06-08 23:24:55', 5),
(38, 'ادمن', 'ادمن', 'ادمن', 'admin', 'admin', 'admin', '0111011998989', 'admin@gmail.com', '1590271725.5pIXQW', 'Syria', '1997-08-23', 'lattakia', '0949823106', 'lattakia', 'أنثى', '1590272531.jpg', NULL, NULL, '$2y$10$5kMNWHytzVnHKf6BpRFaQ.2yr0SiW6kaeglRpd3vmiZ2nZ53gPwca', 'Admin', 'ANU6MFMI3JDeGxfgtJik5fKWeLb6m28uebnHbLsz4xkKXQuwNyXlfyq41hyy', '2020-05-23 22:22:11', '2021-02-10 21:39:37', 5),
(45, 'جيهان', 'مالك', 'نعمان', 'gehan', 'malek', 'noaman', '0111011998989', 'aya@gmail.com', '1591567507.qSjO0p', 'الولايات المتحدة', '1999-03-02', 'New York', '0949823106', 'New York', 'أنثى', NULL, NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'trainee', 'dmHnkSJxTbhluIl1AYqWgRR2FoxZPqv7eMkF5carkcyqZxUrJ3UlqeGx5Zft', '2020-06-07 22:05:07', '2020-06-09 01:35:05', 5),
(46, 'فرح', 'محمد', 'شموط', 'Farah', 'Mohamed', 'Shammout', '0-98765467989', 'naya75@gmail.com', '1591571343.8JOsnc', 'الولايات المتحدة', '1998-04-04', 'New York', '0949823106', 'New York', 'أنثى', NULL, NULL, NULL, '$2y$10$fp0ch1aqY4bolrDg9B2cKuVGuI4TUydo1gMCzaf/IFQu1orj8LoVi', 'trainee', 'PsuuJXpRDvyMJlz1w58zsxZ7UKYC5APD1G8bM9uhXwxmfeZRDkHDW0CNsSCR', '2020-06-07 23:09:03', '2020-06-09 01:35:53', 5),
(47, 'آية', 'إبراهيم', 'محمد', 'aya', 'ibrahim', 'mohames', '0111011998989', 'aya1@gmail.com', '1591664593.lJ0gIZ', 'اللاذقية', '1990-04-02', 'اللاذقية', '0949823106', 'دمشق', 'أنثى', '1591665146.jpg', NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'coach', '9TNDo4qEqZ8sKGnZeMEIsGIJbPJXXvT4e03gzCqGBugiCm8bZa8G4rtYsMzK', '2020-06-09 01:03:13', '2020-06-09 01:15:39', 5),
(48, 'أحمد', 'إبراهيم', 'دندل', 'Ahmad', 'ibrahim', 'dandal', '5343565454', 'ayadandal12@gmail.com', '1591705994.S3PnqN', 'الولايات المتحدة', '1990-03-02', 'New York', '0949823106', 'New York', 'ذكر', NULL, NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'coach', '8cDA82OQ3MtjBLBDJWBhJQAGli76HLcpUk6sPiec4aHFZ2ylbw4aWJaSSGHm', '2020-06-09 12:33:14', '2020-06-09 12:39:44', 5),
(49, 'سوسن', 'إبراهيم', 'محمد', 'test', 'test', 'test', '0111011998989', 'ayadandal5@gmail.com', '1591706585.gPtklM', 'الولايات المتحدة', '1990-04-03', 'Arizona', '0949823106', 'New York', 'أنثى', NULL, NULL, NULL, '$2y$10$nhkd.bNrLIFwauPM4mbveu6E3Y8MGTwROsKGZtaffPPhDpBMAUapK', 'coach', 'n2o8C0hb8lFYfCtggs1JUwipey95PIVp8SNCYc4er4OD2Jvb0dWoigu4knxM', '2020-06-09 12:43:05', '2020-06-09 17:09:09', 5),
(51, 'آية', 'test', 'إبراهيم', 'test', 'test', 'test', '0111011998989', 'teacher@host.com', '1591722815.NCmyh8', 'الولايات المتحدة', '1999-03-03', 'New York', '0949823106', 'New York', 'أنثى', NULL, NULL, NULL, '$2y$10$TkY.N0xd38heDoCR/0W1d.LdG/Qc3MfjNYpSZ78FKHeGM1.CbowDm', 'coach', 'ArFvP31XNwX5ByZjX8SfxCBSwo5vygOfcseDBuskC5FBsno7joWJ5NlAoN13', '2020-06-09 17:13:35', '2020-06-09 18:13:02', 5),
(52, 'محمد', 'قققق', 'غالي', 'test', NULL, 'testtt', '656326', 'test9@gmail.com', '1591723225.mybiX6', 'India', '2020-06-10', 'Punjab', '969692257', 'DUBAI\r\ntessst', 'أنثى', NULL, NULL, NULL, '$2y$10$eTY2gsdAnKov1Pv4u0HTOeHXT4LstU0etOKXA9L9qSWUi0ZVJ1.M6', 'coach', 'KUouSKMglGLastSyf1McAngZYgARWtPpBhPmIV6jHQSmW73kdx7INB5yIAMV', '2020-06-09 17:20:25', '2020-06-09 17:20:46', 5),
(56, 'جيهان', NULL, 'مالك', 'gehan', 'malek', 'nouman', '123456789', 'gehano88.no@gmail.com', '1591725854.3aJ8dF', 'دمشق', '1999-01-23', 'دمشق', '0934325529', 'دمشق', 'انثى', '1612994290.png', NULL, NULL, '$2y$10$jRgQmipr/yCCalKBNP00e.ZpK9vfFkUnIwbYzBzx1rc7.W6FwXtYq', 'coach', 'HYyVHq3uXPIy3Z3axEgGauKj81ApBSvrV10BAlFQE1Z5O5rkkPiQJi7R2zad', '2020-06-09 18:04:14', '2021-02-10 21:58:10', 5),
(58, 'tasneem', NULL, 'almoallem', 'tasneem', NULL, 'almoallem', '345654', 'batoolasulh@gmail.com', '1611606831.A92bmK', 'dgdfg', '2021-02-19', 'California', '+963936269995', 'ksnf', 'female', '1612994254.png', NULL, NULL, '$2y$10$AUaGjn9J.XwVd7tQGRvCHu2/BBigssckqY/w/hfaDJSEFRk5Ov.qy', 'coach', 'IDUp9gsrqtlJEq0wK7fNbwWRlWEgF25cYqI1ilS3afzIGCfcfgNjAswq0lgQ', '2021-01-25 18:33:51', '2021-02-10 22:04:23', 5),
(59, 'tasneem', NULL, 'almoallem', NULL, NULL, NULL, NULL, 'tasneemmalmoallem@gmail.com', '1611609647.xcRtW4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$rO6cnXe26Yy5HCIAB7Goc.IcphYSyeyc0HXq8AaQxlO1T7cReyG8u', 'trainee', 'DjcIzodF7G4XjTTrCuUkCLF9E0i723kQqPms3rSP8vcBPEkk2EsMyVFh0QO0', '2021-01-25 19:20:47', '2021-01-25 19:20:47', 5);

-- --------------------------------------------------------

--
-- Table structure for table `user_courses`
--

CREATE TABLE `user_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_trainee_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_trainee_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_trainee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cause_discount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_courses`
--

INSERT INTO `user_courses` (`id`, `name_trainee_ar`, `name_trainee_en`, `national_id`, `code_trainee`, `code_course`, `course_name`, `discount_value`, `cause_discount`, `type`, `notes`, `created_at`, `updated_at`) VALUES
(11, '3testtesttesttt', 'testtesttesttt', '123456', '1585580381.6TiVTh', '1586550600.xxZSxj', 'python', '3000', 'student', 'فرد', 'djsjdcbdsjcbsdjcjsdbcdjscbsjdkb', '2020-04-11 14:36:24', '2020-04-11 19:33:57'),
(13, '3test test testtt', 'test test testtt', '123456', '1585580381.6TiVTh', '1586550511.HLl1gm', 'php', '2000', 'student', 'طالب', NULL, '2020-04-12 10:11:52', '2020-04-12 10:11:52'),
(14, 'etest testtt', '', '548484949', '1585672669.OwEqRN', '1586550600.xxZSxj', 'تعلم python من ا', NULL, NULL, NULL, NULL, '2020-05-13 16:15:33', '2020-05-13 16:15:33'),
(15, 'etest testtt', '', '548484949', '1585672669.OwEqRN', '15870696357.t7p11s', 'مهارات هندسة الب', NULL, NULL, NULL, NULL, '2020-05-13 16:23:03', '2020-05-13 16:23:03'),
(16, 'etest testtt', '', '548484949', '1585672669.OwEqRN', '15865506060.xxZSxj', 'ذكاء الصنعي وخوا', NULL, NULL, NULL, NULL, '2020-05-22 15:01:11', '2020-05-22 15:01:11'),
(17, 'آية دندل', '', '0111011998989', '1590201907.VIgUnr', '1587069635.t7p11s', 'تصميم مواقع ويب ', NULL, NULL, NULL, NULL, '2020-05-23 03:05:32', '2020-05-23 03:05:32'),
(22, 'آية إبراهيم دندل', 'ayaibrahim dandal', '0111011998989', '1590201907.VIgUnr', '1586550511.HLl1gm', 'تصميم مواقع ويب', '1000', 'موظف في المركز', 'فرد', NULL, '2020-06-03 14:10:24', '2020-06-04 02:34:44'),
(23, 'نور دندل', NULL, NULL, '1590275313.00HaZ7', '1587069635.t7p11s', 'تصميم مواقع ويب ', NULL, NULL, NULL, NULL, '2020-06-04 05:36:59', '2020-06-04 05:36:59'),
(24, 'آية دندل', NULL, NULL, '1591567507.qSjO0p', '1587069635.t7p11s', 'تصميم مواقع ويب محترفة باستخدام css', NULL, NULL, NULL, NULL, '2020-06-07 23:02:41', '2020-06-07 23:02:41'),
(25, 'آية دندل', NULL, NULL, '1591567507.qSjO0p', '1590274604.LIBn2t', 'Laravel', NULL, NULL, NULL, NULL, '2020-06-07 23:31:10', '2020-06-07 23:31:10'),
(26, 'آية دندل', NULL, NULL, '1591567507.qSjO0p', '15865506060.xxZSxj', 'ذكاء الصنعي وخوارزميات الذكية', NULL, NULL, NULL, NULL, '2020-06-07 23:32:07', '2020-06-07 23:32:07'),
(27, 'نور دندل', NULL, NULL, '1590275313.00HaZ7', '15865506060.xxZSxj', 'ذكاء الصنعي وخوارزميات الذكية', NULL, NULL, NULL, NULL, '2020-06-07 23:33:44', '2020-06-07 23:33:44'),
(28, 'نور دندل', NULL, NULL, '1590275313.00HaZ7', '15870696357.t7p11s', 'مهارات هندسة البرمجيات', NULL, NULL, NULL, NULL, '2020-06-07 23:37:13', '2020-06-07 23:37:13'),
(29, 'آية إبراهيم دندل', 'aya ibrahim dandal', '0111011998989', '1590201907.VIgUnr', '1591656048.keko2K', 'CCNA', '2000', 'موظف في المركز', 'طالب', NULL, '2020-06-09 12:41:19', '2020-06-09 12:41:19'),
(32, 'رفيف مالك', NULL, NULL, '1591727593.tuqCTA', '15870696357.t7p11s', 'مهارات هندسة البرمجيات', NULL, NULL, NULL, NULL, '2020-06-09 18:34:05', '2020-06-09 18:34:05'),
(33, 'رفيف مالك', NULL, NULL, '1591727593.tuqCTA', '15865506060.xxZSxj', 'ذكاء الصنعي وخوارزميات الذكية', NULL, NULL, NULL, NULL, '2020-06-09 18:35:18', '2020-06-09 18:35:18'),
(34, 'tasneem almoallem', NULL, NULL, '1611609647.xcRtW4', '1611608596.nUheQZ', 'laravel', '0', '0', 'طالب', NULL, '2021-01-25 19:21:14', '2021-01-25 19:23:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_course_id_foreign` (`course_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_category_id_foreign` (`category_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exams_course_id_foreign` (`course_id`);

--
-- Indexes for table `exams_users`
--
ALTER TABLE `exams_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exams_users_user_id_foreign` (`user_id`),
  ADD KEY `exams_users_exam_id_foreign` (`exam_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_users`
--
ALTER TABLE `media_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_users_media_id_foreign` (`media_id`),
  ADD KEY `media_users_course_id_foreign` (`course_id`),
  ADD KEY `media_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meetings_course_id_foreign` (`course_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_exam_id_foreign` (`exam_id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_courses`
--
ALTER TABLE `user_courses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `exams_users`
--
ALTER TABLE `exams_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_users`
--
ALTER TABLE `media_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `user_courses`
--
ALTER TABLE `user_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `exams_users`
--
ALTER TABLE `exams_users`
  ADD CONSTRAINT `exams_users_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`),
  ADD CONSTRAINT `exams_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `media_users`
--
ALTER TABLE `media_users`
  ADD CONSTRAINT `media_users_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `media_users_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`),
  ADD CONSTRAINT `media_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `meetings`
--
ALTER TABLE `meetings`
  ADD CONSTRAINT `meetings_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
