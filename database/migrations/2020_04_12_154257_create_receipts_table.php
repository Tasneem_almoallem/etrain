<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name_trainee_ar');
            $table->string('name_trainee_en');
            $table->string('national_id');
            $table->string('code_trainee');
            $table->string('code_course');
            $table->string('date_receipt');
            $table->string('money_payed');
            $table->string('name_admin');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
