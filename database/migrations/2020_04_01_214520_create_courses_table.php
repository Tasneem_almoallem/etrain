<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('image')->nullable();
            $table->text('description');
            $table->string('period');
            $table->string('cost_student');
            $table->string('cost_individual');
            $table->string('cost_company');
            $table->string('code');
            $table->string('max_num');
            $table->string('date_of_start');
            $table->string('date_of_end');
            $table->string('session_duration');
            $table->string('time_of_start_session');
            $table->string('time_of_end_session');
            $table->string('days_of_session');
            $table->string('num_room');
            $table->unsignedBigInteger('coach_id');
            $table->string('cost_hour_coach');
            $table->string('cost_total_coach');
            $table->string('cost_rate_coach');
            $table->string('category');
            $table->float('rating')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
