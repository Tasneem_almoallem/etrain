<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name_ar');
            $table->string('middle_name_ar')->nullable();
            $table->string('last_name_ar');
            $table->string('first_name_en');
            $table->string('middle_name_en')->nullable();
            $table->string('last_name_en');
            $table->string('national_id');
            $table->string('email');
            $table->string('code');
            $table->string('birth_country');
            $table->string('date_of_birth');
            $table->string('state')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('gender')->nullable();
            $table->string('image')->nullable();
            $table->string('cv')->nullable();
            $table->text('notes')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
