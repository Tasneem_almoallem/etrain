<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Etrain</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/logo.png')}}">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/responsive.css')}}">

    <link rel="stylesheet" href="{{asset('website/css/font-awesome.min.css')}}">
</head>

<body dir="rtl" class="text-right">
<!-- header-start -->
<header>
    <div class="header-area " style=" background-color: rgba(12, 46, 96,0.2); border:0px">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img text-white">


                                <img src="{{asset('img/logo.png')}}" alt="">

                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="main-menu  d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a class="{{ (Request::is('home') ? 'active' : '') }}" href="{{route('home')}}">الصفحة الرئيسية</a></li>
                                    <li><a class="{{ (Request::is('course') ? 'active' : '') }}" href="{{route('course')}}">الدورات</a></li>

                                    <li><a class="{{ (Request::is('faq') ? 'active' : '') }}" href="{{route('faq')}}">الأسئلة الشائعة</a></li>
                                    <li><a class="{{ (Request::is('contactUs') ? 'active' : '') }}" href="{{route('contactUs')}}">تواصل معنا</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                        <div class="log_chat_area d-flex align-items-center">

                            @if(!isset($user))
                                <a href="{{route('login')}}"  class="login ">
                                    <i class="flaticon-1user"></i>
                                    <span style="font-size: 11px;">تسجيل الدخول</span>
                                </a>
                            @else
                                <a class="nav-link text-white" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$user->first_name_ar.' '.$user->last_name_ar}}
                                </a>
                                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                            @if(isset($user) && $user->hasRole('Admin'))
                                                <li class="li-item"><a href="{{url('/dashboard')}}">لوحة التحكم</a></li>
                                                <li class="li-item"> <a href="{{url('/mangment/1/edit')}}"> إدارة محتوى الصفحة الرئيسية </a></li>
                                                <li class="li-item"> <a href="{{url('/mangment_fq')}}"> إدارة الأسئلة الشائعة  </a></li>

                                                   <li class="li-item"> <a href="{{url('photo')}}"> إدارة  صور الدروس </a></li>
                                                <li class="li-item"> <a href="{{url('/mangment_footer/1/edit')}}"> إدارة أسفل الصفحة  </a></li>
                                            @elseif(isset($user) && $user->hasRole('coach'))
                                                <li class="li-item"><a href="{{route('coach.edit',$user->id)}}">إدارة الحساب</a></li>
                                            @else
                                                <li class="li-item"><a href="{{route('trainee.edit',$user->id)}}">إدارة الحساب</a></li>

                                            @endif
                                            <li class="li-item">
                                                <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">تسجيل الخروج</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>

                                            </li>

                                        </ul>

                            @endif
                            @if(isset($footer))
                            <div class="live_chat_btn">
                                <a class="boxed_btn_orange" href="#">
                                     <span dir="ltr">{{$footer->phone}}</span>
                                    <i class="fa fa-phone"></i>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->
@yield('content')
<!-- footer -->
@if(isset($footer))
<footer class="footer footer_bg_1">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <div class="footer_logo text-white">
                            SMART EDUCATION TOP
                            <!--<a href="#">-->
                            <!--    <img src="{{asset('website/img/logo.png')}}" alt="">-->
                            <!--</a>-->
                        </div>
                        <p>
                            {{$footer->brief}}
                        </p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="{{asset($footer->facebook)}}" target="_blank">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{$footer->twitter}}" target="_blank">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{$footer->linkedin}}" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{$footer->instagram}}" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{$footer->youtube}}" target="_blank">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                    <div class="footer_widget">



                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <div class="footer_widget">


                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            العنوان
                        </h3>
                        <p>
                            {{$footer->address}}<br>
                             <span dir="ltr">{{$footer->phone}}</span> <br>
                            <a href="mailto:{{$footer->email}}" target="_blank" class="text-white">
                                {{$footer->email}}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">

                        Copyright &copy;<script>document.write(new Date().getFullYear());</script>

                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
@endif
<!-- footer -->


<!-- form itself end-->
<div id="test-form" class="white-popup-block mfp-hide">
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="{{asset('website/img/form-logo.png')}}" alt="">
                </a>
            </div>
            <h3>تسجيل الدخول</h3>
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="email" placeholder="البريد الالكتروني" value="{{ old('email') }}" required
                               autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="password" placeholder="كلمة المرور" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" class="boxed_btn_orange">تسجيل الدخول</button>
                    </div>
                </div>
            </form>
            <p class="doen_have_acc" >لاتملك حساب ؟؟  <a class="dont-hav-acc pr-3 text-warning"  href="{{ route('register') }}"> إنشاء حساب جديد  </a>
            </p>
        </div>
    </div>
</div>
<!-- form itself end -->




<!-- JS here -->
<script src="{{asset('website/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script src="{{asset('website/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('website/js/popper.min.js')}}"></script>
<script src="{{asset('website/js/bootstrap.min.js')}}"></script>
<script src="{{asset('website/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('website/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('website/js/ajax-form.js')}}"></script>
<script src="{{asset('website/js/waypoints.min.js')}}"></script>
<script src="{{asset('website/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('website/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('website/js/scrollIt.js')}}"></script>
<script src="{{asset('website/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('website/js/wow.min.js')}}"></script>
<script src="{{asset('website/js/nice-select.min.js')}}"></script>
<script src="{{asset('website/js/jquery.slicknav.min.js')}}"></script>
<script src="{{asset('website/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('website/js/plugins.js')}}"></script>
<script src="{{asset('website/js/gijgo.min.js')}}"></script>

<!--contact js-->
<script src="{{asset('website/js/contact.js')}}"></script>
<script src="{{asset('website/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('website/js/jquery.form.js')}}"></script>
<script src="{{asset('website/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('website/js/mail-script.js')}}"></script>

<script src="{{asset('website/js/main.js')}}"></script>
<script type="text/javascript">
        $( document ).ready(function() {
             $('html, body').animate({
                    scrollTop: $("#tabs-courses").offset().top+200
                }, 2000);
             $( ".tab-change" ).click(function() {

                  $cat = $(this).attr('href');
                  $('#myTabContent').removeClass('show');
                   $('#myTabContent').removeClass('active');

                  $.each( $('.tab-pane'), function( key, value ) {
                    if($(value).attr('id')!==$cat){

                       $(value).removeClass('show');
                       $(value).removeClass('active');
                    }
                    });
                });
         });

    </script>
</body>

</html>
