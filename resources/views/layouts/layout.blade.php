<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="{{asset("img/favicon.png")}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}">

    <!-- animate CSS -->
    <link rel="stylesheet" href="{{asset("css/animate.css")}}">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset("css/owl.carousel.min.css")}}">
    <!-- themify CSS -->
    <link rel="stylesheet" href="{{asset("css/themify-icons.css")}}">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{asset("css/flaticon.css")}}">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset("css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/magnific-popup.css")}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset("css/slick.css")}}">

    <!-- style CSS -->
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
    @yield('css')

</head>

<body>
<!--::header part start::-->
<header class="main_menu home_menu">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="/"> <img src="{{URL::to("img/logo.png")}}" alt="logo"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item justify-content-end"
                         id="navbarSupportedContent">
                        <ul class="navbar-nav align-items-center">
                            <li class="nav-item" ><a class="nav-link {{ (Request::is('home') ? 'active' : '') }}" href="{{route('home')}}">الصفحة الرئيسية</a></li>
                            <li class="nav-item" ><a class="nav-link {{ (Request::is('course') ? 'active' : '') }}" href="{{route('course')}}">الدورات</a></li>
                            <li class="nav-item" ><a class="nav-link {{ (Request::is('faq') ? 'active' : '') }}" href="{{route('faq')}}">الأسئلة الشائعة</a></li>
                            <li class="nav-item" ><a class="nav-link {{ (Request::is('contactUs') ? 'active' : '') }}" href="{{route('contactUs')}}">تواصل معنا</a></li>

                            @if(!isset($user))
                                <li class="d-none d-lg-block">
                                <a href="{{route('login')}}" class="btn_1">
                                    <i class="flaticon-1user"></i>
                                    <span style="font-size: 11px;">تسجيل الدخول</span>
                                </a>
                                </li>
                            @else
                                <li class="nav-item dropdown" >
                                <a class="nav-link dropdown-toggle " href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$user->first_name_ar.' '.$user->last_name_ar}}
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(isset($user) && $user->hasRole('Admin'))
                                        <li ><a class="dropdown-item"  href="{{url('/dashboard')}}">لوحة التحكم</a></li>
                                        <li > <a class="dropdown-item" href="{{url('/mangment/1/edit')}}"> إدارة محتوى الصفحة الرئيسية </a></li>
                                        <li > <a class="dropdown-item"  href="{{url('/mangment_fq')}}"> إدارة الأسئلة الشائعة  </a></li>
                                        <li > <a class="dropdown-item"  href="{{url('/mangment_about/1/edit')}}"> إدارة من نحن ! </a></li>
                                        <li > <a class="dropdown-item" href="{{url('photo')}}"> إدارة  صور الدروس </a></li>
                                        <li > <a class="dropdown-item" href="{{url('/mangment_footer/1/edit')}}"> إدارة أسفل الصفحة  </a></li>
                                    @elseif(isset($user) && $user->hasRole('coach'))
                                        <li ><a class="dropdown-item" href="{{route('coach.edit',$user->id)}}">إدارة الحساب</a></li>
                                    @else
                                        <li ><a class="dropdown-item" href="{{route('trainee.edit',$user->id)}}">إدارة الحساب</a></li>

                                    @endif
                                    <li>
                                        <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">تسجيل الخروج</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                    </li>

                                </ul>
                                </li>

                            @endif
                        </ul>
                    </div>


                </nav>
            </div>
        </div>
    </div>
</header>
<!-- Header part end-->
@yield('content')
@if(isset($footer))
<footer class="footer-area" style="direction: rtl">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="single-footer-widget footer_1">
                    <a href="/"> <img src="{{URL::to('img/logo.png')}}" alt=""> </a>
                    <p>
                        {{$footer->brief}}
                    </p>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 col-md-4">
                <div class="single-footer-widget footer_2">
                    <h4>تواصلوا معنا:</h4>
                    <div class="contact_info">
                        <p><span> العنوان:</span>  {{$footer->address}} </p>
                        <p><span> الهاتف:</span> {{$footer->phone}}</p>
                        <p style="    margin-left: -60px;"><span>البريد الإلكتروني: </span> <a href="mailto:{{$footer->email}}" target="_blank" class="text-white">
                                {{$footer->email}}
                            </a></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-4">
                <div class="single-footer-widget footer_2">
                    <div class="social_icon">
                        <a href="{{asset($footer->facebook)}}" target="_blank">
                            <i class="ti-facebook"></i>
                        </a>
                        <a href="{{$footer->twitter}}" target="_blank">
                            <i class="ti-twitter-alt"></i>
                        </a>
                        <a href="{{$footer->linkedin}}" target="_blank">
                            <i class="ti-linkedin"></i>
                        </a>
                        <a href="{{$footer->instagram}}" target="_blank">
                            <i class="ti-instagram"></i>
                        </a>
                        <a href="{{$footer->youtube}}" target="_blank">
                            <i class="ti-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright_part_text text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <i class="ti-heart" aria-hidden="true"></i>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
@endif
<!-- jquery -->
<script src="{{asset("js/jquery-1.12.1.min.js")}}"></script>
<!-- popper js -->
<script src="{{asset("js/popper.min.js")}}"></script>
<!-- bootstrap js -->
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<!-- easing js -->
<script src="{{asset("js/jquery.magnific-popup.js")}}"></script>
<!-- swiper js -->
<script src="{{asset("js/swiper.min.js")}}"></script>
<!-- swiper js -->
<script src="{{asset("js/masonry.pkgd.js")}}"></script>
<!-- particles js -->
<script src="{{asset("js/owl.carousel.min.js")}}"></script>

<!-- swiper js -->
<script src="{{asset("js/slick.min.js")}}"></script>
<script src="{{asset("js/jquery.counterup.min.js")}}"></script>
<script src="{{asset("js/waypoints.min.js")}}"></script>
<!-- custom js -->
<script src="{{asset("js/custom.js")}}"></script>
@yield('scripts')
</body>

</html>
