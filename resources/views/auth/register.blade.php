@extends('layouts.app')

@section('content')
<div class="container" style="padding: 7rem 2rem;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-right text-white" style="background-color: rgb(236, 157, 76);">إنشاء حساب جديد</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">الاسم الأول بالعربي</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('first_name_ar') ? ' is-invalid' : '' }}" name="first_name_ar" value="{{ old('first_name_ar') }}" required autofocus>

                                @if ($errors->has('first_name_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name_ar" class="col-md-4 col-form-label text-md-right">االكنية بالعربي</label>

                            <div class="col-md-6">
                                <input id="last_name_ar" type="text" class="form-control{{ $errors->has('last_name_ar') ? ' is-invalid' : '' }}" name="last_name_ar" value="{{ old('last_name_ar') }}" required autofocus>

                                @if ($errors->has('last_name_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">البريد الالكتروني</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">نوع المستخدم</label>

                            <div class="col-md-6">
                                <select name="role" class="form-control" id="exampleFormControlSelect2">
                                    <option value="coach">مدرب</option>
                                    <option value="trainee">متدرب</option>
                                </select>
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">كلمة المرور</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">تأكيد كلمة المرور</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn text-white" style="background: #ec9d4c;">
                                   إنشاء
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
