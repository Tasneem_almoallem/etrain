@extends('layouts.layout')
@section('css')
    <style>
        .learning_part .learning_member_text h5:after
        {
            right: 0;
            top: 15px;
            left: auto;
        }

        .special_cource .single_special_cource .special_cource_text .author_info .author_img{
            padding-left:0;
        }

    </style>
@endsection
@section('content')
    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <p style="margin-bottom: 20%;">{{$homeContent->course_brief}}</p>
                            @if (session('messageSearch'))
                                <div class="alert alert-success">
                                    {{ session('messageSearch') }}
                                </div>
                            @endif
                            <form action="{{route('searchCat')}}" method="post">
                                @csrf
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="اسم الدورة "name="course" required>
                                </div>
                                <div class="col-md-6 mt-4">
                                    <select name="degree" class="form-control">
                                        <option value=""></option>
                                        @foreach($categories as $cat)
                                            <option value="{{$cat->category}}">{{$cat->category}}</option>
                                        @endforeach
                                    </select>
                                    <button class="boxed_btn mt-4 mr-4 text-light btn_1" type="submit">
                                        ابحث عن الدورات
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->



    <!-- learning part start-->
    <section class="learning_part">
        <div class="container">
            <div class="row align-items-sm-center align-items-lg-stretch">
                <div class="col-md-7 col-lg-7">
                    <div class="learning_img">
                        <img src="img/learning_img.png" alt="">
                    </div>
                </div>
                <div class="col-md-5 col-lg-5" style=" text-align: right;">
                    <div class="learning_member_text">
                        <h5>حول المركز</h5>
                        <h3>أكثر من {{$certificats_count}} شهادة <br>
                            صادرة عن {{$courses_count}} دورة</h3>
                        <p>{{$homeContent->course_brief}}</p>
                        <a href="{{route('course')}}" class="boxed_btn btn_1">الالتحاق بالدورات</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- learning part end-->

    <!-- member_counter counter start -->
    <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">{{$courses_count}}</span>
                        <h4>دورة</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">{{$trainee_count}}</span>
                        <h4> طالب</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">{{$coachs_count}}</span>
                        <h4>مدرب</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">{{$certificats_count}}</span>
                        <h4>شهادة</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- member_counter counter end -->

    <!--::review_part start::-->
    <section class="special_cource padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>أحدث الدورات </p>
                        <h2>المقامة في مركزنا</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($last_courses as $course)
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="{{asset('/images/'.$course->image)}}" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{url('/course-detailes/'.$course->id)}}" class="btn_4">{{$course->category}}</a>
                            <h4>{{$course->cost_individual}}ل.س</h4>
                            <a href="{{url('/course-detailes/'.$course->id)}}"><h3>{{$course->name}}</h3></a>
                            <p> {{\Illuminate\Support\Str::limit($course->description,200,'...') }} </p>
                            <div class="author_info">
                                <div class="author_img">
                                   <!-- <img src="img/author/author_1.png" alt="">-->
                                    <div class="author_info_text">
                                        @if(isset($course->coach))
                                        <p>المدرب:</p>
                                        <h5><a> {{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</a></h5>
                                            @endif
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        @for($i=0;$i<$course->rating;$i++)
                                            <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                        @endfor
                                        @for($i=$course->rating;$i<5;$i++)
                                            <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                        @endfor

                                    </div>
                                    <p>({{$course->rating}})</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!-- learning part start-->
    <section class="advance_feature learning_part" style="text-align: right">
        <div class="container">
            <div class="row align-items-sm-center align-items-xl-stretch">
                <div class="col-md-6 col-lg-6">
                    <div class="learning_member_text">
                        <h5>من نحن!</h5>
                        <h2>مركزنا وخدماتنا غير محدودة</h2>
                        <p>{{$about_us->brief}}</p>
                        <div class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <div class="learning_member_text_iner">
                                    <span class="ti-pencil-alt"></span>
                                    <h4>هدف المركز</h4>
                                    <p>{{$about_us->goal}}</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <div class="learning_member_text_iner">
                                    <span class="ti-stamp"></span>
                                    <h4>رؤية المركز</h4>
                                    <p>{{$about_us->vision}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="learning_img">
                        <img src="img/advance_feature_img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- learning part end-->

    <!--::review_part start::-->
    <section class="testimonial_part">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p> الأساتذة </p>
                        <h2>المميزين لهذا الشهر</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                        @if($homeContent->coach1)
                    <div class="col-sm-6 col-lg-3 col-xl-3">
                        <div class="single-home-blog">
                            <div class="card">
                                <img src="{{asset('images/'.$homeContent->coach1->image)}}" class="card-img-top" alt="blog">
                                <div class="card-body">


                                    <h5 class="card-title">{{$homeContent->coach1->first_name_ar.' '.$homeContent->coach1->last_name_ar}}</h5>

                                </div>
                            </div>
                        </div>
                    </div>

                        @endif
                        @if($homeContent->coach2)
                                <div class="col-sm-6 col-lg-3 col-xl-3">
                                    <div class="single-home-blog">
                                        <div class="card">
                                            <img src="{{asset('images/'.$homeContent->coach2->image)}}" class="card-img-top" alt="blog">
                                            <div class="card-body">


                                                <h5 class="card-title">{{$homeContent->coach2->first_name_ar.' '.$homeContent->coach2->last_name_ar}}</h5>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if($homeContent->coach3)
                                <div class="col-sm-6 col-lg-3 col-xl-3">
                                    <div class="single-home-blog">
                                        <div class="card">
                                            <img src="{{asset('images/'.$homeContent->coach3->image)}}" class="card-img-top" alt="blog">
                                            <div class="card-body">


                                                <h5 class="card-title">{{$homeContent->coach3->first_name_ar.' '.$homeContent->coach3->last_name_ar}}</h5>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($homeContent->coach4)
                                <div class="col-sm-6 col-lg-3 col-xl-3">
                                    <div class="single-home-blog">
                                        <div class="card">
                                            <img src="{{asset('images/'.$homeContent->coach4->image)}}" class="card-img-top" alt="blog">
                                            <div class="card-body">


                                                <h5 class="card-title">{{$homeContent->coach4->first_name_ar.' '.$homeContent->coach4->last_name_ar}}</h5>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif


            </div>
        </div>
    </section>
    <!--::blog_part end::-->
    @if(isset($recommendations)&&count($recommendations)>0)
    <section class="special_cource padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p> دورات مقترحة</p>
                        <h2>قد تناسب إهتماماتك</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($recommendations as $course)
                    <div class="col-sm-6 col-lg-4">
                        <div class="single_special_cource">
                            <img src="{{asset('/images/'.$course->image)}}" class="special_img" alt="">
                            <div class="special_cource_text">
                                <a href="{{url('/course-detailes/'.$course->id)}}" class="btn_4">{{$course->category}}</a>
                                <h4>{{$course->cost_individual}}ل.س</h4>
                                <a href="{{url('/course-detailes/'.$course->id)}}"><h3>{{$course->name}}</h3></a>
                                <p> {{\Illuminate\Support\Str::limit($course->description,200,'...') }} </p>
                                <div class="author_info">
                                    <div class="author_img">
                                        <!-- <img src="img/author/author_1.png" alt="">-->
                                        <div class="author_info_text">
                                            <p>المدرب:</p>
                                            <h5><a > {{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</a></h5>
                                        </div>
                                    </div>
                                    <div class="author_rating">
                                        <div class="rating">
                                            @for($i=0;$i<$course->rating;$i++)
                                                <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                            @endfor
                                            @for($i=$course->rating;$i<5;$i++)
                                                <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                            @endfor

                                        </div>
                                        <p>({{$course->rating}})</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
    @endif
    <!--::blog_part start::-->
    <section class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <h2>صور من الدورات المقامة في المركز</h2>
                        <p>معرض صور  مأخوذة من الدورات والدروس المعطاة من قبل الأساتذة ضمن المركز وبعض النشاطات</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($photos as $photo)
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-blog">
                        <div class="card">
                            <img src="{{asset('images/'.$photo->image)}}" class="card-img-top" alt="blog">
                            <div class="card-body">


                                    <h5 class="card-title">{{$photo->title}}</h5>
                                <ul>
                                    <li> <span class="ti-calendar"></span>{{$photo->created_at}}</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--::blog_part end::-->
@endsection

