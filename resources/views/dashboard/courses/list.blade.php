@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list"> قائمة الدورات  </h4>
                    
                    <form
                        
                        action="{{route('courses.search')}}"
                        
                       
                        method="POST"
                        class="navbar-form seach-list">
                        {{ csrf_field() }}
                        <div class="input-group no-border">
                            <input type="text" class="form-control" placeholder="ابحث عن الدورات أو فئاتها" name="q"
                                   @if(isset($query)) value="{{$q}}" @endif>
                            <button type="submit" class="btn btn-search btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                    <a
                       
                        href="{{url('/courses/create')}}"
                        
                        
                       style="float: left" alt="  إضافة دورة">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> اسم  الدورة </th>
                        <th>مدة الدورة  </th>
                        <th>فئة الدورة  </th>
                        <th>العمليات  </th>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$course->name}}</td>
                                <td>{{$course->period}}</td>
                                <td>{{$course->category}} </td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('courses.edit',$course->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form method ="post"
                                          
                                          action="{{route('courses.destroy',$course->id)}}"
                                          
                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button class="btn btn-delete float-right text-danger" onclick="confirmation()">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $courses->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>
@endsection
