@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
         @if($course->id)
            action="{{url('/courses/'.$course->id)}}"
             @else action="{{route('courses.store')}}"
              @endif
               enctype="multipart/form-data"
             method="POST"
            class="add-form">
              @if(isset($course->id))
                @method('PUT')
            @endif


            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($course->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم الدورة </label>
                                    <input type="text" class="form-control" name="name"
                                           @if($course && $course->name) value="{{$course->name}}"
                                           @else value="{{ old('name') }}" @endif
                                       required>
                                </div>
                            </div>
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> وصف الدورة  </label>
                                    <textarea class="form-control" name="description"
                                              rows="6">
                                        @if($course && $course->description)  {{$course->description}}
                                           @else
                                           {{ old('description') }}
                                            @endif
                                    </textarea>

                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> مدة الدورة  </label>
                                    <input type="text" class="form-control" name="period"
                                    @if($course && $course->period) value="{{$course->period}}"
                                           @else  value="{{ old('period') }}" @endif

                                            required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> التكلفة للأفراد </label>
                                    <input type="text" class="form-control" name="cost_individual"
                                    @if($course && $course->cost_individual) value="{{$course->cost_individual}}"
                                           @else  value="{{ old('cost_individual') }}" @endif
                                     required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> التكلفة للطلاب </label>
                                    <input type="text" class="form-control" name="cost_student"
                                    @if($course && $course->cost_student) value="{{$course->cost_student}}"
                                           @else  value="{{ old('cost_student') }}" @endif >
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> التكلفة للمؤسسات  </label>
                                    <input type="text" class="form-control" name="cost_company"
                                     @if($course && $course->cost_company) value="{{$course->cost_company}}"
                                           @else  value="{{ old('cost_company') }}" @endif
                                      required>
                                </div>
                            </div>

                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> العدد الأعظمي للطلاب المسموح  </label>
                                    <input type="text" class="form-control" name="max_num"
                                    @if($course && $course->max_num) value="{{$course->max_num}}"
                                           @else  value="{{ old('max_num') }}" @endif
                                              required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">  تاريخ بدء الدورة </label>
                                    <input type="date" class="form-control" id="startDate" name="date_of_start"
                                          @if($course && $course->date_of_start) value="{{$course->date_of_start}}"
                                           @else  value="{{ old('date_of_start') }}" @endif
                                            required>
                                </div>
                            </div>

                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> تاريخ انتهاء الدورة  </label>
                                    <input type="date" class="form-control" id="endDate" name="date_of_end"
                                    @if($course && $course->date_of_end) value="{{$course->date_of_end}}"
                                           @else  value="{{ old('date_of_end') }}" @endif
                                      required>
                                </div>
                            </div>
                             <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">  رمز الدورة </label>
                                    <input type="text" class="form-control" name="code"
                                           value="{{$course->code}}"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> مدة الجلسة </label>
                                    <input type="text" class="form-control" id="duration" name="session_duration"
                                       @if($course && $course->session_duration) value="{{$course->session_duration}}"
                                           @else  value="{{ old('session_duration') }}" @endif
                                     required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> رقم القاعة  </label>
                                    <input type="text" class="form-control" id="room" name="num_room"
                                           @if($course && $course->num_room) value="{{$course->num_room}}"
                                           @else  value="{{ old('num_room') }}" @endif
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> أوقات الجلسة </label>
                                    <select id="courseDate" disabled class="form-control">
                                        @if($course && $course->time_of_start_session&& $course->time_of_end_session)
                                            <option>{{$course->time_of_start_session." - ".$course->time_of_end_session}}</option>
                                            @endif
                                    </select>
                                    <input type="hidden" name="time_of_start_session"
                                       @if($course && $course->time_of_start_session) value="{{$course->time_of_start_session}}"
                                           @else  value="{{ old('time_of_start_session') }}" @endif>
                                    <input type="hidden" name="time_of_end_session" @if($course && $course->time_of_end_session) value="{{$course->time_of_end_session}}"
                                           @else  value="{{ old('time_of_end_session') }}" @endif>
                                </div>
                            </div>
                             {{--<div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> وقت انتهاء الجلسة  </label>
                                    <input type="text" class="form-control" name="time_of_end_session"
                                        @if($course && $course->time_of_end_session) value="{{$course->time_of_end_session}}"
                                           @else  value="{{ old('time_of_end_session') }}" @endif
                                             required>
                                </div>
                            </div>--}}
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> أيام الدورة </label>
                                    <input type="text" class="form-control" name="days_of_session"
                                        @if($course && $course->days_of_session) value="{{$course->days_of_session}}"
                                           @else  value="{{ old('days_of_session') }}" @endif
                                             required>
                                </div>
                            </div>

                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> المدرب  </label>

                                   <select  name="coach_id" class="form-control" required>
                                       <option value=''></option>
                                      @foreach($coachs as $coach)
                                       <option value='{{$coach->id}}' @if($course && $course->coach_id==$coach->id) selected @endif>
                                       {{$coach->first_name_ar.' '.$coach->last_name_ar}}
                                       </option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> كلفة ساعة المدرب  </label>
                                    <input type="text" class="form-control" name="cost_hour_coach"
                                          @if($course && $course->cost_hour_coach) value="{{$course->cost_hour_coach}}"
                                           @else  value="{{ old('cost_hour_coach') }}" @endif
                                             required>
                                </div>
                            </div>
                             <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> كلفة المدرب أو المدربين الكلية </label>
                                    <input type="text" class="form-control" name="cost_total_coach"
                                        @if($course && $course->cost_total_coach) value="{{$course->cost_total_coach}}"
                                           @else  value="{{ old('cost_total_coach') }}" @endif
                                             required>
                                </div>
                            </div>
                             <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> كلفة نسبة المدرب </label>
                                    <input type="text" class="form-control" name="cost_rate_coach"
                                          @if($course && $course->cost_rate_coach) value="{{$course->cost_rate_coach}}"
                                           @else  value="{{ old('cost_rate_coach') }}" @endif
                                             required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> فئة الدورة </label>
                                    <select class="form-control" name="category_id" required>
                                        <option></option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if($course && $course->category_id==$category->id) selected @elseif(old('category_id')==$category->id) selected @endif> {{$category->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{url('courses')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-md-4 pt-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="javascript:;">
                            @if($course && $course->image)
                                <img class="img user-img " id="course-img" src="{{asset('/images/'.$course->image)}}"/>
                                 @else
                                <img class="img user-img" id="course-img" src="{{asset('imgd/faces/marc.jpg')}}"/>

                            @endif
                            <div class="add-img-icon">
                                <div class="choose_file">
                                  <span class="material-icons">
                                    center_focus_weak
                                    </span>
                                    <input name="image" id="courseImg" type="file" accept="image/*"
                                           onchange="putImage()"/>
                                </div>
                            </div>

                        </a>
                    </div>

                <hr>
                    <div class="col-md-12 pt-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="bmd-label-floating"> الملاحظات : </label>
                                <textarea class="form-control" rows="5">
                                    @if($course && $course->notes)
                                        {{$course->notes}}
                                    @else
                                        {{ old('notes') }}
                                    @endif
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function showImage(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putImage() {
            var src = document.getElementById("courseImg");
            var target = document.getElementById("course-img");
            showImage(src, target);
        }

        function showCV(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.value(fr.result);
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putCV() {
            var src = document.getElementById("userCv");
            var target = document.getElementById("user-cv");
            showCV(src, target);
        }
    </script>
    <script>
        $(document).ready(function(){
            $('#room').keyup(function(){

                if($('#startDate').val()=='')
                    alert('الرجاء إدخال تاريخ بدء الدورة');
                else if($('#endDate').val()=='')
                    alert('الرجاء إدخال تاريخ إنتهاء الدورة');
                else if($('#duration').val()=='')
                    alert('الرجاء إدخال مدة الجلسة');
                else if($('#room').val()=='')
                    alert('الرجاء إدخال رقم القاعة');
                else{
                    $.ajax({
                        type:"post",
                        url:"/getAvailableTime",
                        dataType:'json',
                        data:{_token:"{{csrf_token()}}",startDate:$('#startDate').val(),endDate:$('#endDate').val(),duration:$('#duration').val(),room:$('#room').val()},
                        success:function (data) {
                            if(data.times.length>0)
                            {
                                var options='';
                                $(data.times).each(function (i,e) {
                                    options+='<option>'+e+'</option>'
                                })
                                $('#courseDate').html(options);
                                $('#courseDate').removeAttr('disabled');
                            }
                            else alert("لا يوجد أوقات متاحة في هذا التاريخ")
                        }

                    })
                }
            });
            $('#startDate,#endDate,#duration').change(function(){
                if($('#room').val()!=''&&$('#startDate').val()!=''&&$('#duration').val()!=''&&$('#endDate').val()!='')
                {
                    $('#courseDate').attr('disabled',true);
                    $.ajax({
                        type:"post",
                        url:"/getAvailableTime",
                        dataType:'json',
                        data:{_token:"{{csrf_token()}}",startDate:$('#startDate').val(),endDate:$('#endDate').val(),duration:$('#duration').val(),room:$('#room').val()},
                        success:function (data) {
                            if(data.times.length>0)
                            {
                                var options='';
                                $(data.times).each(function (i,e) {
                                    options+='<option>'+e+'</option>'
                                })
                                $('#courseDate').html(options);
                                $('#courseDate').removeAttr('disabled');
                            }
                            else alert("لا يوجد أوقات متاحة في هذا التاريخ")
                        }

                    })
                }

            })
            $('#courseDate').change(function(){
                var time=$(this).val().split('-');
                $('input[name="time_of_start_session"]').val(time[0]);
                $('input[name="time_of_end_session"]').val(time[1])
            });
        });
    </script>

@endsection
