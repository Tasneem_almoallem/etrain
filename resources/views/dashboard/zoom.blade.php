<!DOCTYPE html>

<head>
    <title>Live Course</title>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.8.6/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.8.6/css/react-select.css" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

</head>

<body>
<style>
    .sdk-select {
        height: 34px;
        border-radius: 4px;
    }

    .websdktest button {
        float: right;
        margin-left: 5px;
    }

    #nav-tool {
        margin-bottom: 0px;
    }

    #show-test-tool {
        position: absolute;
        top: 100px;
        left: 0;
        display: block;
        z-index: 99999;
    }

    #display_name {
        width: 250px;
    }


    #websdk-iframe {
        width: 700px;
        height: 500px;
        border: 1px;
        border-color: red;
        border-style: dashed;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        left: 50%;
        margin: 0;
    }
</style>

<nav id="nav-tool" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Zoom WebSDK</a>
        </div>
        <div id="navbar" class="websdktest">
            <form class="navbar-form navbar-right" id="meeting_form">
                <input type="text" name="display_name" id="display_name" value="{{Auth::user()->first_name_ar." ".Auth::user()->last_name_ar}}" maxLength="100"
                       placeholder="Name" class="form-control" required hidden>
                <input type="text" name="meeting_number" id="meeting_number" value="{{$meeting->meeting_id}}" maxLength="200"
                       style="width:150px" placeholder="Meeting Number" class="form-control" required hidden>
                <input type="text" name="meeting_pwd" id="meeting_pwd" value="{{$meeting->password}}" style="width:150px"
                       maxLength="32" placeholder="Meeting Password" class="form-control" hidden>
                <input type="text" name="meeting_email" id="meeting_email" value="{{Auth::user()->email}}" style="width:150px"
                       maxLength="32" placeholder="Email option" class="form-control" hidden>
                <input id="meeting_role" class="sdk-select" hidden value="{{$role}}">
                <input id="meeting_china" class="sdk-select" hidden value="0">
                <input id="meeting_lang" class="sdk-select" hidden value="en-US">

                <input type="hidden" value="" id="copy_link_value" />
                <button type="submit" class="btn btn-primary" id="join_meeting">Join</button>
                <button type="submit" class="btn btn-primary" id="clear_all">Clear</button>
                <button type="button" link="" onclick="window.copyJoinLink('#copy_join_link')"
                        class="btn btn-primary" id="copy_join_link">Copy Direct join link</button>

            </form>
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>

<script>


    document.getElementById('nav-tool').style.display = 'none';

    //document.getElementById('join_meeting').click();

</script>
<script src="https://source.zoom.us/1.8.6/lib/vendor/react.min.js"></script>
<script src="https://source.zoom.us/1.8.6/lib/vendor/react-dom.min.js"></script>
<script src="https://source.zoom.us/1.8.6/lib/vendor/redux.min.js"></script>
<script src="https://source.zoom.us/1.8.6/lib/vendor/redux-thunk.min.js"></script>
<script src="https://source.zoom.us/1.8.6/lib/vendor/lodash.min.js"></script>
<script src="https://source.zoom.us/zoom-meeting-1.8.6.min.js"></script>
<script src="{{asset('js/tool.js')}}"></script>
<script src="{{asset('js/vconsole.min.js')}}"></script>
<script src="{{asset('js/index.js')}}"></script>


</body>

</html>
