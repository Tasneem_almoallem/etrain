@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list">  إدارة أسفل الصفحة  </h4>
                    
                   
                </div>
                
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> الهاتف </th>
                        <th>  الإيميل </th>
                        <th>العنوان </th>
                        
                        <th>رابط الفيس بوك </th>
                        <th>رابط التويتر </th>
                        <th>رابط النستغرام </th>
                        <th> رابط اليوتيوب </th>
                        <th>رابط اللينكد ان </th> 
                        </thead>
                        <tbody>
                        @foreach($footers as $footer)
                            <tr>
                                <td>{{$footer->phone}}</td>
                                <td>{{$footer->email}} </td>
                                <td>{{$footer->address}} </td>
                                
                                <td>{{$footer->facebook}} </td>
                                <td>{{$footer->twitter}} </td>
                                <td>{{$footer->instagram}} </td>
                                <td>{{$footer->youtube}} </td>
                                <td>{{$footer->linkedin}} </td>
                                </tr>
                                <tr>
                                <th>موجز المركز </th>
                                <td>{{$footer->brief}}</td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('mangment_footer.edit',$footer->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    
                                                                    </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    

@endsection
