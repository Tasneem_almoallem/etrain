@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        @if (session('msg'))
                            <div class="alert alert-success">
                                {{ session('msg') }}
                            </div>
                        @endif
        <form 
            action="{{url('/mangment_footer/'.$footer->id)}}" 
              enctype="multipart/form-data"
             method="post" 
            class="add-form" style="width: 100%">
             @method('PUT')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{csrf_field()}}
                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating"> نبذة عن الموقع</label>
                                    <textarea rows="5" type="text" class="form-control" name="brief"
                                          required> {{$footer->brief }}"</textarea>
                                </div>
                            
                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">الهاتف </label>
                                    <input type="text" class="form-control" name="phone"
                                      value="{{ $footer->phone }}" dir="ltr" style="text-align: right" 
                                    
                                            required>
                                </div>
                            
                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">  الإيميل </label>
                                    <input type="text" class="form-control" name="email"
                                           value="{{ $footer->email }}" 
                                            required>
                                </div>
                           
                         <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">  العنوان  </label>
                                    <input type="text" class="form-control" name="address"
                                           value="{{ $footer->address }}" 
                                            required>
                                </div>

                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">رابط الفيس </label>
                                    <input type="text" class="form-control" name="facebook"
                                           value="{{ $footer->facebook }}" 
                                            required>
                                </div>
                            
                             <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">رابط التويتر </label>
                                    <input type="text" class="form-control" name="twitter"
                                           value="{{ $footer->twitter }}" 
                                            required>
                                </div>
                            </div>

                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">رابط  الانستغرام </label>
                                    <input type="text" class="form-control" name="instagram"
                                           value="{{ $footer->instagram }}" 
                                            required>
                                </div>

                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating"> رابط اليوتيوب</label>
                                    <input type="text" class="form-control" name="youtube"
                                           value="{{ $footer->youtube }}" 
                                            required>
                                </div>

                            <div class="col-md-12 pt-4"
                                <div class="form-group">
                                    <label class="bmd-label-floating">رابط اللينكد ان </label>
                                    <input type="text" class="form-control" name="linkedin"
                                           value="{{ $footer->linkedin }}" 
                                            required>
                                </div>
                            <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        
                        <a type="btn" class="btn btn-default pull-right" href="{{url('mangment_footer')}}" >
                            إلغاء
                        </a>
                        </div>
                        
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
    </div>
   

@endsection
