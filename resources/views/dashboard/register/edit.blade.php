@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form  
        
              action="{{url('/user_courses/'.$usercourse->id)}}"
              method="post" 
               enctype="multipart/form-data"
             
            class="add-form">
               
                @method('PUT')
        
            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                   <div class="card-header card-header-primary">
                        @if(!isset($usercourse->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div> 
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label  for="national_id" class="bmd-label-floating "> اختر الرقم الوطني </label>
                                    <select class="form-control btn btn-secondary dropdown-toggle"  id="national_id"  name="national_id" >
                                      <option value=""></option>  
                                      @foreach($users as $user)
                                     <option @if($user->national_id == $usercourse->national_id) selected  @endif value="{{$user->national_id}}">{{$user->national_id}}</option>
                                       @endforeach
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for=""> اختر اسم الدورة  </label>
                                    <select class="form-control btn btn-secondary dropdown-toggle"  name="code_course" id="code_course" >
                                      <option value="{{$usercourse->code_course}}"></option> 
                                        @foreach($courses as $course)
                                       <option @if($course->code == $usercourse->code_course) selected  @endif value="{{$course->code}}">{{$course->name}}</option>
                                         @endforeach
                                </select>
                                 
                                </div>

                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="type">اختر صفة المتدرب </label>
                                     <select name="type" id="type" class="form-control btn btn-secondary dropdown-toggle">
                                
                                 
                                      <option @if ($usercourse->type == 'طالب' ) selected @endif value="طالب" >طالب  </option>
                                      <option @if ($usercourse->type == 'فرد' ) selected  @endif value="فرد" >فرد</option>
                                      <option  @if ( $usercourse->type == 'مؤسسة' ) selected @endif value="مؤسسة">مؤسسة</option>                               
                                  </select>
                                </div>
                            </div>


                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب الكامل بالعربي  </label>
                                    <input type="text" class="form-control" name="name_trainee_ar" id="name_trainee_ar" value="{{$usercourse->name_trainee_ar}}" 
                                        >

                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب بالإنكليزي </label>
                                    <input type="text" class="form-control" name="name_trainee_en" id="name_trainee_en" value="{{$usercourse->name_trainee_en}}" 
                                    >
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> رمز المتدرب </label>
                                    <input type="text" class="form-control" name="code_trainee" id="code_trainee" value="{{$usercourse->code_trainee}}" 
                                    >
                                </div>
                            </div>
                           
                           

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> قيمة الحسم </label>
                                    <input type="text" class="form-control" name="discount_value"
                                     value="{{$usercourse->discount_value}}" 
                                              required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">   سبب الحسم </label>
                                    <input type="text" class="form-control" name="cause_discount"
                                           value="{{$usercourse->cause_discount}}" 
                                            required>
                                </div>
                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    
                                    <input type='hidden' class="form-control" name="course_name" id="course_name" value="{{$usercourse->course_name}}" 
                                    >
                                </div>
                            </div>
                      
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        
                        <a type="btn" class="btn btn-default pull-right" href="{{url('user_courses')}}" >
                            إلغاء
                        </a>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-md-4 pt-4">
                <div class="card card-profile">
                   
                    
                
                    <div class="col-md-12 pt-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="bmd-label-floating"> الملاحظات : </label>
                                <input type="text"  class="form-control" rows="5" name="notes" 
                                    
                               value="{{$usercourse->notes}}" >
                                  
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                            
        </form>
    </div>
    <script>

    $(document).ready(function(){
       $('#national_id').change(function(){
        

    var id = $(this).val();
    console.log(id);
    var url = '{{ route("getDetails", ":id") }}';
    
    
    url = url.replace(':id', id);

    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(response){
            if(response != null){
                $('#name_trainee_ar').val(response[0].first_name_ar + " "+ response[0].middle_name_ar +" "+ response[0].last_name_ar );
                $('#name_trainee_en').val(response[0].first_name_en +response[0].middle_name_en + " "+ response[0].last_name_en);
                $('#code_trainee').val(response[0].code);
            }
        }
    });
});
     });

    $(document).ready(function(){
       $('#code_course').change(function(){
        
         console.log('ammmmm');

    var id = $(this).val();
    console.log(id);
    var url = '{{ route("getValue", ":id") }}';
    
    
    url = url.replace(':id', id);

    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(response){
            if(response != null){
                $('#course_name').val(response[0].name);
            }
        }
    });
});
     });
    </script>

@endsection
