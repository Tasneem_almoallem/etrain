@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">  قائمة التسجيلات </h4>

                    <form

                        action="{{route('user_courses.search')}}"


                        method="POST"
                        class="navbar-form seach-list">
                        {{ csrf_field() }}
                        <div class="input-group no-border">
                            <input type="text" class="form-control" placeholder="ابحث..." name="q"
                                   @if(isset($query)) value="{{$q}}" @endif>
                            <button type="submit" class="btn btn-search btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>

                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> الرقم الوطني </th>
                        <th>اسم المتدرب بالعربي  </th>
                        <th>اسم المتدرب  بالإنكليزي </th>
                        <th>اسم الدورة </th>
                        <th> العمليات </th>
                        </thead>
                        <tbody>

                            <tr>
                                 @foreach($usercourses as $usercourse)
                                <td>{{$usercourse->national_id}}</td>
                                <td>{{$usercourse->name_trainee_ar}}</td>
                                <td>{{$usercourse->name_trainee_en}} </td>
                                <td>{{$usercourse->course_name}} </td>

                                <td style="display: flex">
                                   <a class="btn btn-primary float-right col-3 btn-edit text-success"

                                       href="{{route('user_courses.edit',$usercourse->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                   <form method ="post"

                                          action="{{route('user_courses.destroy',$usercourse->id)}}"

                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button class="btn btn-delete float-right text-danger" onclick="confirmation()">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form>

                                </td>
                            </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
                 {{ $usercourses->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif

            </div>
        </div>
    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>
@endsection
