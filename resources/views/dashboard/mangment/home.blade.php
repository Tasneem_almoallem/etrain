@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list">   إدارة محتوى الصفحة الرئيسية  </h4>
                    
                   
                                    </div>
                
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> نبذة عن الموقع </th>
                        <th> رقم المدرب الأول </th>
                        <th> رقم المدرب الثاني </th>
                        <th>رقم المدرب الثالث </th>
                        <th >رقم المدرب الرابع </th>
                        </thead>
                        <tbody>
                        @foreach($homes as $home)
                            <tr id='printTable'>
                                <td>{{$home->course_brief}}</td>
                                <td>{{$home->coach1_id}} </td>

                                <td>{{$home->coach2_id}}</td>
                                <td>{{$home->coach3_id}}</td>
                                <td>{{$home->coach4_id}}</td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('mangment.edit',$home->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    
                                                                    </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    

@endsection
