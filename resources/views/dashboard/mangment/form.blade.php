@extends('dashboard.layout.master')
@section('content')
    <div class="row">
             @if (session('msg'))
                            <div class="alert alert-success">
                                {{ session('msg') }}
                            </div>
                        @endif
        <form 
            action="{{url('/mangment/'.$home->id)}}" 
              enctype="multipart/form-data"
             method="post" 
            class="add-form">
             @method('PUT')
            <div class="col-md-12">
                <div class="card">
                   
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> نبذة عن الموقع</label>
                                    <textarea type="text" rows="5" class="form-control" name="course_brief"
                                          required> {{$home->course_brief }}"</textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> المدرب الأول  </label>
                    
                                    <select class="form-control" name="coach1" required>
                                        <option value=""></option>
                                      @foreach($coachs as $coach)
                                        <option value="{{$coach->code}}"
                               
                                                 @if(isset($home->coach1) && $coach->code==$home->coach1->code) selected @endif
                                        >{{$coach->first_name_ar.' '.$coach->last_name_ar}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">  المدرب الثاني </label>
                                   
                                    <select class="form-control" name="coach2" required>
                                        <option value=""></option>
                                        @foreach($coachs as $coach)
                                        <option value="{{$coach->code}}"
                                        @if(isset($home->coach2) && $coach->code==$home->coach2->code) selected @endif
                                        
                                        >{{$coach->first_name_ar.' '.$coach->last_name_ar}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                           
                         <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">  المدرب الثالث </label>
                                    
                                    <select class="form-control" name="coach3" required>
                                        <option value=""></option>
                                       @foreach($coachs as $coach)
                                        <option value="{{$coach->code}}"
                                        @if(isset($home->coach3) && $coach->code==$home->coach3->code) selected @endif
                                        
                                        >{{$coach->first_name_ar.' '.$coach->last_name_ar}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">المدرب  الرابع </label>
                                    
                                    <select class="form-control" name="coach4" required>
                                        <option value=""></option>
                                     @foreach($coachs as $coach)
                                        <option value="{{$coach->code}}"
                                        @if(isset($home->coach4) && $coach->code==$home->coach4->code) selected @endif
                                        
                                        >{{$coach->first_name_ar.' '.$coach->last_name_ar}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        
                        <a type="btn" class="btn btn-default pull-right" href="{{url('mangment')}}" >
                            إلغاء
                        </a>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            
        </form>
    </div>
   

@endsection
