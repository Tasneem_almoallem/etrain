@extends('dashboard.layout.master')
@section('content')
    <style>
        .dropzone .dz-preview .dz-image img{
            object-fit: cover;
            width: 120px;
            height: 120px;
        }

    </style>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">قائمة ملفات الدرس</h4>

                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    @if(isset($course) && $course->getMedia('documents'))
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th>اسم الملف</th>
                        <th>نوع الملف</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($course->getMedia('documents') as $file)
                            <tr>
                                <td>{{$file->file_name}}</td>
                                <td>{{$file->mime_type}}</td>
                                <td><a class="btn btn-info btn-sm" href="{{URL::to("media/".$file->id."/".$file->file_name)}}" target="_blank"><i class="material-icons">remove_red_eye</i></a></td>

                              <hr>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                <form
            action="{{url('/files')}}"
            method="post"
            enctype="multipart/form-data"
            class="add-form">
                     {{csrf_field()}}
                     <input type="hidden" value="{{$course_id}}" name="course_id">
                    <div class="col-md-12 pt-4">
                <div class="card card-profile">
                   <h3>أضف ملفات جديدة</h3>

                                                            <div class="card-body">

                                                                <div class="form-group row">
                                                                    <div  style="margin-right: 70px"class="col-sm-10 dropzone needsclick" id="document-dropzone" style="height: fit-content; min-height: 215px;">

                                                                    </div>

                                                                </div>
                        <button type="submit" class="btn btn-info">حفظ</button>
                    </div>


                </div>
            </div>

                </form>
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
       <script>
        function showImage(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putImage() {
            var src = document.getElementById("userImg");
            var target = document.getElementById("user-img");
            showImage(src, target);
        }

        function showCV(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.value(fr.result);
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putCV() {
            var src = document.getElementById("userCv");
            var target = document.getElementById("user-cv");
            showCV(src, target);
        }
    </script>
@endsection
@section('scripts')
    <script>

        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('course.storeMedia') }}',
            maxFilesize: 500, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="files[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('form').find('input[name="files[]"][value="' + name + '"]').remove()

                $.ajax({
                    type: 'POST',
                    url: '{{route('course.deleteMedia')}}',
                    data: {name: name,__token:'{{csrf_token()}}'},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
            },
            init: function () {

                    @if(isset($course) && $course->getMedia('documents'))
                var files =
                {!! json_encode($course->getMedia('documents')) !!}
                    for (var i in files) {
                    var file = files[i]

                    this.options.addedfile.call(this, file,'{{URL::to("media")}}'+"/"+file.id+"/"+file.file_name)
                    if(file.mime_type.startsWith('image/',0))
                       this.options.thumbnail.call(this,file,'{{URL::to("media")}}'+"/"+file.id+"/"+file.file_name);

                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="files[]" value="' + file.file_name + '">')
                }
                @endif
            }

        }
    </script>
@endsection
