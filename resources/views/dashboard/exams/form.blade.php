@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
            @if(isset($exam)&&$exam->id)
            action="{{route('exam.update',[$exam->id])}}"
            @else action="{{route('exam.store')}}"
            @endif
            enctype="multipart/form-data"
            method="POST"
            class="add-form w-100">
            @if(isset($exam)&&isset($exam->id))
                @method('PATCH')
            @endif


            <div class="col-md-12">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($exam->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> عنوان الإختبار </label>
                                    <input type="text" class="form-control" name="title"
                                           @if(isset($exam) && $exam->title) value="{{$exam->title}}"
                                           @else value="{{ old('title') }}" @endif
                                           required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> الدورة  </label>

                                    <select  name="course_id" class="form-control" required>
                                        <option value=''></option>
                                        @foreach(Auth::user()->coachCourses as $course)
                                            <option value='{{$course->id}}' @if(isset($exam) && $exam->course_id==$course->id) selected @endif>
                                                {{$course->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> درجة الصعوبة  </label>
                                    <select  name="difficult" class="form-control" required>
                                        <option value='easy' @if(isset($exam) && $exam->difficult=='easy') selected @endif>مبتدئ</option>
                                        <option value='medium' @if(isset($exam) && $exam->difficult=='medium') selected @endif >متوسط</option>
                                        <option value='hard' @if(isset($exam) && $exam->difficult=='hard') selected @endif>متقدم</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12" id="questions">
                            <h4 class="d-block">أسئلة الإختبار</h4>
                                @if(isset($exam))
                                    @php
                                        $answerIndex = 0
                                    @endphp
                                    @foreach($exam->questions as $question)
                                        <div class=" w-100 row">
                                            <div class="col-md-4 pt-4">
                                              <div class="form-group">
                                                    <label class="bmd-label-floating">نص السؤال </label>
                                                     <textarea class="form-control" rows="4"  name="questions[{{$loop->index}}][text]">{{$question['text']}}</textarea>
                                              </div>
                                            </div>
                                            <div class="col-md-4 pt-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">علامة السؤال </label>
                                                    <input type="number"  max="100" min="1" class="form-control mt-5" value="{{$question['marks']}}" name="questions[{{$loop->index}}][mark]"  required>
                                                    </div>
                                                </div>
                                            <div class="col-1 pt-4 mt-5 ml-1">
                                                <button type="button" class=" btn btn-sm btn-danger d-inline-block" onclick="$(this).parent().parent().remove()"><i class="material-icons">remove_circle</i></button>
                                            </div>
                                            <div class="col-2 pt-4 mt-5">

                                                <button type="button" class=" btn btn-info d-inline-block " onclick="addChoice($(this).parent().next(),'{{$loop->index}}')">إضافة خيارات الإجابات</button>
                                             </div>

                                                <div class="answers  col-12">
                                                <h4>خيارات السؤال:</h4>
                                                @foreach($question->answers as $answer)
                                                    @php
                                                        $answerIndex++;
                                                    @endphp
                                                    <div class=" w-100 row">
                                                        <div class="col-md-4 ">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">نص الإختيار </label>
                                                                <input type="text" class="form-control" value="{{$answer['text']}}" name="questions[{{$loop->parent->index}}][choice][{{$loop->index}}][text]">
                                                                </div>
                                                           </div>
                                                        <div class="col-md-2 ">
                                                            <div class="form-group">
                                                                <input type="checkbox"  value="1"  name="questions[{{$loop->parent->index}}][choice][{{$loop->index}}][correct]" {{$answer->isCorrect?"checked":''}}>
                                                                <label class="bmd-label-floating">الإجابة الصحيحة؟ </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 ">
                                                            <button class="btn btn-sm btn-danger " onclick="$(this).parent().parent().remove()"><i class="material-icons">remove_circle</i></button>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-2 pt-4">
                            <button type="button" class="btn btn-warning " onclick="addQuestion($(this).parent())">إضافة سؤال</button>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{route('exam.index')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>

        </form>
    </div>

<script>
    var index="{{isset($exam)?count($exam->questions):0}}",index2="{{isset($answerIndex)?$answerIndex:0}}";
    function addQuestion(div) {

        $('#questions').append('<div class=" w-100 row">\n' +
            '                            <div class="col-md-4 pt-4">\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="bmd-label-floating">نص السؤال </label>\n' +
            '                                    <textarea class="form-control" rows="4"  name="questions['+index+'][text]"></textarea>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="col-md-4 pt-4">\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="bmd-label-floating">علامة السؤال </label>\n' +
            '                                    <input type="number"  max="100" min="1" class="form-control mt-5" name="questions['+index+'][mark]"\n' +
            '                                           @if($course && $course->category) value="{{$course->category}}"\n' +
            '                                           @else  value="" @endif\n' +
            '                                           required>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '   <div class="col-1 pt-4 mt-5 ml-1">\n' +
            '                                                <button type="button" class=" btn btn-sm btn-danger d-inline-block" onclick="$(this).parent().parent().remove()"><i class="material-icons">remove_circle</i></button>\n' +
            '                                            </div> <div class="col-2 pt-4 mt-5">\n' +
            '                            <button type="button" class="btn btn-info " onclick="addChoice($(this).parent().next(),'+index+')">إضافة خيارات الإجابات</button>\n' +
            '    </div>\n' +
            '                     <div class="answers d-none col-12"><h4>خيارات السؤال:</h4></div>   </div>');
    index++;
    }

    function addChoice(div,i){
        $(div).removeClass('d-none');
        $(div).append('<div class=" w-100 row">\n' +
            '                            <div class="col-md-4 ">\n' +
            '                                <div class="form-group">\n' +
            '                                    <label class="bmd-label-floating">نص الإختيار </label>\n' +
            '                                    <input type="text" class="form-control"  name="questions['+i+'][choice]['+index2+'][text]">\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="col-md-2 ">\n' +
            '                                <div class="form-group">\n' +
            '                                    <input type="checkbox"  value="1"  name="questions['+i+'][choice]['+index2+'][correct]">\n' +
            '                                    <label class="bmd-label-floating">الإجابة الصحيحة؟ </label>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="col-md-1 ">\n' +
            '<button class="btn btn-sm btn-danger " onclick="$(this).parent().parent().remove()"><i class="material-icons">remove_circle</i></button>'+
            '</div>'
        );
        index2++;
    }
</script>
@endsection

