@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list"> قائمة الإختبارات </h4>



                </div>
                @if(!isset($message))
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            <th>#</th>
                            <th> عنوان الإختبار </th>
                            <th>الدورة  </th>
                            <th>درجة الصعوبة  </th>
                            <th>درجة الإختبار</th>

                            </thead>
                            <tbody>
                            @foreach($exams as $exam)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$exam->exam->title}}</td>
                                    <td>{{$exam->exam->course->name}}</td>
                                    <td>{{$exam->exam->difficult}} </td>
                                    <td>{{$exam->result."/".$exam->fullMark}} </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>

@endsection
