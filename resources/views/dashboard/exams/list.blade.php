@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list"> قائمة الإختبارات </h4>


                    <a

                        href="{{route('exam.create')}}"


                        style="float: left" alt="  إضافة إختبار">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            <th>#</th>
                            <th> عنوان الإختبار </th>
                            <th>الدورة  </th>
                            <th>درجة الصعوبة  </th>
                            <th>  </th>
                            </thead>
                            <tbody>
                            @foreach($exams as $exam)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$exam->title}}</td>
                                    <td>{{$exam->course->name}}</td>
                                    <td>{{$exam->difficult}} </td>
                                    <td style="display: flex">
                                        <a class="btn btn-primary float-right col-3 btn-edit text-success"

                                           href="{{route('exam.edit',$exam->id)}}"
                                        >

                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form method ="post"

                                              action="{{route('exam.destroy',$exam->id)}}"

                                              class="col-3">
                                            <div class ="input-group">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-delete float-right text-danger" onclick="confirmation($(this).parent().parent())">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $exams->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(form){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
               form.submit();
            }
        }
    </script>
@endsection
