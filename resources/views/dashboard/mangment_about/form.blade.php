@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        @if (session('msg'))
                            <div class="alert alert-success">
                                {{ session('msg') }}
                            </div>
                        @endif
        <form 
            action="{{url('/mangment_about/'.$info->id)}}" 
              enctype="multipart/form-data"
             method="post" 
            class="add-form" style="width:100%">
             @method('PUT')
            <div class="col-lg-12">
                <div class="card">
                   
                    <div class="card-body">
                        {{csrf_field()}}
                    
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">موجز عن المركز </label>
                                    <textarea rows="5" type="text" class="form-control" name="brief"
                                          required> {{$info->brief }}"</textarea>
                                </div>
                            </div>
                            
                           <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">رؤية المركز </label>
                                    <textarea rows="5" type="text" class="form-control" name="vision"
                                          required> {{$info->vision }}"</textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">هدف المركز </label>
                                    <textarea rows="5" type="text" class="form-control" name="goal"
                                          required> {{$info->goal }}"</textarea>
                                </div>
                            </div>
                         
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        
                        <a type="btn" class="btn btn-default pull-right" href="{{url('mangment_about')}}" >
                            إلغاء
                        </a>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            
        </form>
    </div>
   

@endsection
