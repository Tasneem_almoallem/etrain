@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list">   من  نحن ؟؟ </h4>
                    
                   
                </div>
                
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> موجز عن المركز </th>
                        <th>  رؤية المركز </th>
                        <th>هدف المركز </th>
                        </thead>
                        <tbody>
                        @foreach($infos as $info)
                            <tr id='printTable'>
                                <td>{{$info->brief}}</td>
                                <td>{{$info->vision}} </td>
                                <td>{{$info->goal}} </td>

                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('mangment_about.edit',$info->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    
                                                                    </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    

@endsection
