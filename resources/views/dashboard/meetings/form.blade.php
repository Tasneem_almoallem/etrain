@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
            @if(isset($meeting)&&$meeting->id)
            action="/updateMeeting/{{$meeting->id}}"
            @else action="{{route('meeting.store')}}"
            @endif
            enctype="multipart/form-data"
            method="POST"
            class="add-form w-100">
            @if(isset($meeting)&&isset($meeting->id))
                @method('PATCH')
            @endif


            <div class="col-md-12">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($meeting->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> موضوع الجلسة </label>
                                    <input type="text" class="form-control" name="topic"
                                           @if(isset($meeting) && $meeting->topic) value="{{$meeting->topic}}"
                                           @else value="{{ old('topic') }}" @endif
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> الدورة  </label>

                                    <select  name="course_id" class="form-control" required>
                                        <option value=''></option>
                                        @foreach(Auth::user()->coachCourses as $course)
                                            <option value='{{$course->id}}' @if(isset($meeting) && $meeting->course_id==$course->id) selected @endif>
                                                {{$course->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> موعد بداية الجلسة  </label>
                                    <input type="datetime-local" class="form-control" name="start_time"
                                           @if(isset($meeting) && $meeting->start_time) value="{{$meeting->start_time->format('Y-m-d\TH:i:s')}}"
                                           @else value="{{ old('start_time') }}" @endif
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> مدة الجلسة </label>
                                    <input type="number" min="0" class="form-control" name="duration"
                                           @if(isset($meeting) && $meeting->duration) value="{{$meeting->duration}}"
                                           @else value="{{ old('duration') }}" @endif
                                           required>
                                </div>
                            </div>


                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{route('meeting.index')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>

        </form>
    </div>

@endsection

