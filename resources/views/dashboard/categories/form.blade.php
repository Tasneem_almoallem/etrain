@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
            @if(isset($category)&&$category->id)
            action="{{route('category.update',[$category->id])}}"
            @else action="{{route('category.store')}}"
            @endif
            enctype="multipart/form-data"
            method="POST"
            class="add-form w-100">
            @if(isset($category)&&isset($category->id))
                @method('PATCH')
            @endif


            <div class="col-md-12">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($category->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating mb-5"> اسم الفئة </label>
                                    <input type="text" class="form-control" name="name"
                                           @if(isset($category) && $category->name) value="{{$category->name}}"
                                           @else value="{{ old('name') }}" @endif
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> وصف الفئة  </label>
<textarea class="form-control" rows="3" name="description"> @if(isset($category) && $category->description) {{$category->description}}
    @else {{ old('description') }} @endif</textarea>
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{route('category.index')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>

        </form>
    </div>

@endsection

