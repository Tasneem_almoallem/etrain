@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list"> قائمة فئات الدورات </h4>
                    <a

                        href="{{route('category.create')}}"


                        style="float: left" alt="  إضافة إختبار">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            <th>#</th>
                            <th> اسم الفئة </th>
                            <th>وصف الفئة </th>
                            <th>  </th>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->description}}</td>
                                    <td style="display: flex">
                                        <a class="btn btn-primary float-right col-3 btn-edit text-success"

                                           href="{{route('category.edit',$category->id)}}"
                                        >

                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form method ="post"

                                              action="{{route('category.destroy',$category->id)}}"

                                              class="col-3">
                                            <div class ="input-group">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-delete float-right text-danger" onclick="confirmation($(this).parent().parent())">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $categories->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(form){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                form.submit();
            }
        }
    </script>
@endsection
