@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
           
                    <h4 class="card-title header-table-list">قائمة صور الدروس</h4>
                    <a href="{{url('/photo/create')}}"
                       style="float: left" alt=" إضافة  صورة">
                        <button class="btn btn-warning btn-round btn-just-icon">
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th>المعرف</th>
                        <th>عنوان الصورة</th>
                        <th>تاريخ الإضافة</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($photos as $photo)
                            <tr>
                                <td>{{$photo->id}}</td>
                                <td>{{$photo->title}} </td>
                                <td>{{$photo->date}}</td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       href="{{route('photo.edit',$photo->id)}}">

                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form method ="post"
                                          action="{{route('photo.destroy',$photo->id)}}"
                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button class="btn btn-delete float-right text-danger" onclick="confirmation()">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $photos->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>
@endsection
