@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form style="width: 100%"
            @if($photo->id)
            action="{{url('/photo/'.$photo->id)}}"
            @else
            action="{{url('/photo')}}"
            @endif
            method="post"
            enctype="multipart/form-data"
            class="add-form">

            @if(isset($photo->id))
                @method('PUT')
            @endif
            <div class="col-md-4">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($photo->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الاسم</label>
                                    <input type="text" class="form-control" name="title"
                                           @if($photo && $photo->title) value="{{$photo->title}}"
                                           @else value="{{ old('title') }}" @endif required>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right"
                           href="{{url('photo')}}" >
                            إلغاء
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 pt-4">
                <div class="card card-profile">
                    <div class="">
                        <a href="javascript:;">
                            @if($photo && $photo->image)
                                <img class="" id="user-img" style="    width: 100%;" src="{{asset('/images/'.$photo->image)}}"/>
                            @else
                                <img class="" style="    width: 100%;" id="user-img" src="{{asset('imgd/faces/marc.jpg')}}"/>
                            @endif
                            <div class="add-img-icon">
                                <div class="choose_file">
                                  <span class="material-icons">
                                    center_focus_weak
                                    </span>
                                    <input name="image" id="userImg" type="file" accept="image/*"
                                           onchange="putImage()"/>
                                </div>
                            </div>

                        </a>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <script>
        function showImage(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putImage() {
            var src = document.getElementById("userImg");
            var target = document.getElementById("user-img");
            showImage(src, target);
        }


    </script>

@endsection
