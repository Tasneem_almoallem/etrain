@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form 
            @if(isset($fq)) action="{{url('/mangment_fq/'.$fq->id)}}" 
             @else action="{{route('mangment_fq.store')}}"
             @endif
              enctype="multipart/form-data"
             method="post" 
            class="add-form" style="width:100%">
            @if(isset($fq))
             @method('PUT')
             @endif
            <div class="col-lg-12">
                <div class="card">
                   
                    <div class="card-body">
                        {{csrf_field()}}
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">السؤال </label>
                                    <textarea type="text" class="form-control" name="question"
                                          required rows="5"> @if(isset($fq))
                                          {{$fq->question }}
                                      @endif</textarea>
                                </div>
                            </div>
                            
                           <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">السؤال </label>
                                    <textarea rows="5" type="text" class="form-control" name="answer"
                                          required>
                                          @if(isset($fq)) {{$fq->answer }}
                                          @endif
                                      </textarea>
                                </div>
                            </div>
                            

                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        
                        <a type="btn" class="btn btn-default pull-right" href="{{url('mangment_fq')}}" >
                            إلغاء
                        </a>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>
            
        </form>
    </div>
   

@endsection
