@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list">   إدارة الأسئلة الشائعة  </h4>
                     <a
                       
                        href="{{url('/mangment_fq/create')}}"
                        
                        
                       style="float: left" alt="  إضافة  سؤال">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                   
                </div>
                
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> السؤال </th>
                        <th> جوابه </th>
                        
                        </thead>
                        <tbody>
                        @foreach($faqs as $faq)
                            <tr id='printTable'>
                                <td>{{$faq->question}}</td>
                                <td>{{$faq->answer}} </td>

                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('mangment_fq.edit',$faq->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    
                                                                    </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    

@endsection
