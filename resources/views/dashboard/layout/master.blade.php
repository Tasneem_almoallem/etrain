<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('imgd/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('imgd/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
      SMART EDUCATION TOP
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset('csss/material-dashboard.css?v=2.1.2')}}" rel="stylesheet" />
    <link href="{{asset('csss/material-dashboard-rtl.css?v=2.1.2')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('csss/style.css')}}">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('csss/demo.css')}}" rel="stylesheet" />
    <link href="{{asset('csss/custom.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/dropzone.css')}}"/>
    <script src="{{asset('jss/core/jquery.min.js')}}"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <style type="text/css">
             .ps-scrollbar-y-rail{
                display: block;
             }
         </style>

</head>

<body class="" dir="rtl">
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('imgd/sidebar-1.jpg')}}'">
        <div class="sidebar-wrapper">
            <ul class="nav">
                 <li class="nav-item active">
                    <a class="nav-link" href="{{url('/home')}}">
                        <p>SMART EDUCATION TOP</p>
                    </a>
                </li>
                  @if(Auth::user()->hasRole('Admin'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <p>لوحة التحكم</p>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasRole('Admin'))
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/users')}}">
                        <i class="material-icons">person</i>
                        <p>إدارة المستخدمين</p>
                    </a>
                </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('category.index')}}">
                            <i class="material-icons">person</i>
                            <p>إدارة فئات الدورات</p>
                        </a>
                    </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/coach')}}">
                        <i class="material-icons">group</i>
                        <p>إدارة المدربين</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/trainee')}}">
                        <i class="material-icons">person_pin</i>
                        <p>إدارة المتدربين</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/courses')}}">
                        <i class="material-icons">bubble_chart</i>
                        <p>إدارة الدورات</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/certificates')}}">
                        <i class="material-icons">location_ons</i>
                        <p>إدارة الشهادات</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/user_courses')}}">
                        <i class="material-icons">notifications</i>
                        <p>تسجيل دورة</p>
                    </a>
                </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{url('/messages')}}">
                            <i class="material-icons">language</i>
                            <p>إدارة الرسائل</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('receipts.index')}}">
                            <i class="material-icons">language</i>
                            <p>إدارة إيصالات الدفع</p>
                        </a>
                    </li>

                @elseif(Auth::user()->hasRole('trainee'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/course_trainee')}}">
                        <i class="material-icons">unarchive</i>
                        <p>سجل دوراتي </p>
                    </a>
                </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/getCertificates')}}">
                            <i class="material-icons">unarchive</i>
                            <p>شهاداتي </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/getExams')}}">
                            <i class="material-icons">unarchive</i>
                            <p>إختباراتي </p>
                        </a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/course_coach')}}">
                            <i class="material-icons">unarchive</i>
                            <p>سجل دوراتك </p>
                        </a>
                    </li>


                @endif
                @if(Auth::user()->hasRole('coach'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('exam.index')}}">
                            <i class="material-icons">unarchive</i>
                            <p>إدارة الإختبارات</p>
                        </a>
                    </li>
                @endif
                @if(Auth::user()->hasRole('coach'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('meeting.index')}}">
                            <i class="material-icons">unarchive</i>
                            <p>جلسات البث المباشر</p>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">person</i>
                                <p class="d-lg-none d-md-block text-light">
                                    Account
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                @if(Auth::user()->hasRole('Admin'))
                                   <a  class="dropdown-item" href="{{route('users.edit',Auth::user()->id)}}">إدارة الحساب</a>
                                   <a  class="dropdown-item" href="{{url('/mangment/1/edit')}}">إدارة محتوى الصفحة الرئيسية </a>
                                   <a  class="dropdown-item" href="{{url('/mangment_fq')}}">إدارة الأسئلة الشائعة</a>

                                   <a  class="dropdown-item" href="{{url('photo')}}">إدارة الصور الدروس</a>
                                   <a  class="dropdown-item" href="{{url('/mangment_footer/1/edit')}}">إدارة أسفل الصفحة </a>
                                @elseif(Auth::user()->hasRole('coach'))
                                    <a  class="dropdown-item" href="{{route('coach.edit',Auth::user()->id)}}">إدارة الحساب</a>
                                @else
                                    <a  class="dropdown-item" href="{{route('trainee.edit',Auth::user()->id)}}">إدارة الحساب</a>

                                @endif
                                    <a  class="dropdown-item" href="{{route('home')}}">الرجوع إلى الصفحة الرئيسية</a>
                                <hr>
                                <a class="dropdown-item" href="auth/logout"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">تسجيل الخروج</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
               @yield('content')
            </div>
        </div>
        <footer class="footer" dir="ltr">
            <div class="container-fluid">
                <div class="copyright float-right">
                    &copy; COPYRIGHT
                    <script>
                        document.write(new Date().getFullYear())
                    </script><i class="material-icons text-danger">favorite</i>
                </div>
            </div>
        </footer>
    </div>
</div>

<!--   Core JS Files   -->

<script src="{{asset('jss/core/popper.min.js')}}"></script>
<script src="{{asset('jss/core/bootstrap-material-design.min.js')}}"></script>
<!-- <script src="{{asset('jss/plugins/perfect-scrollbar.jquery.min.js')}}"></script> -->
<!-- Plugin for the momentJs  -->
<script src="{{asset('jss/plugins/moment.min.js')}}"></script>
<!--  Plugin for Sweet Alert -->
<script src="{{asset('jss/plugins/sweetalert2.js')}}"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('jss/plugins/jquery.validate.min.js')}}"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{asset('jss/plugins/jquery.bootstrap-wizard.js')}}"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('jss/plugins/bootstrap-selectpicker.js')}}"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{asset('jss/plugins/bootstrap-datetimepicker.min.js')}}"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="{{asset('jss/plugins/jquery.dataTables.min.js')}}"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{asset('jss/plugins/bootstrap-tagsinput.js')}}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('jss/plugins/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{asset('jss/plugins/fullcalendar.min.js')}}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{asset('jss/plugins/jquery-jvectormap.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('jss/plugins/nouislider.min.js')}}"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{asset('jss/plugins/arrive.min.js')}}"></script>
<!-- Chartist JS -->
<script src="{{asset('jss/plugins/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('jss/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('jss/material-dashboard.js?v=2.1.2" type="text/javascript')}}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('jss/demo.js')}}"></script>
<script src="{{asset('js/dropzone.js')}}"></script>
<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function(event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function() {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function() {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function() {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function() {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function() {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in asset/js/demos.js
        md.initDashboardPageCharts();

    });
</script>
@yield('scripts')
</body>

</html>
