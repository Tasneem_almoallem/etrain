@extends('dashboard.layout.master')
@section('content')
    @if(Auth::user()->hasRole('Admin'))
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">school</i>
                    </div>
                    <p class="card-category">عدد المتدربين في هذا الشهر</p>
                    <h3 class="card-title">{{$trainees}}
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                          عدد المتدربين الكلي
                        <a href="javascript:;">{{$trainees_all}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">people</i>
                    </div>
                    <p class="card-category">عدد المدربين المنتسبين في هذا الشهر</p>
                    <h3 class="card-title">{{$coaches}}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                     عدد المدربين الكلي  {{$coaches_all}}
                     </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">menu_book</i>
                    </div>
                    <p class="card-category">عدد  الدورات الجديدة</p>
                    <h3 class="card-title">{{$courses}}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                         عدد  الدورات الكلية  {{$courses_all}}
                     </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">category</i>
                    </div>
                    <p class="card-category">عددفئات الدورات</p>
                    <h3 class="card-title">{{$categories}}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                       الفئات
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title"> المتدربون المنتسبون حديثاً : </span>
                          
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile">
                            <table class="table table-hover">
                              <thead class="text-primary">
                                <th>ID</th>
                                <th> اسم المتدرب </th>
                            </thead>

                             <tbody>
                               <tr>
                            @foreach($last_trainees as $last_trainee)
                            <td>{{$last_trainee->id}}</td>
                            <td>{{$last_trainee->first_name_ar." ".$last_trainee->last_name_ar}}</td>
                            
                        </tr>
                        @endforeach
                        </tbody>
                            </table>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header card-header-warning">
                    <h4 class="card-title">أحدث الدورات </h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <th>ID</th>
                        <th>اسم الدورة </th>
                        <th>مدة الدورة </th>
                        <th>فئة الدورة</th>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($last_courses as $last_course)
                            <td>{{$last_course->id}}</td>
                            <td>{{$last_course->name}}</td>
                            <td>{{$last_course->period}}</td>
                            <td>{{$last_course->category}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else

    @endif

@endsection
