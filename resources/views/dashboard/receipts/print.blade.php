@extends('dashboard.layout.master')
@section('content')
     <div class="card-body table-responsive">

        <table id='printTable' class="table table-hover">
               <thead class="text-primary">
               <th></th>
               <th></th>







              </thead>
                 <tbody>
                 <tr><td>رقم الإيصال</td> <td>{{$receipt->id}}</td></tr>

                  <tr><th>اسم المستخدم</th><td>{{$receipt->name_trainee_ar}}</td></tr>
                 <tr><th>رمز المتدرب</th><td>{{$receipt->code_trainee}}</td></tr>
                  <tr><th> رمز الدورة</th><td>{{$receipt->code_course}}</td></tr>
                 <tr><th> تاريخ الايصال </th><td>{{$receipt->date_receipt}}</td></tr>
                 <tr>  <th>المبلغ المدفوع</th><td>{{$receipt->money_payed}}</td></tr>
                 <tr><th>اسم الموظف المسؤول</th> <td>{{$receipt->name_admin}}</td></tr>

                 </tbody>

            </table>

           <a type="btn" class="btn btn-default pull-right" href="{{url('receipts')}}" >
                            إلغاء
                        </a>

                <a href="#null" onclick="printContent('printTable')" class="btn btn-primary">طباعة </a>

         </div>

 <script type="text/javascript">
<!--
function printContent(id){
str=document.getElementById(id).innerHTML
newwin=window.open('','printwin','left=100,top=100,width=400,height=400')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}
//-->
</script>



@endsection
