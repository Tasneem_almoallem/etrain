@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    
                    <h4 class="card-title header-table-list">  قائمة الإيصالات </h4>
                    <a
                       
                        href="{{url('/receipts/create')}}"
                        
                        
                       style="float: left" alt="  إضافة دورة">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> رقم الإيصال </th>
                        <th>اسم المتدرب بالعربي  </th>
                        <th>تاريخ  الإيصال </th>
                        <th>المبلغ المدفوع </th>
                        <th> العمليات </th>
                        </thead>
                        <tbody>
                        
                            <tr>
                                 @foreach($receipts as $receipt)
                                <td>{{$receipt->id}}</td>
                                <td>{{$receipt->name_trainee_ar}}</td>
                                <td>{{$receipt->date_receipt}} </td>
                                <td>{{$receipt->money_payed}} </td>

                                <td style="display: flex">
                                   <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       
                                       href="{{route('receipts.edit',$receipt->id)}}"
                                      > 

                                        <i class="material-icons">edit</i>
                                    </a>
                                   <form method ="post"
                                          
                                          action="{{route('receipts.destroy',$receipt->id)}}"
                                          
                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button class="btn btn-delete float-right text-danger" onclick="confirmation()">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form> 
                                    <a class="btn btn-info btn-print float-right"
                                       
                                       href="{{route('receipts.print',$receipt->id)}}"
                                      ><i class="material-icons">print</i></a>
                                  
                                </td>
                            </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
                 {{ $receipts->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
                
            </div>
        </div>
    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>
@endsection
