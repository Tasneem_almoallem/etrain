@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form

              action="{{url('/receipts/'.$receipt->id)}}"
              method="post"
               enctype="multipart/form-data"

            class="add-form">

                @method('PUT')

            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif

                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label  for="national_id" class="bmd-label-floating "> الرقم الوطني </label>
                                    <input type="text" class="form-control" name="national_id"
                                           value="{{ $receipt->national_id }}"
                                            >

                                </div>
                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="">  الدورة  </label>
                                    <select class="form-control"  name="code_course" required id="courses">
                                        <option></option>
                                        @foreach($courses as $course)
                                            <option value="{{$course->code}}" {{$course->code==$receipt->code_course?'selected':''}}>{{$course->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="type"> المتدرب </label>
                                    <select class="form-control" id="students" name="code_trainee" required>
                                        <option></option>
                                        @foreach($students as $student)
                                            <option value="{{$student->code}}" {{$student->code==$receipt->code_trainee?'selected':''}}>{{$student->first_name_ar." ".$student->last_name_ar}}</option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>


                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب الكامل بالعربي  </label>
                                    <input type="text" class="form-control" name="name_trainee_ar" id="name_trainee_ar" value="{{ $receipt->name_trainee_ar }}"
                                        >

                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب بالإنكليزي </label>
                                    <input type="text" class="form-control" name="name_trainee_en" id="name_trainee_en" value="{{$receipt->name_trainee_en}}"
                                    >
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> تاريخ الإيصال </label>
                                    <input type="date" class="form-control" name="date_receipt" id="date_receipt" required value="{{$receipt->date_receipt}}"
                                    >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group m-0 p-0">
                                    <label  for="name_admin" class="bmd-label-floating ">اختر اسم المسؤول </label>
                                    <select class="form-control btn btn-secondary dropdown-toggle m-0 p-0"  id="name_admin"  name="name_admin" >
                                      <option value=""></option>
                                      @foreach($users as $user)
                                     <option @if($user->first_name_ar." ".$user->last_name_ar == $receipt->name_admin) selected  @endif value="{{$user->first_name_ar." ".$user->last_name_ar}}">{{$user->first_name_ar." ".$user->last_name_ar}}</option>
                                       @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> المبلغ المدفوع </label>
                                    <input type="text" id="money" class="form-control" name="money_payed"
                                              value="{{$receipt->money_payed}}">
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{url('receipts')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>


        </form>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#courses').change(function(){
                if($(this).val()!="")
                {
                    $.ajax({
                        type:"get",
                        url:"/getCoursePrice/"+$(this).val(),
                        dataType:'json',
                        success:function (data) {
                            $('#money').attr('value',data.price);
                            var op="<option></option>";
                            $(data.students).each(function(i,e){
                                op+='<option value="'+e.code+'">'+e.first_name_ar+" "+e.last_name_ar+'</option>';
                            });
                            $('#students').html(op);
                        }
                    });
                }
                else{
                    $('#money').val('');
                }
            });
        });
    </script>
@endsection
