@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form

              action="{{route('receipts.store')}}"

               enctype="multipart/form-data"
             method="POST"
            class="add-form">



            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                  <!--  <div class="card-header card-header-primary">
                        @if(!isset($course->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div> -->
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label  for="national_id" class="bmd-label-floating "> الرقم الوطني </label>
                                    <input type="text" class="form-control" name="national_id" value="{{ $usercourse->national_id }}">

                                </div>
                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for=""> رمز الدورة  </label>

                                 <input type="text" class="form-control" name="code_course"
                                           value="{{ $usercourse->code_course }}">

                                </div>

                            </div>
                             <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating" for="type">رمز المتدرب </label>
                                     <input type="text" class="form-control" name="code_trainee"
                                           value="{{ $usercourse->code_trainee }}">

                                </div>
                            </div>


                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب الكامل بالعربي  </label>
                                    <input type="text" class="form-control" name="name_trainee_ar" id="name_trainee_ar" value="{{ $usercourse->name_trainee_ar }}"
                                        >

                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب بالإنكليزي </label>
                                    <input type="text" class="form-control" name="name_trainee_en" id="name_trainee_en" value="{{$usercourse->name_trainee_en}}"
                                    >
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> تاريخ الإيصال </label>
                                    <input type="date" class="form-control" name="date_receipt" id="date_receipt" required
                                    >
                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label  for="name_admin" class="bmd-label-floating ">اختر اسم المسؤول </label>
                                    <select class="form-control btn btn-secondary dropdown-toggle"  id="name_admin"  name="name_admin" >
                                      <option value=""></option>
                                      @foreach($users as $user)
                                     <option value="{{$user->first_name_ar." ".$user->last_name_ar}}">{{$user->first_name_ar." ".$user->last_name_ar}}</option>
                                       @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> المبلغ المدفوع </label>
                                    <input type="number" class="form-control" name="money_payed"
                                             min="1" required>
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{url('receipts')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection
