@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
         @if(isset($certificate)&&$certificate->id)
            action="{{url('/certificates/'.$certificate->id)}}"
             @else action="{{route('certificates.store')}}"
              @endif
               enctype="multipart/form-data"
             method="POST"
            class="add-form">
              @if(isset($certificate->id))
                @method('PUT')
            @endif


            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($certificate->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-8 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم المتدرب بالإنكليزي </label>
                                    <select class="form-control" id="users" name="user_id">
                                        <option></option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" @if($certificate && $certificate->user_id==$user->id) selected @elseif(old('user_id')==$user->id) selected @endif>{{$user->first_name_ar." ".$user->last_name_ar}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-8 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating"> اسم الدورة  </label>
                                    <select class="form-control" id="courses" name="course_id" @if(!isset($courses)) disabled @endif>
                                    <option></option>
                                        @if(isset($courses))
                                            @foreach($courses as $course)
                                                <option value="{{$course->id}}" @if($certificate && $certificate->course_id==$course->id) selected @elseif(old('course_id')==$course->id) selected @endif>{{$course->name}}</option>

                                            @endforeach
                                            @endif
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-8 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">   تاريخ منح الشهادة </label>
                                    <input type="date" class="form-control" name="date_of_give"
                                          @if($certificate && $certificate->date_of_give) value="{{$certificate->date_of_give}}"
                                           @else  value="{{ old('date_of_give') }}" @endif
                                            required>
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>

                        <a type="btn" class="btn btn-default pull-right" href="{{url('certificates')}}" >
                            إلغاء
                        </a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <hr>

        </form>
    </div>
    <script>
        function showImage(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putImage() {
            var src = document.getElementById("courseImg");
            var target = document.getElementById("course-img");
            showImage(src, target);
        }

        function showCV(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.value(fr.result);
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putCV() {
            var src = document.getElementById("userCv");
            var target = document.getElementById("user-cv");
            showCV(src, target);
        }
    </script>
    <script>
        $(document).ready(function(){
            $('#users').change(function(){
                if(this.value!='')
                {
                    $.ajax({
                        type:"post",
                        url:"/getUserCourses",
                        data:{user_id:this.value,_token:"{{csrf_token()}}"},
                        success:function(data){
                            $('#courses').html(data);
                            $('#courses').removeAttr('disabled');
                        }
                    })
                }
            })
        })
    </script>

@endsection
