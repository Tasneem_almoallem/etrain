@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">   قائمة الشهادات </h4>


                </div>
                @if(!isset($message))
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            <th> رقم  الشهادة </th>
                            <th> اسم المتدرب </th>
                            <th> اسم الدورة </th>
                            <th>تاريخ المنح </th>
                            <th >العمليات  </th>
                            </thead>
                            <tbody>
                            @foreach($certificates as $certificate)
                                <tr id='printTable'>
                                    <td>{{$certificate->id}}</td>
                                    <td>{{$certificate->user->first_name_ar." ".$certificate->user->last_name_ar}} </td>

                                    <td>{{$certificate->course->name}}</td>
                                    <td>{{$certificate->date_of_give}}</td>
                                    <td style="display: flex">

                                        <a class="btn btn-info btn-print float-right"

                                           href="{{route('certificates.print',$certificate->id)}}"
                                        ><i class="material-icons">print</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>


@endsection
