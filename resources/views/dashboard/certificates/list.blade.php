@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">   قائمة الشهادات </h4>


                    <a

                        href="{{url('/certificates/create')}}"


                       style="float: left" alt="  إضافة دورة">
                        <button class="btn btn-warning btn-round  glyphicon glyphicon-plus">
                            <i class="material-icons  glyphicon glyphicon-plus">add </i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> رقم  الشهادة </th>
                        <th> اسم المتدرب </th>
                        <th> اسم الدورة </th>
                        <th>تاريخ المنح </th>
                        <th >العمليات  </th>
                        </thead>
                        <tbody>
                        @foreach($certificates as $certificate)
                            <tr id='printTable'>
                                <td>{{$certificate->id}}</td>
                                <td>{{$certificate->user->first_name_ar." ".$certificate->user->last_name_ar}} </td>

                                <td>{{$certificate->course->name}}</td>
                                <td>{{$certificate->date_of_give}}</td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"

                                       href="{{route('certificates.edit',$certificate->id)}}"
                                      >

                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form method ="post"

                                          action="{{route('certificates.destroy',$certificate->id)}}"

                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button type="button" class="btn btn-delete float-right text-danger" onclick="confirmation($(this).parent().parent())" >
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form>
                                    <a class="btn btn-info btn-print float-right"

                                       href="{{route('certificates.print',$certificate->id)}}"
                                      ><i class="material-icons">print</i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $certificates->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(form){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
$(form).submit();
            }
        }
    </script>

@endsection
