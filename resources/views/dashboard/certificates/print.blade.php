@extends('dashboard.layout.master')
@section('content')
     <div class="card-body table-responsive">
         <div id='printTable' class="row bg-light d-none" dir="rtl" style="direction:rtl; text-align: right">
       <p> تم منح هذه الشهادة من قبل موقع ETrain</p>
        <p><h4>{{$certificate->id}} :رقم الشهادة</h4></p>
         <p><h4>{{$certificate->user->first_name_ar." ".$certificate->user->last_name_ar}} :اسم المتدرب</h4></p>
         <p><h4>{{$certificate->course->name}}: اسم الدورة </h4></p>
         <p><h4>{{$certificate->date_of_give}} :تاريخ المنح </h4></p>
             <img src="{{URL::to("img/logo.png")}}" class="d-block" />
         </div>
         <table  class="table table-hover">
               <thead class="text-primary">
              <th>رقم الشهادة </th>
              <th>اسم المتدرب </th>
              <th> اسم الدورة </th>
              <th> تاريخ المنح </th>

              </thead>


                 <tbody>
                 <td>{{$certificate->id}}</td>
                 <td>{{$certificate->user->first_name_ar." ".$certificate->user->last_name_ar}} </td>

                 <td>{{$certificate->course->name}}</td>
                 <td>{{$certificate->date_of_give}}</td>
                 </tbody>

            </table>

           <a type="btn" class="btn btn-default pull-right" href="{{url('certificates')}}" >
                            إلغاء
                        </a>

                <a href="#null" onclick="printContent('printTable')" class="btn btn-primary">طباعة </a>

         </div>

 <script type="text/javascript">
<!--
function printContent(id){
str=document.getElementById(id).innerHTML
newwin=window.open('','printwin','left=100,top=100,width=400,height=400')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}
//-->
</script>



@endsection
