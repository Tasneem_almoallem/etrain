@extends('dashboard.layout.master')
@section('content')
 <div class="row">



         <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">  رسائل الزوار </h4>
                </div>


                 <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th>العنوان </th>
                        <th>تاريخ  الرسالة</th>
                         <th>اسم المرسل</th>
                         <th>البريد الالكتروني</th>
                        <th>الرسالة</th>
                        </thead>
                        <tbody>
                        @if(isset($guests))
                            @foreach($guests as $msg)
                            <tr>

                                <td>{{$msg->title}}</td>
                                <td>{{$msg->created_at}} </td>
                                <td>{{$msg->name}} </td>
                                <td><a href="mailTo:{{$msg->email}}">{{$msg->email}} </a> </td>

                                <td>
                                 {{$msg->body}}
                                </td>
                            </tr>
                       @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>


    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>

@endsection
