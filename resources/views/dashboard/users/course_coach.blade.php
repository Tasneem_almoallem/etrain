@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                </div>
            
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> اسم الدورة </th>
                        <th> تاريخ البدء </th>
                        <th> تاريخ الانتهاء </th>
                        <th></th>
                         <th></th>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$course->name}}</td>
                                <td>{{$course->date_of_start}} </td>
                                <td>{{$course->date_of_end}}</td>
                               <td><a href="{{url('course_coach/'.$course->id)}}">المتدربين</a></td>
                               <td><a href="{{url('files/'.$course->id.'/edit')}}">اضافة ملفات</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
    
@endsection
