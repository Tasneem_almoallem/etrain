@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <form
            @if($user->id)
            @if($role == 'Admin')
            action="{{url('/users/'.$user->id)}}"
            @elseif ($role == 'trainee')
            action="{{url('/trainee/'.$user->id)}}"
            @else
            action="{{url('/coach/'.$user->id)}}"
            @endif
            @else
            @if($role == 'Admin')
            action="{{url('/users')}}"
            @elseif ($role == 'trainee')
            action="{{url('/trainee')}}"
            @else
            action="{{url('/coach')}}"
            @endif
            @endif
            method="post"
            enctype="multipart/form-data"
            class="add-form">

            @if(isset($user->id))
                @method('PUT')
            @endif
            <div class="col-md-8">
                <div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            يوجد هناك بعض المشاكل

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif
                    <div class="card-header card-header-primary">
                        @if(!isset($user->id))
                            <h4 class="card-title">إضافة جديد</h4>
                        @else
                            <h4 class="card-title">تعديل</h4>
                        @endif
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الاسم</label>
                                    <input type="text" class="form-control" name="first_name_ar"
                                           @if($user && $user->first_name_ar) value="{{$user->first_name_ar}}"
                                           @else value="{{ old('first_name_ar') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الاسم الأوسط</label>
                                    <input type="text" class="form-control" name="middle_name_ar"
                                           @if($user && $user->middle_name_ar) value="{{$user->middle_name_ar}}"
                                           @else value="{{ old('middle_name_ar') }}" @endif>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الكنية</label>
                                    <input type="text" class="form-control" name="last_name_ar"
                                           @if($user && $user->last_name_ar) value="{{$user->last_name_ar}}"
                                           @else value="{{ old('last_name_ar') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الاسم بالانكليزية</label>
                                    <input type="text" class="form-control" name="first_name_en"
                                           @if($user && $user->first_name_en) value="{{$user->first_name_en}}"
                                           @else value="{{ old('first_name_en') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الاسم الأوسط بالانكليزية</label>
                                    <input type="text" class="form-control" name="middle_name_en"
                                           @if($user && $user->middle_name_en) value="{{$user->middle_name_en}}"
                                           @else value="{{ old('middle_name_en') }}" @endif>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الكنية بالانكليزية</label>
                                    <input type="text" class="form-control" name="last_name_en"
                                           @if($user && $user->last_name_en) value="{{$user->last_name_en}}"
                                           @else value="{{ old('last_name_en') }}" @endif required>
                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">البريد الالكتروني</label>
                                    <input type="email" class="form-control" name="email"
                                           @if($user && $user->email) value="{{$user->email}}"
                                           @else value="{{ old('email') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الرقم الوطني</label>
                                    <input type="text" class="form-control" name="national_id"
                                           @if($user && $user->national_id) value="{{$user->national_id}}"
                                           @else value="{{ old('national_id') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الرمز</label>
                                    <input type="text" class="form-control" name="code"
                                           value="{{$user->code}}"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">مكان الولادة</label>
                                    <input type="text" class="form-control" name="birth_country" id="address"
                                           @if($user && $user->birth_country) value="{{$user->birth_country}}"
                                           @else value="{{ old('birth_country') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">تاريخ الولادة</label>
                                    <input type="date" class="form-control" name="date_of_birth"
                                           @if($user && $user->date_of_birth) value="{{$user->date_of_birth}}"
                                           @else value="{{ old('date_of_birth') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">المحافظة </label>
                                    <input type="text" class="form-control" name="state" id="address2"
                                           @if($user && $user->state) value="{{$user->state}}"
                                           @else value="{{ old('state') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-12 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">العنوان</label>
                                    <textarea rows="3" class="form-control" name="address" required>
                                        @if($user && $user->address) {{$user->address}}
                                        @else {{ old('address') }} @endif
                                    </textarea>
                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الهاتف</label>
                                    <input type="text" class="form-control" name="phone"
                                           @if($user && $user->phone) value="{{$user->phone}}"
                                           @else value="{{ old('phone') }}" @endif required>
                                </div>
                            </div>

                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">الجنس</label>
                                    <input type="text" class="form-control" name="gender"
                                           @if($user && $user->gender) value="{{$user->gender}}"
                                           @else value="{{ old('gender') }}" @endif required>
                                </div>
                            </div>
                            <div class="col-md-4 pt-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">كلمة السر</label>
                                    <input type="password" class="form-control" name="password"
                                           @if($user && $user->password) value="{{$user->password}}"
                                           @else value="{{ old('password') }}" @endif required>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">حفظ</button>
                        @if(Auth::user()->role == 'Admin')
                        <a type="btn" class="btn btn-default pull-right"
                           @if($role=='Admin')
                           href="{{url('users')}}"
                           @elseif($role=='trainee')
                           href="{{url('trainee')}}"
                           @else
                           href="{{url('coach')}}"
                            @endif >
                            إلغاء
                        </a>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 pt-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="javascript:;">
                            @if($user && $user->image)
                                <img class="img user-img" id="user-img" src="{{asset('/images/'.$user->image)}}"/>
                            @else
                                <img class="img user-img" id="user-img" src="{{asset('imgd/faces/marc.jpg')}}"/>
                            @endif
                            <div class="add-img-icon">
                                <div class="choose_file">
                                  <span class="material-icons">
                                    center_focus_weak
                                    </span>
                                    <input name="image" id="userImg" type="file" accept="image/*"
                                           onchange="putImage()"/>
                                </div>
                            </div>

                        </a>
                    </div>
                    <div class="card-body">
                        @if($role=='Admin')
                            <h3>نوع المستخدم: أدمن</h3>
                        @elseif($role=='trainee')
                            <h3>نوع المستخدم: متدرب</h3>
                        @else
                            <h3>نوع المستخدم: مدرب</h3>
                        @endif
                        <hr>

                        <div class="row cv-parent">
                            <h4 class="text-right">السيرة الذاتية</h4>
                            <div class="col-12">
                                @if(isset($user->cv))
                                    <span class="text-warning" id="cv-name">{{$user->cv}}</span>
                                    <a
                                        href="{{asset('/cvs/'.$user->cv)}}" target="_blank"
                                        alt="انقر هنا لرؤية السيرة الذاتية">
                                        <span class="material-icons text-success">visibility</span>
                                    </a>
                                @endif
                            </div>

                            <div class="cv-loader mt-3">
                                @if(isset($user->cv))
                                    <span style="width:50%">
                                        تعديل السيرة الذاتية
                                    </span>
                                @else
                                    <span style="width:50%">
                                        أضف السيرة الذاتية
                                    </span>
                                @endif
                                <input name="cv" id="userCv" type="file"
                                       accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                                       onchange="putCV()"
                                       @if(isset($user->cv))
                                       value="{{$user->cv}}"
                                       style="width: 32%;"
                                       @else
                                       style="width: 60%;"
                                       @endif
                                       class="form-control-file"
                                />
                            </div>

                        </div>
                    </div>

                    <div class="col-md-12 pt-4">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="bmd-label-floating"> الملاحظات</label>
                                <textarea class="form-control" rows="5">
                                    @if($user && $user->notes)
                                        {{$user->notes}}
                                    @else
                                        {{ old('notes') }}
                                    @endif
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function showImage(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.src = fr.result;
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putImage() {
            var src = document.getElementById("userImg");
            var target = document.getElementById("user-img");
            showImage(src, target);
        }

        function showCV(src, target) {
            var fr = new FileReader();

            fr.onload = function () {
                target.value(fr.result);
            }
            fr.readAsDataURL(src.files[0]);

        }

        function putCV() {
            var src = document.getElementById("userCv");
            var target = document.getElementById("user-cv");
            showCV(src, target);
        }
    </script>

@endsection
