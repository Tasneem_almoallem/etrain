@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    @if($role == 'Admin')
                    <h4 class="card-title header-table-list">قائمة المستخدمين</h4>
                    @elseif ($role == 'trainee')
                    <h4 class="card-title header-table-list">قائمة المتدربين</h4>
                    @else
                    <h4 class="card-title header-table-list">قائمة المدربين</h4>
                    @endif
                    <form
                        @if($role == 'Admin')
                        action="{{route('users.search')}}"
                        @elseif ($role == 'trainee')
                        action="{{route('trainee.search')}}"
                        @else
                        action="{{route('coach.search')}}"
                        @endif
                        method="POST"
                        class="navbar-form seach-list">
                        {{ csrf_field() }}
                        <div class="input-group no-border">
                            <input type="text" class="form-control" placeholder="ابحث..." name="q"
                                   @if(isset($query)) value="{{$q}}" @endif>
                            <button type="submit" class="btn btn-search btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                    <a
                        @if($role == 'Admin')
                        href="{{url('/users/create')}}"
                        @elseif ($role == 'trainee')
                        href="{{url('/trainee/create')}}"
                        @else
                        href="{{url('/coach/create')}}"
                        @endif
                       style="float: left" alt=" إضافة مستخدم">
                        <button class="btn btn-warning btn-round btn-just-icon">
                            <i class="material-icons">person_add</i>
                        </button>
                    </a>
                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th>المعرف</th>
                        <th>الاسم</th>
                        <th>الإيميل</th>
                        <th>العمليات</th>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->first_name_ar." ".$user->last_name_ar}} </td>
                                <td>{{$user->email}}</td>
                                <td style="display: flex">
                                    <a class="btn btn-primary float-right col-3 btn-edit text-success"
                                       @if($role == 'Admin')
                                       href="{{route('users.edit',$user->id)}}"
                                       @elseif ($role == 'trainee')
                                       href="{{route('trainee.edit',$user->id)}}"
                                       @else
                                       href="{{route('coach.edit',$user->id)}}"   @endif>

                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form method ="post"
                                          @if($role == 'Admin')
                                          action="{{route('users.destroy',$user->id)}}"
                                          @elseif ($role == 'trainee')
                                          action="{{route('trainee.destroy',$user->id)}}"
                                          @else
                                          action="{{route('coach.destroy',$user->id)}}"
                                          @endif
                                          class="col-3">
                                        <div class ="input-group">
                                            @method('DELETE')
                                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                            <button class="btn btn-delete float-right text-danger" onclick="confirmation()">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $users->links() }}
                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif
            </div>
        </div>
    </div>
    <script>
        function confirmation(){
            var result = confirm("هل أنت متأكد من عملية الحذف؟");
            if(result){
                // Delete logic goes here
            }
        }
    </script>
@endsection
