@extends('dashboard.layout.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> اسم الدورة </th>
                        <th> </th>
                        <th></th>

                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$course->course_name}}</td>
                                <td> </td>
                                <td></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            @if(isset($recomendation))
             <div class="card">
                <div class="card-header card-header-info">
                    دورات مقترحة
                </div>
                 <div class="row">
                 @foreach($recomendation as $recomendation)
                     <div class="col-md-4">
                <a href="{{url('/course-detailes/'.$recomendation->id)}}">
                    <div class="card-body table-responsive">
                        <img src="{{asset('images/'.$recomendation->image)}}">
                     <h3>{{$recomendation->name}}</h3>
                </div>
                </a></div>
                 @endforeach
                 </div>
            </div>
             @endif
        </div>

    </div>

@endsection
