@extends('dashboard.layout.master')
@section('content')
 <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">

                    <h4 class="card-title header-table-list">  قائمة المتدربين  </h4>

                </div>
                @if(!isset($message))
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-primary">
                        <th> الاسم</th>
                         <th> البريد الالكتروني</th>

                        </thead>
                        <tbody>

                                 @foreach($users as $user)
                            <tr>
                                <td>{{$user->first_name_ar}} @if($user->last_name_ar){{$user->last_name_ar}} @endif</td>
                                <td>{{$user->email}}</td>

                            </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>


                @else
                    <div class="col-12 float-left text-danger text-center">
                        <strong>{{$message}}</strong>
                    </div>

                @endif

            </div>
        </div>


    </div>


@endsection
