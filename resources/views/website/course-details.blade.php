@extends('layouts.layout')
@section('css')
    <style>
        div.stars {
            width: 270px;
            display: inline-block;
        }

        input.star { display: none; }

        label.star {
            float: right;
            padding: 10px;
            font-size: 36px;
            color: #444;
            transition: all .2s;
        }

        input.star:checked ~ label.star:before {
            content: '\f005';
            color: #FD4;
            transition: all .25s;
        }

        input.star-5:checked ~ label.star:before {
            color: #FE7;
            text-shadow: 0 0 20px #952;
        }

        input.star-1:checked ~ label.star:before { color: #F62; }

        label.star:hover { transform: rotate(-15deg) scale(1.3); }

        label.star:before {
            content: '\f006';
            font-family: FontAwesome;
        }
        .comments-container {
            margin: 60px auto 15px;
            width: 768px;
        }

        .comments-container h4 {
            font-size: 36px;

            font-weight: 400;
        }

        .comments-container h4 a {
            font-size: 18px;
            font-weight: 700;
        }

        .comments-list {
            margin-top: 30px;
            position: relative;
        }

        /**
         * Lineas / Detalles
         -----------------------*/
        .comments-list:before {
            content: '';
            width: 2px;
            height: 100%;
            background: #c7cacb;
            position: absolute;
            left: 32px;
            top: 0;
        }

        .comments-list:after {
            content: '';
            position: absolute;
            background: #c7cacb;
            bottom: 0;
            left: 27px;
            width: 7px;
            height: 7px;
            border: 3px solid #dee1e3;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }

        .reply-list:before, .reply-list:after {display: none;}
        .reply-list li:before {
            content: '';
            width: 60px;
            height: 2px;
            background: #c7cacb;
            position: absolute;
            top: 25px;
            left: -55px;
        }


        .comments-list li {
            margin-bottom: 15px;
            display: block;
            position: relative;
        }

        .comments-list li:after {
            content: '';
            display: block;
            clear: both;
            height: 0;
            width: 0;
        }

        .reply-list {
            padding-left: 88px;
            clear: both;
            margin-top: 15px;
        }
        /**
         * Avatar
         ---------------------------*/
        .comments-list .comment-avatar {
            width: 65px;
            height: 65px;
            position: relative;
            z-index: 99;
            float: left;
            border: 3px solid #FFF;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            box-shadow: 0 1px 2px rgba(0,0,0,0.2);
            overflow: hidden;
        }

        .comments-list .comment-avatar img {
            width: 100%;
            height: 100%;
        }

        .reply-list .comment-avatar {
            width: 50px;
            height: 50px;
        }

        .comment-main-level:after {
            content: '';
            width: 0;
            height: 0;
            display: block;
            clear: both;
        }
        /**
         * Caja del Comentario
         ---------------------------*/
        .comments-list .comment-box {
            width: 680px;
            float: right;
            position: relative;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
            -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.15);
            box-shadow: 0 1px 1px rgba(0,0,0,0.15);
        }

        .comments-list .comment-box:before, .comments-list .comment-box:after {
            content: '';
            height: 0;
            width: 0;
            position: absolute;
            display: block;
            border-width: 10px 12px 10px 0;
            border-style: solid;
            border-color: transparent #FCFCFC;
            top: 8px;
            left: -11px;
        }

        .comments-list .comment-box:before {
            border-width: 11px 13px 11px 0;
            border-color: transparent rgba(0,0,0,0.05);
            left: -12px;
        }

        .reply-list .comment-box {
            width: 610px;
        }
        .comment-box .comment-head {
            background: #FCFCFC;
            padding: 10px 12px;
            border-bottom: 1px solid #E5E5E5;
            overflow: hidden;
            -webkit-border-radius: 4px 4px 0 0;
            -moz-border-radius: 4px 4px 0 0;
            border-radius: 4px 4px 0 0;
        }

        .comment-box .comment-head i {
            float: right;
            margin-left: 14px;
            position: relative;
            top: 2px;
            color: #A6A6A6;
            cursor: pointer;
            -webkit-transition: color 0.3s ease;
            -o-transition: color 0.3s ease;
            transition: color 0.3s ease;
        }

        .comment-box .comment-head i:hover {
            color: #03658c;
        }

        .comment-box .comment-name {
            color: #283035;
            font-size: 14px;
            font-weight: 700;
            float: left;
            margin-right: 10px;
        }

        .comment-box .comment-name a {
            color: #283035;
        }

        .comment-box .comment-head span {
            float: left;
            color: #999;
            font-size: 13px;
            position: relative;
            top: -3px;
        }

        .comment-box .comment-content {
            background: #FFF;
            padding: 12px;
            font-size: 15px;
            color: #595959;
            -webkit-border-radius: 0 0 4px 4px;
            -moz-border-radius: 0 0 4px 4px;
            border-radius: 0 0 4px 4px;
        }

        .comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {color: #03658c;}


        /** =====================
         * Responsive
         ========================*/
        @media only screen and (max-width: 766px) {
            .comments-container {
                width: 480px;
            }

            .comments-list .comment-box {
                width: 390px;
            }

            .reply-list .comment-box {
                width: 320px;
            }
        }
    </style>
@endsection
@section('content')
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>{{$course->name}}</h2>
                            <p>الرئيسية<span>/</span>تفاصيل الدورة</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="course_details_area section_padding" style="direction: rtl;
    text-align: right;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 course_details_left">
                    <div class="main_image">
                        <img class="img-fluid" src="{{asset('images/'.$course->image)}}" alt="">
                    </div>
                    <div class="content_wrapper">
                        <h3 class="title_top">{{$course->name}}</h3>
                        <h4>{{$course->category}}</h4>
                        <div class="content">
                            <p class="text_info">
                                {{$course->description}}
                            </p>
                            <p> تبدأ في: {{$course->date_of_start}} وتنتهي في: {{$course->date_of_end}} </p>

                            <p>تبدأ في الساعة: {{$course->time_of_start_session}} إلى الساعة: {{$course->time_of_end_session}}</p>
                            <p>
                                وذلك في أيام: {{$course->days_of_session}}
                            </p>
                        </div>

                        @if(isset($user) && $user->role=='trainee' && isset($courseUser) && $courseUser>0 && !$not_availble && $is_paid)
                        <h4 class="title">ملفات الدورة</h4>
                        <div class="content">
                            <ul class="course_list">
                                @foreach($course->getMedia('documents') as $file)
                                    <li >
                                        <div class="justify-content-between align-items-center d-flex">
                                        @if( \Illuminate\Support\Str::startsWith($file->mime_type, 'video'))
                                            <video width="150" height="100" controls><source src="{{URL::to("media/".$file->id."/".$file->file_name)}}" ></video>
                                        @elseif(\Illuminate\Support\Str::startsWith($file->mime_type, 'image'))
                                            <img width="150" height="100" src="{{URL::to("media/".$file->id."/".$file->file_name)}}">
                                        @endif

                                        <p style="margin-right: 350px;">
                                            {{explode('_', $file->file_name,2)[1] }}
                                        </p>
                                        <div class="d-flex justify-content-end ">
                                        <a class="btn_2 text-uppercase mr-2" href="{{URL::to("media/".$file->id."/".$file->file_name)}}" target="_blank"><i class="ti-eye"></i></a>
                                        <a class="btn_2 text-uppercase" href="/download/{{$file->id}}"><i class="ti-download"></i></a>
                                        </div></div>
                                            @if( \Illuminate\Support\Str::startsWith($file->mime_type, 'video'))
                                                @if(isset($rated)&&isset($rated[$file->id]))
                                                <div class="rating">
                                                    <label >تقييمك للفيديو:</label>
                                                    @for($i=0;$i<$rated[$file->id];$i++)
                                                        <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                                    @endfor
                                                    @for($i=$rated[$file->id];$i<5;$i++)
                                                        <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                                    @endfor
                                                    <span>({{$rated[$file->id]}})</span>
                                                </div>

                                            @else
                                                <form method="post" action="/rate/{{$file->id}}/{{$course->id}}" >
                                                    @csrf
                                                    <div class="stars w-100 justify-content-between align-items-center d-flex">
                                                        <label >تقييمك للفيديو:</label>
                                                        <div>
                                                            <input class="star star-5" id="star-5 "value="5" type="radio" name="star"/>
                                                            <label class="star star-5" for="star-5"></label>
                                                            <input class="star star-4" id="star-4" value="4" type="radio" name="star"/>
                                                            <label class="star star-4" for="star-4" ></label>
                                                            <input class="star star-3" id="star-3" value="3" type="radio" name="star"/>
                                                            <label class="star star-3" for="star-3"></label>
                                                            <input class="star star-2" id="star-2" value="2" type="radio" name="star"/>
                                                            <label class="star star-2" for="star-2"></label>
                                                            <input class="star star-1" id="star-1" value="1" type="radio" name="star"/>
                                                            <label class="star star-1" for="star-1"></label>
                                                        </div>
                                                        <button class="btn btn_4 ">إرسال</button>
                                                    </div>
                                                </form>
                                                    @endif



                                                @endif
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                        @if($course->meetings)
                            <h4 class="title">البث المباشر</h4>
                            <div class="content">
                                <ul class="course_list">
                                    @foreach($course->meetings as $meeting)

                                        <li class="justify-content-between align-items-center d-flex">
                                            <p >
                                                {{$meeting->topic}}
                                            </p>
                                            <p>
                                                {{$meeting->start_time}}
                                            </p>
                                            <div class="d-flex justify-content-end ">

                                                <a class="btn btn_1 text-uppercase" href="/join/{{$meeting->id}}"> انضم الآن <i class="ti-control-play"></i></a>
                                            </div>
                                        </li>

                                    @endforeach

                                </ul>
                            </div>
                            @endif



                        @endif
                        @auth
                            <div class="content">
                                <div class="comments-container">
                                    <h4 class="title">تعليقات المتدربين: </h4>

                                    <ul id="comments-list" class="comments-list" style="direction: ltr">
                                        @foreach($comments as $comment)
                                            <li>

                                                <div class="comment-main-level">
                                                    <!-- Avatar -->
                                                    <!-- Contenedor del Comentario -->
                                                    <div class="comment-box">
                                                        <div class="comment-head">
                                                            <h6 class="comment-name by-author"><a >{{$comment->user->first_name_ar." ".$comment->user->last_name_ar}}</a></h6>
                                                            <span>{{$comment->created_at->diffForHumans()}}</span>
                                                            <i class="fa fa-reply" onclick="comment({{$comment->id}})"></i>
                                                        </div>
                                                        <div class="comment-content">
                                                            {{$comment->comment}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Respuestas de los comentarios -->
                                                @if($comment->replies)
                                                    <ul class="comments-list reply-list">
                                                        @foreach($comment->replies as $reply)
                                                            <li>
                                                                <!-- Avatar -->
                                                                <!-- Contenedor del Comentario -->
                                                                <div class="comment-box">
                                                                    <div class="comment-head">
                                                                        <h6 class="comment-name by-author"><a >{{$comment->user->first_name_ar." ".$comment->user->last_name_ar}}</a></h6>
                                                                        <span>{{$reply->created_at->diffForHumans()}}</span>

                                                                    </div>
                                                                    <div class="comment-content">
                                                                        {{$reply->comment}}
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach

                                                    </ul>
                                                @endif

                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="feedeback">
                                        <h5>ضع تعليقك هنا:</h5>
                                        <form method="post" action="{{route('comment.store')}}">
                                            @csrf
                                            <input name="parent_id" id="parent" type="hidden">
                                            <input name="course_id" type="hidden" value="{{$course->id}}">
                                            <textarea name="comment" class="form-control" cols="10" rows="5"></textarea>
                                            <div class="mt-10 text-right">
                                                <a  class="btn btn_1 " onclick="$('form').submit()">تعليق</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endauth


                    </div>
                </div>

                <div class="col-lg-4 right-contents">
                    <div class="sidebar_top">
                        <ul>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    @if(isset($course->coach))
                                    <p>المدرب:</p>
                                    <span class="color">{{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</span>
                                @endif
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>تكلفة الدورة:</p>
                                    <span class="active">أفراد {{$course->cost_individual}} ل.س</span>
                                    <br>
                                    <span class="active">طلاب {{$course->cost_student}} ل.س</span>
                                    <br>
                                    <span class="active">شركات {{$course->cost_company}} ل.س</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>عدد الطلاب  الاعظمي: </p>
                                    <span>{{$course->max_num}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="justify-content-between d-flex" href="#">
                                    <p>المدة: </p>
                                    <span> {{$course->period}}</span>
                                </a>
                            </li>

                        </ul>
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @elseif(isset($courseUser) && $courseUser>0)
                            <div class="alert alert-success">
                                قمت بالتسجيل في هذا الدرس مسبقا.
                            </div>
                        @elseif(isset($not_availble) && $not_availble)
                            <div class="alert alert-warning">
                                انتهت مدة التسجيل
                            </div>
                        @endif
                        @if(isset($user) && $user->role=='trainee' && !$courseUser && !$not_availble)
                            <a href="/registertrainee/{{$course->id}}" class="btn_1 d-block">اشتراك بالدورة</a>
                        @elseif(isset($user) && $user->role=='trainee' && isset($courseUser) && $courseUser>0 && !$not_availble )
                            <a href="/closeregistertrainee/{{$course->id}}" class="btn_1 d-block">إلغاء الاشتراك</a>
                        @endif

                    </div>

                    <h4 class="title">التقييم</h4>
                    <div class="content">
                        <div class="review-top row pt-40">
                            <div class="col-lg-12">
                                <h6 class="mb-15">يمكنك تقييم الدورة الآن</h6>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>التقييم الحالي:</span>
                                    <div class="rating">
                                        @for($i=0;$i<$course->rating;$i++)
                                        <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                        @endfor
                                            @for($i=$course->rating;$i<5;$i++)
                                                <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                            @endfor
                                        <span>({{$course->rating}})</span>
                                    </div>

                                </div>


                            </div>
                        </div>


                    </div>
                    @if(isset($user) && $user->role=='trainee' && isset($courseUser) && $courseUser>0 && !$not_availble)
                    <div class="sidebar_top mb-4" style="direction: rtl; text-align: right">
                        <h4 class="widget_title">الإختبارات</h4>
                        <ul class="list cat-list">
                            @foreach($course->exams as $exam)
                                <li>
                                    <a class="justify-content-between d-flex" href="/exam/{{$exam->id}}/start">
                                        <p>{{$exam->title}} </p>
                                        <span><strong>
                                            @if($exam->difficult=='hard')
                                                متقدم
                                            @elseif($exam->difficult=='medium')
                                                متوسط
                                            @else
                                                مبتدئ
                                            @endif
                                            </strong>
                                        </span>
                                    </a>
                                    <a class="btn btn_1" href="/exam/{{$exam->id}}/start">إبدأ الإختبار</a>
                                </li>

                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="sidebar_top">
                        <h4 class="widget_title">الفئات</h4>
                        <ul class="list cat-list">
                            @foreach($categories as $cat)
                                <li>
                                    <a href="#" class="d-flex">
                                        <p>{{$cat->category}}</p>
                                        <p>({{$cat->total}})</p>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @if(isset($recommendations)&&count($recommendations)>0)
        <section class="special_cource padding_top">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-5">
                        <div class="section_tittle text-center">
                            <p> دورات مقترحة</p>
                            <h2>قد تناسب إهتماماتك</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($recommendations as $course)
                        <div class="col-sm-6 col-lg-4">
                            <div class="single_special_cource">
                                <img src="{{asset('/images/'.$course->image)}}" class="special_img" alt="">
                                <div class="special_cource_text">
                                    <a href="{{url('/course-detailes/'.$course->id)}}" class="btn_4">{{$course->category}}</a>
                                    <h4>{{$course->cost_individual}}ل.س</h4>
                                    <a href="{{url('/course-detailes/'.$course->id)}}"><h3>{{$course->name}}</h3></a>
                                    <p> {{\Illuminate\Support\Str::limit($course->description,200,'...') }} </p>
                                    <div class="author_info">
                                        <div class="author_img">
                                            <!-- <img src="img/author/author_1.png" alt="">-->
                                            <div class="author_info_text">
                                                <p>المدرب:</p>
                                                <h5><a > {{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</a></h5>
                                            </div>
                                        </div>
                                        <div class="author_rating">
                                            <div class="rating">
                                                @for($i=0;$i<$course->rating;$i++)
                                                    <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                                @endfor
                                                @for($i=$course->rating;$i<5;$i++)
                                                    <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                                @endfor

                                            </div>
                                            <p>({{$course->rating}})</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>
    @endif
@endsection
@section('scripts')
    <script>
        function comment(v,id) {
         $('#parent').val(id);
            $('.feedeback')[0].scrollIntoView();

        }
    </script>
@endsection
