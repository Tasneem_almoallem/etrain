@extends('layouts.layout')
@section('css')
    <style>

        .exam label.radio {
            cursor: pointer
        }
    </style>
@endsection
@section('content')
    <div class="container  exam" style="margin-top: 100px">
        <div class="d-flex justify-content-center row">
            <div class="col-md-10 col-lg-10">
                <div class="border">
                    <div class="question bg-white p-3 border-bottom">
                        <div class="d-flex flex-row justify-content-between align-items-center mcq">
                            <h4>{{$exam->title}}</h4><span>عدد الأسئلة:{{count($exam->questions)}}</span>
                        </div>
                    </div>
                    <form method="post" action="/exams/{{$exam->id}}">
                    @csrf
                    @foreach($exam->questions as $qu)
                    <div class="question bg-white p-3 border-bottom" id="Q.{{$loop->index}}">
                        <div class="d-flex flex-row align-items-center question-title">
                            <h3 class="text-danger">Q.{{$loop->index+1}}</h3>
                            <h5 class="mt-1 ml-2">{{$qu->text}}</h5>
                        </div>
                        @foreach($qu->answers as $answer)
                        <div class="ans ml-2">
                            <label class="radio"> <input type="radio" name="question[{{$qu->id}}]" value="{{$answer->id}}"> <span>{{$answer->text}}</span>
                            </label>
                        </div>
                        @endforeach

                    </div>
                    @endforeach
                    <button class="btn btn_4 pull-right">إرسال</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection



