@extends('layouts.app')
@section('content')
    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg overlay2">
        <h3>نبذة عنا</h3>
    </div>
    <!-- bradcam_area_end -->

    <!-- about_area_start -->
    <div class="about_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="single_about_info">
                        <h3>أكثر من {{$certificats_count}} شهادة <br>
                            صادرة عن {{$courses_count}} دورة</h3>
                        <p>{{$homeContent->course_brief}}</p>
                       <a href="{{route('course')}}" class="boxed_btn">الالتحاق بالدورات</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="about_tutorials">
                        <div class="courses">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>{{$courses_count}}</span>
                                    <p>دورة</p>
                                </div>
                            </div>
                        </div>
                        <div class="courses-blue">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>{{$trainee_count}}</span>
                                    <p> طلاب</p>
                                </div>

                            </div>
                        </div>
                        <div class="courses-sky">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>{{$coachs_count}}</span>
                                    <p> مدربين</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

              <div class="our_courses" style="padding-bottom: 0px!important">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <h3>مركزنا وخدماتنا غير محدودة</h3>
                        <p> نوفر لطلابنا وأساتذتنا خدمات وأدوات  تسهل عملية التعليم              </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-6">
                    <div class="single_course text-center">
                        <div class="icon">
                            <i class="flaticon-art-and-design"></i>
                        </div>
                        <h3>نبذة عنا</h3>
                        <p>
                            {{$about_us->brief}}
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-6">
                    <div class="single_course text-center">
                        <div class="icon blue">
                            <i class="flaticon-crown"></i>
                        </div>
                        <h3>رؤية المركز</h3>
                        <p>
                           {{$about_us->vision}}
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-6">
                    <div class="single_course text-center">
                        <div class="icon">
                            <i class="flaticon-premium"></i>
                        </div>
                        <h3>هدف المركز</h3>
                        <p>
                             {{$about_us->goal}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- our_team_member_start -->
    <div class="our_team_member">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb-5"><h3>أساتذة المركز</h3></div>
                @foreach($coaches as $coach)
                  <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_team">
                        <div class="thumb">
                            <img class="team-pic" src="{{asset('images/'.$coach->image)}}" alt="">
                            <div class="social_link">
                                <a href="mailto:{{$coach->email}}">
                                    <i class="fa fa-envelope"></i>
                                </a>
                                <a href="https://api.whatsapp.com/send?phone={{$coach->phone}}">
                                    <i class="fa fa-phone"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3>{{$coach->first_name_ar.' '.$coach->last_name_ar}}</h3>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <!-- our_team_member_end -->

            </div>
        </div>
    </div>
    <!-- about_area_end -->

@endsection
