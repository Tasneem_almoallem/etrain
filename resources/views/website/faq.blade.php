@extends('layouts.layout')
@section('content')
    <!-- bradcam_area_start -->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>الأسئلة الشائعة</h2>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- bradcam_area_end -->


    <!--================Blog Area =================-->
    <section class="blog_area section-padding m-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-7">
                    <div class="outline_courses_info">
                        <div id="accordion">
                            @foreach($faqs as $faq)
                            <div class="card">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapse{{$faq->id}}">
                                <div class="card-header" id="{{$faq->id}}">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapse{{$faq->id}}">
                                            <i class="fa fa-check"></i> {{$faq->question}}
                                        </button>
                                    </h5>
                                </div>
                            </a>
                                <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordion">
                                    <div class="card-body">
                                       {{$faq->answer}}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

@endsection
