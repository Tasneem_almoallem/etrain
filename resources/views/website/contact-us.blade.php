@extends('layouts.layout')
@section('content')
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>تواصلوا معنا</h2>
                            <p>الرئيسية<span>/<span> تواصلوا معنا </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- ================ contact section start ================= -->
    <section class="contact-section section_padding" dir="rtl">
        <div class="container">
            <div class="d-none d-sm-block mb-5 pb-4">
                <div id="map" style="height: 480px;"></div>
                <script>
                    function initMap() {
                        var uluru = {lat: -25.363, lng: 131.044};
                        var grayStyles = [
                            {
                                featureType: "all",
                                stylers: [
                                    { saturation: -90 },
                                    { lightness: 50 }
                                ]
                            },
                            {elementType: 'labels.text.fill', stylers: [{color: '#ccdee9'}]}
                        ];
                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: -31.197, lng: 150.744},
                            zoom: 9,
                            styles: grayStyles,
                            scrollwheel:  false
                        });
                    }

                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&callback=initMap"></script>

            </div>


            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title" style="text-align:right;">ابقى على تواصل معنا</h2>
                </div>
                <div class="col-lg-8">
                    <form class="form-contact contact_form"  action="{{url('message')}}" method="post"  enctype="multipart/form-data" >
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">

                                يوجد هناك بعض المشاكل

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif

                        @if (session('MessageSuccess'))
                            <div class="alert alert-success">
                                {{ session('MessageSuccess') }}
                            </div>
                        @endif
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control w-100" name="body" id="message" cols="30" rows="9"  placeholder="الاستفسار" required="true"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control valid" name="name" id="name" type="text"
                                           @if(isset($user)) value="{{$user->first_name_ar.' '.$user->last_name_ar}}"
                                           @else placeholder="الاسم"@endif required="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control valid" name="email" id="email"
                                           @if(isset($user)) value="{{$user->email}}"
                                           @else placeholder="البريد الالكتروني" @endif required="true">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="title" id="subject" type="text"  placeholder="العنوان" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">أرسل</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4" style="text-align: right">
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body mr-2">
                            <h3>العنوان</h3>
                            <p>{{$footer->address}}</p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body mr-2">
                            <h3>الهاتف</h3>
                            <p dir="ltr">{{$footer->phone}}</p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body mr-2">
                            <h3>البريد الالكتروني</h3>
                            <p>{{$footer->email}}</p>
                        </div>
                    </div>
                    <div class="socail_links pr-4">

                        <a class="px-2" href="{{asset($footer->facebook)}}" target="_blank" style="color: #4f39d4;">
                            <i class="ti-facebook"></i>
                        </a>

                        <a class="pr-1 text-primary"  href="{{$footer->twitter}}" target="_blank">
                            <i class="ti-twitter-alt"></i>
                        </a>

                        <a class="pl-1 text-primary"  href="{{$footer->linkedin}}" target="_blank">
                            <i class="fa fa-linkedin" style="font-size: 1rem!important;"></i>
                        </a>

                        <a class="px-1 text-warning"  href="{{$footer->instagram}}" target="_blank">
                            <i class="fa fa-instagram" style="font-size: 1rem!important;"></i>
                        </a>

                        <a class="px-1 text-danger"  href="{{$footer->youtube}}" target="_blank">
                            <i class="fa fa-youtube-play" style="font-size: 1rem!important;"></i>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ contact section end ================= -->
@endsection

