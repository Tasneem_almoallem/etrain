@extends('layouts.layout')
@section('css')
    <style>
        .special_cource .single_special_cource .special_cource_text .author_info .author_img{
            padding-left:0;
        }
        .section_tittle h2:after {

            bottom: -37px;
        }
    </style>
    @endsection
@section('content')
<!-- bradcam_area_start -->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>الدورات</h2>
                        <p>الرئيسية<span>/</span>الدورات</p>
                        <div class="col-3">
                        <form action="{{route('searchCat')}}"
                              enctype="multipart/form-data" method="post" class="form-search">
                            @csrf
                            <div>
                                <label class="text-light">اسم الدورة</label>
                                <input name="course" class="form-control" id="exampleFormControlSelect2">

                            </div>
                            <div>
                                <label class="text-light ">فئة الدورة</label>
                                <select name="degree" class="form-control" id="exampleFormControlSelect3">
                                    <option value=""></option>
                                    @foreach($categories as $cat)
                                        <option value="{{$cat->category}}">{{$cat->category}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="boxed_btn btn_1 mt-2" type="submit">ابحث</button>
                        </form></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- bradcam_area_end -->

<!-- popular_courses_start -->

<section class="special_cource padding_top" id="tabs-courses" >
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>الدورات </h2>
                    <p>ايمكنك تصفح الدورات المقام في المركز ولرؤية التفاصيل والتسجيل يمكنك تسجيل الدخول ومن ثم الاشتراك أو يمكنك التواصل معنا للاشتراك</p>
                </div>
            </div>
        </div>
        <div class="row">
            @if (isset($messageSearch))
                <div class="alert alert-success">
                    {{ $messageSearch }}
                </div>
            @endif
                <div class="col-xl-12">
                    <div class="course_nav">
                        <nav style="direction: rtl;">
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item tabCat mb-2">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true" onclick="change('all')">جميع الدورات</a>
                                </li>
                                @foreach($categories as $cat)
                                    <li class="nav-item tabCat  mb-2">
                                        <a class="nav-link tab-change" id="{{$cat->category.'link'}}" data-toggle="tab" href="#{{$cat->category}}" role="tab"
                                           aria-controls="{{$cat->category}}" aria-selected="false" onclick="change('{{$cat->category}}')"
                                        >
                                            {{$cat->category}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
            @foreach($courses as $course)
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="{{asset('/images/'.$course->image)}}" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="{{url('/course-detailes/'.$course->id)}}" class="btn_4">{{$course->category}}</a>
                            <h4>{{$course->cost_individual}}ل.س</h4>
                            <a href="{{url('/course-detailes/'.$course->id)}}"><h3>{{$course->name}}</h3></a>
                            <p> {{\Illuminate\Support\Str::limit($course->description,200,'...') }} </p>
                            <div class="author_info">
                                <div class="author_img">
                                    <!-- <img src="img/author/author_1.png" alt="">-->
                                    <div class="author_info_text">
                                        @if(isset($course->coach))
                                        <p>المدرب:</p>
                                        <h5><a > {{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</a></h5>
                                    @endif
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <div class="rating">
                                            @for($i=0;$i<$course->rating;$i++)
                                                <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                            @endfor
                                            @for($i=$course->rating;$i<5;$i++)
                                                <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                            @endfor

                                        </div>
                                    </div>
                                    <p>{{$course->rating}}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach
                        </div>
                    </div>

                    @foreach($categories as $cat)
                    <div class="tab-pane fade show " id="{{$cat->category}}" role="tabpanel" aria-labelledby="{{$cat->category}}">
                        <div class="row">
                            @foreach($courses as $course)
                                @if($course->category==$cat->category)
                                <div class="col-sm-6 col-lg-4">
                                    <div class="single_special_cource">
                                        <img src="{{asset('/images/'.$course->image)}}" class="special_img" alt="">
                                        <div class="special_cource_text">
                                            <a href="{{url('/course-detailes/'.$course->id)}}" class="btn_4">{{$course->category}}</a>
                                            <h4>{{$course->cost_individual}}ل.س</h4>
                                            <a href="{{url('/course-detailes/'.$course->id)}}"><h3>{{$course->name}}</h3></a>
                                            <p> {{\Illuminate\Support\Str::limit($course->description,200,'...') }} </p>
                                            <div class="author_info">
                                                <div class="author_img">
                                                    <!-- <img src="img/author/author_1.png" alt="">-->
                                                    <div class="author_info_text">
                                                        @if($course->coach)
                                                        <p>المدرب:</p>
                                                        <h5><a > {{$course->coach->first_name_ar." ".$course->coach->last_name_ar}}</a></h5>
                                                  @endif
                                                    </div>
                                                </div>
                                                <div class="author_rating">
                                                    <div class="rating">
                                                        @for($i=0;$i<$course->rating;$i++)
                                                            <a ><img src="{{URL::to("img/icon/color_star.svg")}}" alt=""></a>
                                                        @endfor
                                                        @for($i=$course->rating;$i<5;$i++)
                                                            <a ><img src="{{URL::to("img/icon/star.svg")}}" alt=""></a>
                                                        @endfor

                                                    </div>
                                                    <p>({{$course->rating}})</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                           @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>

        </div>
    </div>
</section>




<!-- popular_courses_end-->

<script>
    function change(id){
        if(id=='all'){
            var slides = document.getElementsByClassName("tab-pane");
                for (var i = 0; i < slides.length; i++) {
                   slides[i].style.display="block"
                }
        }else{
            var slides = document.getElementsByClassName("tab-pane");
                for (var i = 0; i < slides.length; i++) {
                    if(slides[i].id!=id){
                          slides[i].style.display="none";
                    }else{
                        slides[i].style.display="block";
                    }

                }
        }
    }
</script>
@endsection
