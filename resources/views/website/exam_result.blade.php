@extends('layouts.layout')
@section('css')
    <style>

        .exam label.radio {
            cursor: pointer
        }
    </style>
@endsection
@section('content')
    <div class="container  exam" style="margin-top: 100px">
        <div class="d-flex justify-content-center row">
            <div class="col-md-10 col-lg-10">
                <div class="border">
                    <div class="question bg-white p-3 border-bottom">
                        <div class="d-flex flex-row justify-content-between align-items-center mcq">
                            <h4>{{$exam->title}}</h4><span>عدد الأسئلة:{{count($exam->questions)}}</span>
                            <h4 class="text-warning">النتيجة: {{$studentMark."/".$fullmark}}</h4>
                        </div>
                    </div>

                        @foreach($exam->questions as $qu)
                            <div class="question bg-white p-3 border-bottom" id="Q.{{$loop->index}}">
                                <div class="d-flex flex-row align-items-center question-title">
                                    <h3 class="text-danger">Q.{{$loop->index+1}}</h3>
                                    <h5 class="mt-1 ml-2">{{$qu->text}}</h5>
                                </div>
                                @foreach($qu->answers as $answer)
                                    <div class="ans ml-2">
                                        <label class="radio">
                                            @if($answer->isCorrect)
                                                <input type="radio" checked>
                                            <span class="text-success" >{{$answer->text}}</span>
                                                @elseif($answer->id==$answers[$qu->id])
                                                <input type="radio" checked>
                                                <span class="text-danger" >{{$answer->text}}</span>
                                                @else
                                                <input type="radio" >
                                                <span >{{$answer->text}}</span>
                                            @endif
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection



