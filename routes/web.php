<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * authentication routes
 */
Auth::routes();
Route::get('/', function () {
    return redirect('home');
});
/*****
 * website routes
 */
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/course', 'HomeController@courses')->name('course');
Route::get('/download/{id}', 'HomeController@download');
Route::get('/course-detailes/{id}', 'HomeController@courseDetails')->name('courseDetails');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/contact-us', 'HomeController@contactUs')->name('contactUs');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/search_cat','HomeController@search_cat')->name('searchCat');
Route::post('/message','HelpController@sendMsg')->name('message');


Route::get('/registertrainee/{id}','TraineeController@register')->name('courseRegister');
Route::get('/closeregistertrainee/{id}','TraineeController@closeregister');
Route::resource('photo', 'PhotoController');
/*****
 * dashboard routes
 */
Route::get('/dashboard', 'DashboardController@index')->middleware('auth');
Route::resource('users', 'UserController')->middleware('auth');
Route::post('users/search','UserController@search')->name('users.search')->middleware('auth');
Route::resource('trainee', 'TraineeController')->middleware('auth');
Route::post('trainee/search','TraineeController@search')->name('trainee.search')->middleware('auth');
//Route::get('trainee/view','TraineeController@view')->name('trainee.view');

//Route::get('trainee/view', 'TraineeController@view')->name('trainee.view');

Route::resource('coach', 'CoachController')->middleware('auth');
Route::post('coach/search','CoachController@search')->name('coach.search')->middleware('auth');

Route::resource('courses','CourseController')->middleware('auth');
Route::post('courses/search','CourseController@search')->name('courses.search')->middleware('auth');

Route::resource('certificates','CertificateController')->middleware('auth');
Route::post('certificates/search','CertificateController@search')->name('certificates.search')->middleware('auth');
Route::get('certificates/print/{id}','CertificateController@print')->name('certificates.print')->middleware('auth');

 Route::get('user_courses/getDetails/{id}', 'User_CourseController@getDetails')->name('getDetails');
 Route::get('user_courses/getValue/{id}', 'User_CourseController@getValue')->name('getValue');

Route::resource('user_courses','User_CourseController');
Route::post('user_courses/search','User_CourseController@search')->name('user_courses.search');

Route::resource('receipts','ReceiptController')->middleware('auth');
Route::get('receipts/getform/{id}', 'ReceiptController@getform')->name('receipts.getform')->middleware('auth');
Route::get('receipts/print/{id}','ReceiptController@print')->name('receipts.print')->middleware('auth');

Route::resource('mangment','HomePageController')->middleware('auth');
Route::resource('mangment_fq','FaqController')->middleware('auth');
Route::resource('mangment_about','AboutUsController')->middleware('auth');
Route::resource('mangment_footer','FooterController')->middleware('auth');

Route::resource('course_trainee','CourseTraineeController')->middleware('auth');
Route::resource('course_coach','CourseCoachController')->middleware('auth');

Route::resource('user_course_message','CoachMessagesController')->middleware('auth');

Route::resource('files','FilesController')->middleware('auth');
Route::post('course/media', 'FilesController@storeMedia')
    ->name('course.storeMedia')->middleware('auth');
Route::post('course/deletemedia', 'FilesController@deleteMedia')
    ->name('course.deleteMedia')->middleware('auth');
Route::resource('exam','ExamController')->middleware('auth');
Route::get('/exam/{id}/start','HomeController@getExam')->middleware('auth');
Route::get('/join/{id}','HomeController@join')->middleware('auth');

Route::get('/ZoomMeeting',function (){
    return view('dashboard.live');
})->name('meeting')->middleware('auth');
Route::get('/zoomInit',function () {
    return view('dashboard.zoom');
})->middleware('auth');
Route::get('hostMeeting/{id}','MeetingController@startMeeting')->middleware('auth');
Route::resource('meeting','MeetingController')->middleware('auth');
Route::get('createMeeting','MeetingController@createMeeting')->middleware('auth');

Route::patch('updateMeeting/{id}','MeetingController@updateMeeting')->middleware('auth');
Route::post('exams/{id}','HomeController@getExamResult')->middleware('auth');
Route::resource('category','categoryController')->middleware('auth');
Route::post('/rate/{id}/{course}','HomeController@saveRate');
Route::resource('comment','commentController');
Route::post('getUserCourses','CertificateController@getUserCourses');
Route::get('getCertificates','CertificateController@getCertificates')->middleware('auth');
Route::get('getExams','ExamController@getExams')->middleware('auth');

Route::resource('messages','MessageController');
Route::post('getAvailableTime','CourseController@getAvailableTime');
Route::get('/getCoursePrice/{course:code}','CourseController@getPrice');
